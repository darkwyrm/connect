# Licensed Resources

Connect builds on the shoulders of giants. This software uses the following third party resources.

- Icons and Symbols from the [Material Icons Library](https://fonts.google.com/icons), released under the Apache 2.0 license.
