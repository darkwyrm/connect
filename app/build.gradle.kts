version = "0.1.0"

plugins {
    kotlin("jvm") version "1.9.25"
    kotlin("plugin.serialization") version "1.9.25"
    id("org.openjfx.javafxplugin") version "0.1.0"
    `jvm-test-suite`

    // Add the Shadow plugin for fat jar generation
    id("com.gradleup.shadow") version "8.3.5"

    application
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://plugins.gradle.org/m2/")
    }
    maven {
        // This repo is needed for Argon2. Won't be needed once keznacl is moved out of the source
        // tree.
        setUrl("https://maven.scijava.org/content/repositories/public/")
    }
}

dependencies {
    // Upgrading the serialization plugin to 1.7.x requires at least Kotlin 2.0.0RC1
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
    implementation("commons-io:commons-io:2.16.1")

    // Needed by keznacl
    implementation("com.github.alphazero:Blake2b:bbf094983c")
    implementation("org.purejava:tweetnacl-java:1.1.2")
    implementation("de.mkammerer:argon2-jvm:2.11")

    // Needed by libmensago for KDB
    implementation("org.xerial:sqlite-jdbc:3.45.2.0")

    // https://mvnrepository.com/artifact/com.h2database/h2
    implementation("com.h2database:h2:2.2.224")

    // Needed by libkeycard for RandomID ULID backend
    implementation("com.github.f4b6a3:ulid-creator:5.2.3")

    // Command-line argument parsing
    implementation("commons-cli:commons-cli:1.9.0")

    // GUI, controls, and image handling
    implementation("org.openjfx:javafx-plugin:0.1.0")
    implementation("org.openjfx:javafx-base:22.0.2")
    implementation("org.openjfx:javafx-controls:22.0.2")
    implementation("org.openjfx:javafx-swing:22.0.2")
    implementation("org.controlsfx:controlsfx:11.2.1")

    // JUnit 5 with Kotlin
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.10.2")

    // For the DNSHandler class
    implementation("org.minidns:minidns-hla:1.0.5")

    // TOML and Postgres support because DebugDNSHandler directly interacts with the local server
    // config and database
    implementation("com.moandjiezana.toml:toml4j:0.7.2")
    implementation("org.postgresql:postgresql:42.7.3")

    // Get rid of SLF4J warnings
    implementation("org.slf4j:slf4j-simple:2.0.16")

    testImplementation("org.testfx:testfx-junit5:4.0.18")
}

testing {
    suites {
        @Suppress("UnstableApiUsage")
        val integrationTest by registering(JvmTestSuite::class) {
            dependencies {
                implementation(project())
                implementation("org.openjfx:javafx-plugin:0.1.0")
                implementation("org.openjfx:javafx-base:22.0.2")
                implementation("org.openjfx:javafx-controls:22.0.2")
                implementation("org.openjfx:javafx-swing:22.0.2")
                implementation("org.testfx:testfx-junit5:4.0.18")

                // For serializing messages
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

                // For easy recursive deletion of directories
                implementation("commons-io:commons-io:2.16.1")

                // For DNS testing
                implementation("org.minidns:minidns-hla:1.0.5")

                implementation("com.h2database:h2:2.2.224")
            }
        }
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

javafx {
    modules("javafx.controls", "javafx.web", "javafx.swing")
}

application {
    mainClass.set("connect.AppKt")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<Jar>().configureEach {
    archiveBaseName.set(rootProject.name)
    archiveVersion.set(project.version.toString())
    archiveClassifier.set("")

    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClass,
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}
