package libkeycard

import keznacl.*

/**
 * A somewhat-generic data structure class encapsulating four keypairs: two for signing and two
 * for encryption. It is not used on its own. Instead, UserKeySet and OrgKeySet are actually
 * utilized for returning and transferring keypairs used in keycard entries.
 */
sealed class EntryKeySet(
    protected val encPair: EncryptionPair,
    protected val signPair: SigningPair,
    protected val altEncPair: EncryptionPair?,
    protected val altSignPair: SigningPair?
)

/**
 * A data structure class containing the keypairs used in User keycard entries
 */
class UserKeySet(
    crePair: EncryptionPair, crsPair: SigningPair, ePair: EncryptionPair,
    sPair: SigningPair,
) : EntryKeySet(crePair, crsPair, ePair, sPair) {

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("CREncPair: $encPair\n")
        sb.append("CRSSignPair: $signPair\n")
        sb.append("EncPair: $altEncPair\n")
        sb.append("SignPair: $altSignPair\n")

        return sb.toString()
    }

    fun getCREPair(): EncryptionPair = encPair
    fun getCRSPair(): SigningPair = signPair
    fun getEPair(): EncryptionPair = altEncPair!!
    fun getSPair(): SigningPair = altSignPair!!

    companion object {

        /**
         * Creates a new set of keys for a user entry
         */
        fun generate(encAlgorithm: CryptoType? = null, signAlgorithm: CryptoType? = null):
                Result<UserKeySet> {

            val enc = encAlgorithm ?: getPreferredAsymmetricAlgorithm()
            val sign = signAlgorithm ?: getPreferredSigningAlgorithm()
            return UserKeySet(
                EncryptionPair.generate(enc).getOrElse { return it.toFailure() },
                SigningPair.generate(sign).getOrElse { return it.toFailure() },
                EncryptionPair.generate(enc).getOrElse { return it.toFailure() },
                SigningPair.generate(sign).getOrElse { return it.toFailure() },
            ).toSuccess()
        }

        /**
         * Creates a new OrgKeySet object from strings in CryptoString format
         */
        fun fromStrings(
            pubCREncStr: String, privCREncStr: String,
            pubCRSignStr: String, privCRSignStr: String,
            pubEncStr: String, privEncStr: String,
            pubSignStr: String, privSignStr: String,
        ): Result<UserKeySet> {
            return UserKeySet(
                EncryptionPair.fromStrings(pubCREncStr, privCREncStr).getOrThrow(),
                SigningPair.fromStrings(pubCRSignStr, privCRSignStr).getOrThrow(),
                EncryptionPair.fromStrings(pubEncStr, privEncStr).getOrThrow(),
                SigningPair.fromStrings(pubSignStr, privSignStr).getOrThrow(),
            ).toSuccess()
        }
    }
}

/**
 * A data structure class containing the keypairs used in Organization keycard entries
 */
class OrgKeySet(sPair: SigningPair, ePair: EncryptionPair, altSPair: SigningPair?) :
    EntryKeySet(ePair, sPair, null, altSPair) {

    fun getSPair(): SigningPair {
        return signPair
    }

    fun getAltSPair(): SigningPair? {
        return altSignPair
    }

    fun getEPair(): EncryptionPair {
        return encPair
    }

    companion object {

        /**
         * Creates a new set of keys for a user entry
         */
        fun generate(encAlgorithm: CryptoType? = null, signAlgorithm: CryptoType? = null):
                Result<OrgKeySet> {

            val enc = encAlgorithm ?: getPreferredAsymmetricAlgorithm()
            val sign = signAlgorithm ?: getPreferredSigningAlgorithm()
            return OrgKeySet(
                SigningPair.generate(sign).getOrElse { return it.toFailure() },
                EncryptionPair.generate(enc).getOrElse { return it.toFailure() },
                SigningPair.generate(sign).getOrElse { return it.toFailure() },
            ).toSuccess()
        }

        /**
         * Creates a new OrgKeySet object from strings in CryptoString format
         */
        fun fromStrings(
            pubSignStr: String, privSignStr: String,
            pubEncStr: String, privEncStr: String,
            pubAltSignStr: String?, privAltSignStr: String?
        ): Result<OrgKeySet> {
            return when {
                pubAltSignStr == null && privAltSignStr == null -> {
                    OrgKeySet(
                        SigningPair.fromStrings(pubSignStr, privSignStr).getOrThrow(),
                        EncryptionPair.fromStrings(pubEncStr, privEncStr).getOrThrow(),
                        null,
                    ).toSuccess()
                }

                pubAltSignStr != null && privAltSignStr != null -> {
                    OrgKeySet(
                        SigningPair.fromStrings(pubSignStr, privSignStr).getOrThrow(),
                        EncryptionPair.fromStrings(pubEncStr, privEncStr).getOrThrow(),
                        SigningPair.fromStrings(pubAltSignStr, privAltSignStr).getOrThrow(),
                    ).toSuccess()
                }

                else -> {
                    BadValueException(
                        "Alt key fields must either be both null or both non-null"
                    ).toFailure()
                }
            }
        }
    }
}
