package libmensago

import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.RandomID

/** Note is a data model class used primarily for data (de)serialization */
@Serializable
class Note(var title: String, var contents: String, var format: ContentFormat) {
    var version = 1.0f
    var type = PayloadType.Note
    var id = RandomID.generate()
    var attachments = mutableListOf<Attachment>()

    /**
     * Creates a Payload object from the note.
     *
     * @exception SerializationException For encoding-specific errors
     * @exception IllegalArgumentException If the encoded input does not comply format's
     * specification
     */
    fun toPayload(): Result<Payload> {
        val rawJSON = runCatching { Json.encodeToString(this) }
            .getOrElse { return it.toFailure() }
        return Payload(PayloadType.Note, rawJSON).toSuccess()
    }

    companion object {
        /**
         * Creates a new note from a payload
         *
         * @exception SerializationException For decoding-specific errors
         * @exception IllegalArgumentException If the decoded input is not a valid Note object
         */
        fun fromPayload(p: Payload): Result<Note> {
            if (p.type != PayloadType.Note)
                return BadValueException("Wrong payload type").toFailure()

            return kotlin.runCatching { Json.decodeFromString<Note>(p.jsondata) }
        }
    }
}
