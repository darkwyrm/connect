package libmensago

import keznacl.*
import kotlinx.serialization.Serializable
import libkeycard.Domain
import libkeycard.Timestamp
import libkeycard.WAddress

/**
 * The RecipientLabel class contains the information used by the receiving domain's server to
 * deliver a message to its recipient. To protect the message author's privacy, this part of the
 * delivery tag contains the full address of the recipient, but only the domain of the author.
 */
@Serializable
class RecipientLabel(var to: WAddress, var senderDom: Domain)

/**
 * The SenderLabel class contains the information used by the sending domain's server to
 * deliver a message to the server for its recipient. To protect the receipient's privacy, this
 * part of the delivery tag contains only the domain of the recipient, but the full address of the
 * message author so that the sending server can handle errors and perform other administration.
 */
@Serializable
class SenderLabel(var from: WAddress, var recipientDom: Domain)

/**
 * The AddressPair class pairs together a sendar address with a recipient address. It is used to
 * ensure that deliverable payloads either have both or neither address.
 */
data class AddressPair(val from: WAddress, val to: WAddress)

/**
 * The EnvelopeHeader is a data structure class which encapsulates all information needed to deliver
 * a completely encrypted message. It is merely an intermediary class for constructing a
 * SealedEnvelopeHeader instance used in an Envelope object. General usage will entail creating a
 * new EnvelopeHeader and then calling seal().
 *
 */
class EnvelopeHeader private constructor(
    var receiver: RecipientLabel?, var sender: SenderLabel?,
    val payloadKey: SecretKey
) {
    var version = APIVersion(1, 0, 0)
    var date = Timestamp()

    private constructor(payloadKey: SecretKey) : this(null, null, payloadKey)

    /**
     * Creates a SealedEnvelopeHeader instance ready for use in an Envelope object. The recipientKey
     * parameter is intentionally vague. Determining whether to use asymmetric or symmetric
     * encryption and the algorithm used requires additional context to determine. So long as the
     * encryption key used can be decrypted by the recipient, the specific don't matter as far as
     * this call is concerned.
     *
     * @param recipientKey An encryption key used for communicating with the recipient.
     * @param senderKey The encryption key for the sender's organization. This is obtained from the
     * Encryption-Key field in the organization's keycard.
     * @param receiverKey The encryption key for the recipient's organization. This is obtained from
     * the Encryption-Key field in the keycard for the recipient's organization.
     */
    fun seal(recipientKey: Encryptor, senderKey: Encryptor?, receiverKey: Encryptor?):
            Result<SealedEnvelopeHeader> {
        return when {
            receiver == null && sender == null -> {
                val encryptedPayloadKey = recipientKey.encrypt(payloadKey.key.toByteArray())
                    .getOrElse { return it.toFailure() }
                SealedEnvelopeHeader(
                    recipientKey.getPublicHash().getOrElse { return it.toFailure() },
                    encryptedPayloadKey, date,
                ).toSuccess()
            }

            receiver != null && sender != null -> {
                val encryptedReceiverTag = serializeAndEncrypt(receiver, receiverKey!!)
                    .getOrElse { return it.toFailure() }
                val encryptedSenderTag = serializeAndEncrypt(sender, senderKey!!)
                    .getOrElse { return it.toFailure() }
                val encryptedPayloadKey = recipientKey.encrypt(payloadKey.key.toByteArray())
                    .getOrElse { return it.toFailure() }

                SealedEnvelopeHeader(
                    encryptedReceiverTag, encryptedSenderTag, date, encryptedPayloadKey,
                    recipientKey.getPublicHash().getOrElse { return it.toFailure() },
                    version
                ).toSuccess()
            }

            else -> BadValueException().toFailure()
        }

    }

    override fun toString(): String = "EnvelopeHeader($receiver, $sender, $payloadKey)"

    companion object {

        /**
         * Create a new EnvelopeHeader object from the desired sender and recipient addresses and
         * message type information.
         */
        fun new(addPair: AddressPair?): Result<EnvelopeHeader> {

            val key = SecretKey.generate().getOrElse { return it.toFailure() }
            if (addPair == null)
                return EnvelopeHeader(key).toSuccess()

            return EnvelopeHeader(
                RecipientLabel(addPair.to, addPair.from.domain),
                SenderLabel(addPair.from, addPair.to.domain),
                key
            ).toSuccess()
        }

        /**
         * This convenience function is for the most common case -- creating an envelope header for
         * a message being sent.
         */
        fun fromMessage(message: Message): Result<EnvelopeHeader> {
            return EnvelopeHeader(
                RecipientLabel(message.to, message.from.domain),
                SenderLabel(message.from, message.to.domain),
                SecretKey.generate().getOrElse { return it.toFailure() },
            ).toSuccess()
        }
    }
}
