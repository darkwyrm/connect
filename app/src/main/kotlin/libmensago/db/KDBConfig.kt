package libmensago.db

import java.sql.Connection
import java.sql.DriverManager
import java.util.*

interface KDBConfig {
    fun connect(): Result<Connection> {
        return kotlin.runCatching { DriverManager.getConnection(getURL(), getProperties()) }
    }

    fun getURL(): String
    fun getProperties(): Properties?
}

/**
 * KDB settings class for connecting to PostgreSQL databases.
 */
data class PGConfig(
    var user: String, var password: String, var host: String, var port: Int,
    var dbname: String
) : KDBConfig {

    override fun getURL(): String {
        return "jdbc:postgresql://$host:$port/$dbname"
    }

    override fun getProperties(): Properties {
        return Properties().apply {
            this["user"] = user
            this["password"] = password
        }
    }
}

/**
 * KDB settings class for connecting to SQLite3 databases.
 *
 * @param path The filesystem path to the database. If an in-memory database is desired,
 * use ":memory:" as the value.
 */
data class SqliteConfig(val path: String) : KDBConfig {

    override fun getURL(): String {
        return "jdbc:sqlite:$path"
    }

    override fun getProperties(): Properties? {
        return null
    }
}

/**
 * KDB settings class for connecting to H2 databases.
 *
 * @param path The filesystem path to the database. If an in-memory database is desired,
 * use ":memory:" as the value.
 */
data class H2Config(val path: String) : KDBConfig {

    override fun getURL(): String {
        return "jdbc:h2:$path"
    }

    override fun getProperties(): Properties? {
        return null
    }
}
