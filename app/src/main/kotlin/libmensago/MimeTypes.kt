package libmensago

object MimeTypes {
    val contact = MimeType.fromString("application/vnd.mensago.contact")
    val cryptoString = MimeType.fromString("application/vnd.mensago.cryptostring")!!
    val deviceInfo = MimeType.fromString("application/vnd.mensago.deviceinfo")!!
    val randomID = MimeType.fromString("application/vnd.mensago.randomid")!!
}
