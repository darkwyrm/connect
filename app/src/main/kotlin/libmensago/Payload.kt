package libmensago

import keznacl.*
import kotlinx.serialization.Serializable
import libkeycard.BadFieldValueException
import libkeycard.Domain

/**
 * The Payload class encapsulates the raw JSON data in a data item payload. It is used handling
 * data transferred to and from the server.
 */
data class Payload(val type: PayloadType, val jsondata: String) {
    fun encrypt(key: Encryptor): Result<CryptoString> {
        return key.encrypt("$type\r\n$jsondata\r\n")
    }

    companion object {
        fun decrypt(key: Decryptor, encdata: CryptoString): Result<Payload> {
            val parts = key.decrypt(encdata).getOrElse { return it.toFailure() }.decodeToString()
                .split("\r\n", limit = 2)
            if (parts.size != 2) return BadValueException("Invalid decrypted payload format")
                .toFailure()

            val ptype = PayloadType.fromString(parts[0])
                ?: return BadFieldValueException("Invalid payload type ${parts[0]}").toFailure()
            return Payload(ptype, parts[1]).toSuccess()
        }
    }
}

/**
 * An enumerated type which represents type of payload in an item. The string format for a payload
 * type is similar to that of a MIME type but with a period and major+minor version appended:
 * org.mensago/message.v1.0, etc.
 */
@Serializable
data class PayloadType(val group: Domain, val datatype: String, val apiversion: APIVersion) {
    override fun toString(): String {
        return "$group/$datatype.v$apiversion"
    }

    fun isMessage(): Boolean {
        return this in listOf(Message, ContactMessage, DeviceRequest)
    }

    fun isNote(): Boolean {
        return this == Note
    }

    companion object {
        val Message = PayloadType(
            Domain.fromString("org.mensago")!!, "message",
            APIVersion(1, 0, 0)
        )
        val DeviceRequest = PayloadType(
            Domain.fromString("org.mensago")!!, "devrequest",
            APIVersion(1, 0, 0)
        )
        val ContactMessage = PayloadType(
            Domain.fromString("org.mensago")!!, "conrequest",
            APIVersion(1, 0, 0)
        )
        val Contact = PayloadType(
            Domain.fromString("org.mensago")!!, "contact",
            APIVersion(1, 0, 0)
        )
        val Note = PayloadType(
            Domain.fromString("org.mensago")!!, "note",
            APIVersion(1, 0, 0)
        )


        fun fromString(s: String): PayloadType? {
            val parts = s.lowercase().split('/')
            if (parts.size != 2) return null

            val group = Domain.fromString(parts[0]) ?: return null
            val dataParts = parts[1].split(".v")
            if (dataParts.size != 2) return null
            val version = APIVersion.fromString(dataParts[1]) ?: return null
            return PayloadType(group, dataParts[0], version)
        }
    }
}

@Serializable
data class APIVersion(val major: Int, val minor: Int, val patch: Int) {
    override fun toString(): String {
        return "$major.$minor.$patch"
    }

    companion object {
        fun fromString(s: String): APIVersion? {
            return try {
                val parts = s.split('.')
                if (parts.size != 3) return null
                APIVersion(parts[0].toInt(), parts[1].toInt(), parts[2].toInt())
            } catch (_: Exception) {
                return null
            }
        }
    }
}
