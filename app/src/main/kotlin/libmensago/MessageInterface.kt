package libmensago

import libkeycard.WAddress

/**
 * MessageInterface is an access interface which exists to allow for interaction with different
 * kinds of messages without creating problems with the serialization plugin by using inheritance.
 */
interface MessageInterface {

    fun sender(): WAddress
    fun recipient(): WAddress

    fun updateBody(str: String)
    fun accessBody(): String

    fun getAddressPair(): AddressPair {
        return AddressPair(from = sender(), to = recipient())
    }

    fun updateSubType(str: String?)
    fun accessSubType(): String?

    fun toPayload(): Result<Payload>
}

