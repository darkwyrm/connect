package libmensago

import keznacl.BadValueException
import keznacl.toFailure
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libkeycard.WAddress

/** The Contact class is a contact information exchange class */
@Suppress("unused")
@Serializable
class Contact(
    var entityType: EntityType,
    var formattedName: String = "",
    var update: Boolean? = null,
    var givenName: String? = null,
    var familyName: String? = null,
    var middleName: String? = null,
    var prefix: String? = null,
    var suffix: String? = null,
    var gender: String? = null,
    var bio: String? = null,

    var social: MutableList<LabeledField>? = null,
    var phone: MutableList<LabeledField>? = null,
    var keys: MutableMap<String, String>? = null,
    var websites: MutableList<LabeledField>? = null,

    var userID: UserID? = null,
    var workspace: RandomID? = null,
    var domain: Domain? = null,

    var mail: MutableList<MailingAddress>? = null,
    var email: MutableList<LabeledField>? = null,

    var anniversary: String? = null,
    var birthday: String? = null,
    var organization: String? = null,
    var orgUnits: MutableList<String>? = null,
    var title: String? = null,
    var languages: MutableList<String>? = null,

    var notes: String? = null,
    var noteFormat: ContentFormat? = null,

    var photo: Photo? = null,

    var attachments: MutableList<Attachment>? = null,
    var custom: MutableMap<String, String>? = null,
    var id: RandomID = RandomID.generate(),
) {
    val type = MimeType.fromString("application/vnd.mensago.contact")
    val version = "1.0.0"

    override fun toString(): String = formattedName

    fun getWAddress(): WAddress? {
        return if (workspace != null && domain != null)
            WAddress.fromParts(workspace!!, domain!!) else null
    }

    /** The type of entity represented by a Contact instance */
    enum class EntityType {
        Individual,
        Organization,
        Group;

        override fun toString(): String {
            return when (this) {
                Individual -> "individual"
                Organization -> "organization"
                Group -> "group"
            }
        }

        companion object {
            fun fromString(s: String): EntityType? {
                return when (s) {
                    "individual" -> Individual
                    "organization" -> Organization
                    "group" -> Group
                    else -> null
                }
            }
        }
    }

    /** A generic key-value container needed for the Contact class with an optional Preferred flag. */
    @Serializable
    class LabeledField(var field: String, var value: String, var preferred: Boolean?) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as LabeledField
            return field == other.field && value == other.value && preferred == other.preferred
        }

        override fun hashCode(): Int {
            var result = 31 * field.hashCode() + value.hashCode()
            result = 31 * result + preferred.hashCode()
            return result
        }
    }

    /** A data container for mailing address fields */
    @Serializable
    class MailingAddress(
        var label: String,
        var street: String?,
        var extended: String?,
        var locality: String?,
        var region: String?,
        var postalCode: String?,
        var country: String?,
        var preferred: Boolean?,
    )

    @Serializable
    class Photo(
        var mime: MimeType,
        var data: String,
    )

    companion object {
        fun fromPayload(p: Payload): Result<Contact> {
            if (p.type != PayloadType.Contact)
                return BadValueException("Wrong payload type").toFailure()

            return kotlin.runCatching { Json.decodeFromString<Contact>(p.jsondata) }
        }
    }
}