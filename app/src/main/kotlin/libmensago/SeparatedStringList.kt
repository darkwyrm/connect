package libmensago

/**
 * SeparatedStringList represents a group of strings that are separated by a string of some type,
 * e.g. a comma, a colon, etc. THe separator may be more than one character, but be aware that items
 * which contain the separator character(s) may exhibit strange behavior.
 */
class SeparatedStringList(value: String, var separator: String = ",") {
    val items = value.split(separator).filter { it.isNotEmpty() }.toMutableList()

    fun join(): String {
        return if (items.isEmpty()) ""
        else items.joinToString(separator)
    }

    /**
     * Appends a string to the list. This can append one item at a time or multiple items if given
     * a string containing the separator.
     */
    fun push(s: String): SeparatedStringList {
        items.addAll(parse(s, separator))
        return this
    }

    /** Replaces the contents of the list with that of the given string */
    fun set(s: String): SeparatedStringList {
        items.clear()
        push(s)
        return this
    }

    fun setSeparator(s: String): SeparatedStringList {
        separator = s
        return this
    }

    override fun toString(): String {
        return join()
    }

    companion object {

        fun fromList(list: List<Any>, separator: String): SeparatedStringList {
            val out = SeparatedStringList(separator)
            out.items.addAll(list.map { it.toString() })
            return out
        }

        private fun parse(s: String, sep: String): List<String> {
            return s.split(sep)
                .map { it.trim() }
                .filter { it.isNotEmpty() }
        }
    }
}
