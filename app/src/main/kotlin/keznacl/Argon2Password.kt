package keznacl

import de.mkammerer.argon2.Argon2Advanced
import de.mkammerer.argon2.Argon2Factory
import java.nio.charset.Charset
import java.util.*

/**
 * Instantiates an Argon2-family class instance from the supplied hash.
 *
 * @exception EmptyDataException Returned if given an empty string
 * @exception BadValueException Returned if given a string that isn't an Argon2 hash
 */
fun argonPassFromHash(hashStr: String): Result<Argon2Password> {
    hashStr.ifEmpty { return EmptyDataException().toFailure() }

    val parts = hashStr.trim().split('$', limit = 3).filter { it.isNotEmpty() }
    if (parts.isEmpty()) return BadValueException().toFailure()
    val out = when (parts[0]) {
        "argon2d" -> Argon2dPassword()
        "argon2i" -> Argon2idPassword()
        "argon2id" -> Argon2idPassword()
        else -> return BadValueException().toFailure()
    }
    out.setFromHash(hashStr)?.let { return BadValueException().toFailure() }

    return out.toSuccess()
}


/**
 * Represents a password and its hash from the Argon2 family. It can also create a hash from a
 * cleartext password. In most situations, you merely need to instantiate an instance of
 * [Argon2idPassword] and call [updateHash]. The defaults provide good security in many situations,
 * but if you need extra, you can call [setStrength] or set parameters manually.
 */
sealed class Argon2Password : Password() {
    protected abstract val hasher: Argon2Advanced
    protected var rawSalt = byteArrayOf()
    protected var iterationValue = 10
    protected var memoryValue = 0x100_000
    protected var threadValue = 4
    protected var versionValue = 19

    override fun setSalt(value: String): Throwable? {
        return runCatching {
            rawSalt = Base64.getDecoder().decode(value)
            info.salt = value
        }.exceptionOrNull()
    }

    /** Returns the version of the Argon algorithm used. This normally returns 19. */
    fun getVersion(): Int {
        return versionValue
    }

    /** Returns the time cost (iterations) */
    fun getIterations(): Int {
        return iterationValue
    }

    /** Sets the time cost (iterations) to be used. This must be at least 1 and if set to less than
     * 1, will be set to 1. Please consult the Argon2 documentation for recommended values for
     * suitable values.
     */
    fun setIterations(i: Int) {
        iterationValue = if (i < 1) 1 else i
        info.parameters = "m=$memoryValue,t=$iterationValue,p=$threadValue"
    }

    /** Returns the memory cost in bytes */
    fun getMemory(): Int {
        return memoryValue
    }

    /**
     * Sets the memory cost in bytes. This must be at least 1 and if set to less than 1, will be
     * set to 1. 1 is not a secure value for memory cost, however. Please consult the Argon2
     * documentation for recommended values for suitable values.
     */
    fun setMemory(m: Int) {
        memoryValue = if (m < 1) 1 else m
        info.parameters = "m=$memoryValue,t=$iterationValue,p=$threadValue"
    }

    /** Returns the parallelism (threads) value for the hash */
    fun getThreads(): Int {
        return threadValue
    }

    /** Sets the parallelism (threads) value for the hash. Please consult the Argon2
     * documentation for recommended values for suitable values.
     */
    fun setThreads(p: Int) {
        threadValue = if (p < 1) 1 else p
        info.parameters = "m=$memoryValue,t=$iterationValue,p=$threadValue"
    }

    /**
     * Sets the parameter string for the hash. This also updates the parameters for the password
     * object itself. The format is a comma-separated triplet of key-value pairs. An example would
     * be `m=1024,t=100,p=4`, where 1024 is the memory cost, t is the time cost, and p is the
     * parallelism value.
     *
     * @exception BadValueException Returned if given a bad parameter string.
     */
    override fun setParameters(value: String): Throwable? {
        val params = parseParameters(value).getOrElse { return it }
        memoryValue = params.first
        iterationValue = params.second
        threadValue = params.third

        info.parameters = value
        return null
    }

    /**
     * Sets the instance's properties from an existing Argon2 hash
     *
     * @exception BadValueException Returned if given a bad hash algorithm name or the hash from
     * a different variant of the same algorithm family.
     * @exception NumberFormatException Returned if a hash parameter value or the version is not a
     * number
     * @exception IllegalArgumentException Returned if there is a Base64 decoding error
     */
    override fun setFromHash(hashStr: String): Throwable? {
        parseHash(hashStr)?.let { return it }
        hashValue = hashStr

        return null
    }

    /**
     * Updates the object from a [PasswordInfo] structure. NOTE: this call only performs basic
     * validation. If you are concerned about the validity of the data in pwInfo, pass it through
     * [Argon2Password.validateInfo] first.
     *
     * @exception NumberFormatException Returned if given non-integers for parameter values
     * @exception IllegalArgumentException Returned for Base64 decoding errors
     */
    fun setFromInfo(pwInfo: PasswordInfo): Throwable? {
        runCatching {
            setSalt(pwInfo.salt)
        }.getOrElse { return it }

        val paramData = parseParameters(pwInfo.parameters).getOrElse { return it }
        memoryValue = paramData.first
        iterationValue = paramData.second
        threadValue = paramData.third

        return null
    }

    /**
     * Sets the general strength of the instance's properties. This affects memory cost,
     * parallelism, and iterations.
     *
     * @return Reference to the current object
     */
    override fun setStrength(strength: HashStrength): Argon2Password {

        memoryValue = when (strength) {
            HashStrength.Basic -> 0x100_000
            HashStrength.Extra -> 0x200_000
            HashStrength.Secret -> 0x400_000
            HashStrength.Extreme -> 0x800_000
        }
        iterationValue = 10
        threadValue = 4

        return this
    }

    /**
     * Updates the instance's hash to represent the provided password using the current hash
     * properties and returns the updated value.
     */
    override fun updateHash(pw: String): Result<String> {

        runCatching {
            hashValue = hasher.hash(
                iterationValue, memoryValue, threadValue, pw.toCharArray(),
                Charset.defaultCharset(), rawSalt
            )
        }.getOrElse { return it.toFailure() }

        return hashValue.toSuccess()
    }

    /**
     * Returns true if the supplied password matches against the hash stored in the instance.
     * Because there are no guarantees if the instance's cleartext password property is in sync with
     * the hash, this call only compares the supplied password against the hash itself.
     */
    override fun verify(pw: String): Boolean {
        return hasher.verify(hashValue, pw.toCharArray())
    }

    /**
     * Updates internal properties from the passed hash. It does *not* change the internal hash
     * property itself.
     *
     * @exception BadValueException Returned if given a bad hash algorithm name
     * @exception NumberFormatException Returned if a hash parameter value or the version is not a
     * number
     * @exception IllegalArgumentException Returned if there is a Base64 decoding error
     */
    private fun parseHash(hashStr: String): Throwable? {
        // Sample of the hash format
//      $argon2id$v=19$m=65536,t=2,p=1$azRGvkBtov+ML8kPEaBcIA$tW+o1B8XeLmbKzzN9EQHu9dNLW4T6ia4ha0Z5ZDh7f4

        val parts = hashStr.trim().split('$').filter { it.isNotEmpty() }

        if (parts[0].uppercase() != info.algorithm)
            return BadValueException("not an ${info.algorithm} hash")

        val versionParts = parts[1].split('=')
        if (versionParts[0] != "v") return BadValueException("second hash field not the version")

        val paramParts = parseParameters(parts[2]).getOrElse { return it }

        runCatching {
            versionValue = versionParts[1].toInt()
            memoryValue = paramParts.first
            iterationValue = paramParts.second
            threadValue = paramParts.third
        }.getOrElse { return it }

        info.parameters = parts[2]
        info.salt = parts[3]
        rawSalt = runCatching { Base64.getDecoder().decode(getSalt()) }.getOrElse { return it }

        return null
    }

    /**
     * Parses out an Argon2 parameter token and returns a triple containing memory, iterations (time
     * cost), and parallelism (threads).
     *
     * @exception NumberFormatException Returned if given non-integers for parameter values
     */
    private fun parseParameters(paramStr: String): Result<Triple<Int, Int, Int>> {
        val paramParts = paramStr
            .split(',')
            .associateBy({ it.split('=')[0] }, { it.split('=')[1] })
        return runCatching {
            Triple(
                paramParts["m"]!!.toInt(),
                paramParts["t"]!!.toInt(),
                paramParts["p"]!!.toInt()
            ).toSuccess()
        }.getOrElse { it.toFailure() }
    }

    companion object {

        /**
         * Ensures that all the hash-related information in the instance is present and valid
         *
         * @exception IllegalArgumentException Returned for Base64 decoding errors
         */
        fun validateInfo(info: PasswordInfo): Throwable? {

            if (info.salt.isEmpty()) return EmptyDataException("Salt missing")
            if (info.parameters.isEmpty()) return EmptyDataException("Hash parameters missing")

            if (!listOf("ARGON2D", "ARGON2I", "ARGON2ID").contains(info.algorithm))
                return BadValueException("${info.algorithm} not supported by Argon2Password class")

            runCatching {
                Base64.getDecoder().decode(info.salt)
            }.getOrElse {
                return IllegalArgumentException()
            }

            val paramParts = info.parameters
                .split(',')
                .associateBy({ it.split('=')[0] }, { it.split('=')[1] })

            if (paramParts["m"] != null) {
                runCatching {
                    paramParts["m"]!!.toInt()
                }.getOrElse {
                    return BadValueException("bad memory value in parameters")
                }
            } else return MissingDataException("memory value missing from parameters")

            if (paramParts["t"] != null) {
                runCatching {
                    paramParts["t"]!!.toInt()
                }.getOrElse {
                    return BadValueException("bad time cost value in parameters")
                }
            } else return MissingDataException("time cost value missing from parameters")

            if (paramParts["p"] != null) {
                runCatching {
                    paramParts["p"]!!.toInt()
                }.getOrElse {
                    return BadValueException("bad thread value in parameters")
                }
            } else return MissingDataException("thread value missing from parameters")

            return null
        }
    }
}

/**
 * An Argon2 algorithm which balances protection against brute force attacks and side-channel
 * attacks. If you're not sure which of the three Argon variants to choose, pick this one.
 */
class Argon2idPassword : Argon2Password() {

    override val hasher: Argon2Advanced = Argon2Factory
        .createAdvanced(Argon2Factory.Argon2Types.ARGON2id)

    init {
        info.algorithm = "ARGON2ID"
        rawSalt = hasher.generateSalt()
        setSalt(Base64.getEncoder().withoutPadding().encodeToString(rawSalt))?.let { throw it }
    }

    override fun copy(): Password {
        val out = Argon2idPassword()
        out.info = info.copy()
        out.rawSalt = rawSalt.clone()
        out.iterationValue = iterationValue
        out.memoryValue = memoryValue
        out.threadValue = threadValue
        out.versionValue = versionValue

        return out
    }

    companion object {
        fun fromString(s: String): Argon2idPassword? {
            val out = Argon2idPassword()
            return if (out.setFromHash(s) != null) null else out
        }
    }
}

/**
 * An Argon2 algorithm which maximizes protection against GPU-based brute force attacks, but
 * introduces possible side-channel attacks.
 */
class Argon2dPassword : Argon2Password() {

    override val hasher: Argon2Advanced = Argon2Factory
        .createAdvanced(Argon2Factory.Argon2Types.ARGON2d)

    init {
        info.algorithm = "ARGON2D"
        rawSalt = hasher.generateSalt()
        setSalt(Base64.getEncoder().withoutPadding().encodeToString(rawSalt))?.let { throw it }
    }

    override fun copy(): Password {
        val out = Argon2dPassword()
        out.info = info.copy()
        out.rawSalt = rawSalt.clone()
        out.iterationValue = iterationValue
        out.memoryValue = memoryValue
        out.threadValue = threadValue
        out.versionValue = versionValue

        return out
    }

    companion object {
        fun fromString(s: String): Argon2dPassword? {
            val out = Argon2dPassword()
            return if (out.setFromHash(s) != null) null else out
        }
    }
}

/** An Argon2 algorithm which is optimized to resist side-channel attacks. */
class Argon2iPassword : Argon2Password() {
    override val hasher: Argon2Advanced = Argon2Factory
        .createAdvanced(Argon2Factory.Argon2Types.ARGON2i)

    init {
        info.algorithm = "ARGON2I"
        rawSalt = hasher.generateSalt()
        setSalt(Base64.getEncoder().withoutPadding().encodeToString(rawSalt))?.let { throw it }
    }

    override fun copy(): Password {
        val out = Argon2iPassword()
        out.info = info.copy()
        out.rawSalt = rawSalt.clone()
        out.iterationValue = iterationValue
        out.memoryValue = memoryValue
        out.threadValue = threadValue
        out.versionValue = versionValue

        return out
    }

    companion object {
        fun fromString(s: String): Argon2iPassword? {
            val out = Argon2iPassword()
            return if (out.setFromHash(s) != null) null else out
        }
    }
}
