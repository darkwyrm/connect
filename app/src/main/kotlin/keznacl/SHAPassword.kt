package keznacl

import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import java.util.regex.Pattern

/**
 * Class which uses SHA3-256 for hashing passwords. This is not suitable for circumstances requiring
 * good security, but it is useful in situations where security is not a concern, such as unit
 * tests.
 *
 * The hash format used for this algorithm is $sha3-256$t=iterations$salt$hash
 */
class SHAPassword : Password() {

    private var saltSize = 32
    private var iterations = 100_000
    private var internalHash = ""
    private val rng = SecureRandom()
    private val encoder = Base64.getEncoder()
    private val hasher = MessageDigest.getInstance("SHA3-256")

    init {
        info.algorithm = "SHA3-256"
        info.parameters = "t=100000"
        updateSalt()
    }

    /** Returns the size of the salt used for the hash */
    fun getSaltSize(): Int {
        return saltSize
    }

    /** Returns the number of iterations used */
    fun getIterations(): Int {
        return iterations
    }

    /**
     * Sets the number of iterations used. While this algorithm should not be used for general
     * purpose password hashing, using a value of at least 500,000 should at least provide some
     * semblance of security if used. This value must be at least 1. If set to a value less than 1,
     * it will be set to 1.
     */
    fun setIterations(i: Int) {
        iterations = if (i < 1) 1 else i
        info.parameters = "t=$i"
    }

    override fun copy(): Password {
        val out = SHAPassword()
        out.info = info.copy()
        out.saltSize = saltSize
        out.iterations = iterations
        out.internalHash = internalHash

        return out
    }

    /**
     * Sets the parameter string for the hash. This also updates the parameters for the password
     * object itself. The format is a 1-letter key to number pair. An example would
     * be `t=1024`, where 1024 is the number of iterations to use.
     *
     * @exception BadValueException Returned if given a bad parameter string.
     */
    override fun setParameters(value: String): Throwable? {
        // This call parses the parameter string and sets the iterations property from it
        parseParameters(value)?.let { return it }

        // This updates the internal parameter string
        return super.setParameters(value)
    }

    override fun setFromHash(hashStr: String): Throwable? {
        parseHash(hashStr)?.let { return it }
        hashValue = hashStr

        return null
    }

    /**
     * Updates the object from a [PasswordInfo] structure. NOTE: this call only performs basic
     * validation. If you are concerned about the validity of the data in pwInfo, pass it through
     * [Argon2Password.validateInfo] first.
     *
     * @exception NumberFormatException Returned if given non-integers for parameter values
     * @exception IllegalArgumentException Returned for Base64 decoding errors
     */
    fun setFromInfo(pwInfo: PasswordInfo): Throwable? {
        info.salt = pwInfo.salt
        return parseParameters(pwInfo.parameters)
    }

    /** Sets the strength of the hash in a non-technical way. */
    override fun setStrength(strength: HashStrength): SHAPassword {
        when (strength) {
            HashStrength.Basic -> {
                iterations = 500_000
                updateSalt(24)
            }

            HashStrength.Extra -> {
                iterations = 750_000
                updateSalt(32)
            }

            HashStrength.Secret -> {
                iterations = 1_000_000
                updateSalt(32)
            }

            HashStrength.Extreme -> {
                iterations = 1_250_000
                updateSalt(32)
            }
        }
        return this
    }

    /**
     * Updates the hash value given a cleartext password. This variant always returns success.
     */
    override fun updateHash(pw: String): Result<String> {
        internalHash = compute(pw)
        hashValue = "\$sha3-256\$t=$iterations\$${info.salt}\$$internalHash"
        return hashValue.toSuccess()
    }

    /**
     * Changes the salt used for the hash. The method can take a integer indicating the number of
     * bytes to use for the salt. If not given one, a new salt is generated using the current
     * length.
     */
    fun updateSalt(size: Int? = null) {
        if (size != null) {
            if (size < 0) saltSize else saltSize = size
        }

        val rawSalt = ByteArray(saltSize)
        rng.nextBytes(rawSalt)
        info.salt = encoder.encodeToString(rawSalt)
    }

    /** Returns true if the supplied cleartext password matches that used to create the hash. */
    override fun verify(pw: String): Boolean {
        return compute(pw) == internalHash
    }

    /**
     * Private method which calculates the hash and returns it Base64-encoded. Note that this value
     * is only the hash and no other parameters, such as iteraction count.
     */

    private fun compute(s: String): String {
        val count = if (iterations < 1) 1 else iterations

        var input = if (saltSize > 0) {
            "${info.salt}$s".encodeToByteArray()
        } else
            s.encodeToByteArray()

        hasher.reset()
        repeat(count) {
            hasher.update(input)
            input = hasher.digest()
        }
        return encoder.encodeToString(input)
    }

    /**
     * Updates internal properties from the passed hash. It does *not* change the internal hash
     * property itself.
     *
     * @exception BadValueException Returned if given a bad hash algorithm name
     * @exception NumberFormatException Returned if a hash parameter value or the version is not a
     * number
     * @exception IllegalArgumentException Returned if there is a Base64 decoding error
     */
    private fun parseHash(hashStr: String): Throwable? {
        // Sample of the hash format
//      $sha3-256$t=100000$azRGvkBtov+ML8kPEaBcIA$tW+o1B8XeLmbKzzN9EQHu9dNLW4T6ia4ha0Z5ZDh7f4

        val parts = hashStr.trim().split('$').filter { it.isNotEmpty() }

        if (parts.size !in 3..4 || parts[0].uppercase() != info.algorithm)
            return BadValueException("not a SHA3-256 hash")

        if (!itPattern.matcher(parts[1]).matches())
            return BadValueException("bad iteration count parameter")
        iterations = runCatching { parts[1].split('=').last().toInt() }
            .getOrElse { return BadValueException("bad iteration count parameter") }

        info.parameters = parts[1]

        if (parts.size == 4) {
            info.salt = parts[2]
            internalHash = parts[3]
        } else {
            info.salt = ""
            internalHash = parts[2]
        }
        hashValue = "\$sha3-256\$t=$iterations\$${info.salt}\$$internalHash"

        return null
    }

    private fun parseParameters(s: String): Throwable? {
        val parts = s.split(',')
            .associateBy({ it.split('=')[0] }, { it.split('=')[1] })
        if (parts.size != 1) return BadValueException("Invalid parameter string")
        if (parts["t"] == null) return BadValueException("Missing interations parameter (t=???)")
        iterations = runCatching { parts["t"]!!.toInt() }
            .getOrElse { return BadValueException("bad iteration count parameter") }

        return null
    }

    companion object {
        private val itPattern = Pattern.compile(
            "^t=[0-9]+\$"
        )!!

        fun validateInfo(info: PasswordInfo): Throwable? {
            if (info.parameters.isEmpty()) return EmptyDataException("Hash parameters missing")
            if (info.salt.isNotEmpty()) {
                runCatching {
                    Base64.getDecoder().decode(info.salt)
                }.getOrElse {
                    return IllegalArgumentException("Error decoding salt")
                }
            }
            if (info.algorithm.uppercase() != "SHA3-256")
                return UnsupportedAlgorithmException()

            val paramParts = info.parameters.split(',')
                .associateBy({ it.split('=')[0] }, { it.split('=')[1] })
            if (paramParts.size != 1) return BadValueException("Invalid parameter string")
            if (paramParts["t"] == null)
                return BadValueException("Missing interations parameter (t=???)")
            val iterations = runCatching { paramParts["t"]!!.toInt() }
                .getOrElse { return BadValueException("bad iteration count parameter") }
            if (iterations < 1) return BadValueException("bad iteration count parameter")

            return null
        }
    }
}

fun shaPassFromHash(hashStr: String): Result<SHAPassword> {
    hashStr.ifEmpty { return EmptyDataException().toFailure() }

    val parts = hashStr.trim().split('$').filter { it.isNotEmpty() }
    if (parts.isEmpty() || parts.size !in 3..4)
        return BadValueException().toFailure()
    if (parts[0] != "sha3-256")
        return UnsupportedAlgorithmException().toFailure()

    val out = SHAPassword()
    out.setFromHash(hashStr)?.let { return BadValueException().toFailure() }
    return out.toSuccess()

}
