package connect

import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.Image
import keznacl.*
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libmensago.Contact
import libmensago.ContentFormat
import libmensago.MimeType
import libmensago.ResourceNotFoundException
import utilities.DBProxy
import utilities.getStringOrNull
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

/**
 * RandomID used to represent the current user. Because of the way that ULIDs are generated, this
 * will not conflict with randomly-generated ones.
 */
val gUserConID = RandomID.fromString("00000000-0000-0000-0000-000000000001")!!

/**
 * The UserInfoDAO is another DAO that interacts with contact information except that it explicitly
 * targets the current user's information. It provides different APIs than ContactDAO because its
 * primary purpose is saving information to the database in response to changes made by the user.
 */
class UserInfoDAO {

    fun loadPublicContact(): Result<Contact> {
        val db = DBProxy.get()
        val rs = db.query("SELECT * FROM contacts WHERE id=?", gUserConID)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val photoType = MimeType.fromString(rs.getStringOrNull("phototype"))
        val encodedPhoto: String = kotlin.runCatching {
            rs.getBytes("photo")?.let { Base85.encode(it) } ?: ""
        }.getOrElse { return it.toFailure() }

        return Contact(
            Contact.EntityType.Individual,
            formattedName = rs.getString("formatted_name") ?: "",
            update = false,
            givenName = rs.getStringOrNull("given_name"),
            familyName = rs.getStringOrNull("family_name"),
            middleName = rs.getStringOrNull("middle_name"),
            prefix = rs.getStringOrNull("prefix"),
            suffix = rs.getStringOrNull("suffix"),
            gender = rs.getStringOrNull("gender"),
            bio = rs.getStringOrNull("bio"),

            social = loadLabeledField("social").getOrElse { return it.toFailure() },
            phone = loadLabeledField("phone").getOrElse { return it.toFailure() },

            // We won't be loading any keys here the keys needed are dependent on the purpose, such
            // as generating new ones for a contact request

            websites = loadLabeledField("website").getOrElse { return it.toFailure() },

            userID = UserID.fromString(rs.getStringOrNull("uid")),
            workspace = RandomID.fromString(rs.getStringOrNull("wid")),
            domain = Domain.fromString(rs.getStringOrNull("domain")),

            mail = loadMailingAddresses().getOrElse { return it.toFailure() },
            email = loadLabeledField("email").getOrElse { return it.toFailure() },

            anniversary = rs.getStringOrNull("anniversary"),
            birthday = rs.getStringOrNull("birthday"),
            organization = rs.getStringOrNull("organization"),
            orgUnits = rs.getStringOrNull("orgunits")?.split(",")?.toMutableList(),
            title = rs.getStringOrNull("title"),
            languages = rs.getStringOrNull("languages")?.split(",")?.toMutableList(),
            notes = rs.getStringOrNull("notes"),
            noteFormat = ContentFormat.fromString(rs.getStringOrNull("format")),

            // Feature: AttachmentSupport
            // FeatureTODO: Load attachments here

            // Feature: ContactCustomAttributes
            // FeatureTODO: Load contact's custom attributes

            id = RandomID.generate()
        ).apply {
            if (photoType != null && encodedPhoto.isNotEmpty())
                photo = Contact.Photo(photoType, encodedPhoto)
        }.toSuccess()
    }

    fun loadFormattedName(): Result<String> = loadMisc("formatted_name")
    fun saveFormattedName(formattedName: String): Throwable? {
        if (formattedName.isEmpty() || formattedName.length > 64)
            return BadValueException("Formatted name cannot be longer than 64 characters")
        saveMisc("formatted_name", formattedName)?.let { return it }
        return null
    }

    fun saveName(cname: ContactName): Throwable? {
        return DBProxy.get().execute(
            "UPDATE contacts SET formatted_name=?, given_name=?, family_name=?, middle_name=?, " +
                    "prefix=?, suffix=? WHERE id=?",
            cname.formattedName, cname.givenName ?: "", cname.familyName ?: "",
            cname.middleName ?: "", cname.prefix ?: "", cname.suffix ?: "", gUserConID
        )
    }

    fun loadGivenName(): Result<String> = loadMisc("given_name")

    fun saveGivenName(givenName: String): Throwable? {
        if (givenName.isEmpty())
            return BadValueException("First name cannot be empty")
        if (givenName.length > 64)
            return BadValueException("First name cannot be longer than 64 characters")
        saveMisc("given_name", givenName)?.let { return it }
        return null
    }

    fun loadFamilyName(): Result<String> = loadMisc("family_name")

    fun saveFamilyName(familyName: String): Throwable? {
        if (familyName.length > 64)
            return BadValueException("Last name cannot be longer than 64 characters")
        saveMisc("family_name", familyName)?.let { return it }
        return null
    }

    fun loadMAddress(): Result<String> {
        val uid = loadMisc("uid").getOrElse { return it.toFailure() }
        val domain = loadMisc("domain").getOrElse { return it.toFailure() }
        if (uid.isEmpty() || domain.isEmpty()) return "".toSuccess()
        return "$uid/$domain".toSuccess()
    }

    fun loadWAddress(): Result<String> {
        val wid = loadMisc("wid").getOrElse { return it.toFailure() }
        val domain = loadMisc("domain").getOrElse { return it.toFailure() }
        if (wid.isEmpty() || domain.isEmpty()) return "".toSuccess()
        return "$wid/$domain".toSuccess()
    }

    /**
     * Allows setting the user's UserID, Workspace ID, and Domain all in one call, a common use
     * case.
     */
    fun saveIdentity(uid: String?, wid: String?, domain: String?): Throwable? {
        return DBProxy.get()
            .execute(
                "UPDATE contacts SET uid=?,wid=?,domain=? WHERE id=?", uid ?: "",
                wid ?: "", domain ?: "", gUserConID
            )
    }

    fun loadMisc(field: String): Result<String> {
        if (field.isEmpty()) return EmptyDataException().toFailure()

        val db = DBProxy.get()
        val rs = db.query("SELECT $field FROM contacts WHERE id=?", gUserConID)
            .getOrElse { return it.toFailure() }

        if (!rs.next()) return "".toSuccess()

        return Result.success(rs.getString(1) ?: "")
    }

    fun saveMisc(field: String, value: String): Throwable? {
        if (field.isEmpty()) return EmptyDataException()

        val db = DBProxy.get()
        return if (value.isEmpty())
            db.execute("DELETE FROM contacts WHERE $field=?", field.lowercase())
        else
            db.execute("UPDATE contacts SET $field=? WHERE id=?", value, gUserConID)
    }

    /**
     * Loads MultiInfo fields, such as email addresses, phone numbers, and websites
     */
    fun loadMultiInfo(type: String, label: String): Result<String> {
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT * FROM contact_multi WHERE conid=? AND itemtype=? AND label=?",
            gUserConID, type, label
        ).getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        return (rs.getString("itemvalue") ?: "").toSuccess()
    }

    /**
     * Saves MultiInfo fields, such as email addresses, phone numbers, and websites
     */
    fun saveMultiInfo(type: String, label: String, value: String, preferred: Boolean): Throwable? {
        if (type.isEmpty() || label.isEmpty())
            return EmptyDataException("Type and label cannot be empty")

        return if (value.isEmpty())
            DBProxy.get().execute(
                "DELETE FROM contact_multi WHERE conid=? AND itemtype=?"
            )
        else
            DBProxy.get().execute(
                "MERGE INTO contact_multi(id,conid,label,itemtype,itemvalue,preferred) " +
                        "KEY(conid,label) VALUES(?,?,?,?,?,?)",
                RandomID.generate(), gUserConID, label, type, value, preferred
            )
    }

    /** Loads the user's profile photo. Returns null if one has not been set. */
    fun loadPhoto(): Result<Image?> {
        val rs = DBProxy.get().query("SELECT photo FROM contacts WHERE id=?", gUserConID)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        val stream = rs.getBinaryStream("photo") ?: return Result.success(null)
        return runCatching { Image(stream) }
    }

    /** Saves the user's profile photo to the database or removes it if passed null. */
    fun savePhoto(photo: Image?): Throwable? {
        val db = DBProxy.get()
        if (photo == null)
            return db.execute(
                "UPDATE contacts SET phototype=null,photo=null WHERE id=?", gUserConID
            )

        val image = SwingFXUtils.fromFXImage(photo, null)
        val binaryStream = ByteArrayOutputStream()
        ImageIO.write(image, "png", binaryStream)
        return db.execute(
            "UPDATE contacts SET phototype=?,photo=? WHERE id=?",
            "image/png", binaryStream.toByteArray(), gUserConID
        )
    }

    private fun loadLabeledField(type: String): Result<MutableList<Contact.LabeledField>> {
        val out = mutableListOf<Contact.LabeledField>()
        with(DBProxy.get()) {
            val rs = query(
                "SELECT * FROM contact_multi WHERE conid=? AND itemtype=? ORDER BY label",
                gUserConID, type
            ).getOrElse { return it.toFailure() }

            while (rs.next()) {
                out.add(
                    Contact.LabeledField(
                        rs.getString("label") ?: "",
                        rs.getString("itemvalue") ?: "",
                        rs.getBoolean("preferred")
                    )
                )
            }
        }
        return out.toSuccess()
    }

    private fun loadMailingAddresses(): Result<MutableList<Contact.MailingAddress>> {
        val out = mutableListOf<Contact.MailingAddress>()
        with(DBProxy.get()) {
            val rs = query(
                "SELECT * FROM contact_addresses WHERE conid=? ORDER BY label", gUserConID
            ).getOrElse { return it.toFailure() }

            while (rs.next()) {
                out.add(
                    Contact.MailingAddress(
                        rs.getString("label") ?: "",
                        rs.getString("street"),
                        rs.getString("extended"),
                        rs.getString("locality"),
                        rs.getString("region"),
                        rs.getString("postalcode"),
                        rs.getString("country"),
                        rs.getBoolean("preferred")
                    )
                )
            }
        }
        return out.toSuccess()
    }
}
