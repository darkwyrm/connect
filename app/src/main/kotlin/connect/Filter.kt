package connect

/**
 * The FilterTerm class represents one component of a search query, such as "label:inbox"
 */
class FilterTerm(val property: String, val comparator: FilterComparator, val value: String) {

    fun compare(str: String): Boolean {
        return when (comparator) {
            FilterComparator.Contains -> str.contains(value)
            FilterComparator.NotContains -> !str.contains(value)
        }
    }

    fun compareSet(set: Set<String>): Boolean {
        if (set.isEmpty()) return false
        return when (comparator) {
            FilterComparator.Contains -> set.contains(value)
            FilterComparator.NotContains -> !set.contains(value)
        }
    }

    companion object {
        /**
         * Creates a new FilterTerm instance from a string. The expected format is 'property:value'
         * or 'property:"value phrase"'.
         */
        fun fromString(s: String): FilterTerm? {
            val parts = s.split(':', limit = 2)

            val propName: String
            val filterValue: String
            when (parts.size) {
                0, 1 -> {
                    if (s.startsWith('-')) {
                        propName = "-contents"
                        filterValue = s.substring(1)
                    } else {
                        propName = "contents"
                        filterValue = s
                    }
                }

                2 -> {
                    propName = parts[0]
                    filterValue = parts[1].trim('"')
                }

                else -> return null
            }


            return if (propName.startsWith('-'))
                FilterTerm(propName, FilterComparator.NotContains, filterValue)
            else
                FilterTerm(propName, FilterComparator.Contains, filterValue)
        }
    }
}

/**
 * The FilterComparator class represents ways of filtering a value, such as if a value is contained
 * by a property or the property equals a specific value.
 */
enum class FilterComparator {
    Contains,
    NotContains;
}

enum class SortOrder {
    Ascending,
    Descending;
}

class FilterSort(var column: String, var order: SortOrder)

/** The Filter class is a collection of filtering terms */
class Filter(var name: String, val terms: MutableList<FilterTerm> = mutableListOf()) {
    var sorter = FilterSort("", SortOrder.Ascending)

    // Is the user allowed to delete the item?
    var readOnly = false

    fun add(termStr: String): FilterTerm? {
        val newTerm = FilterTerm.fromString(termStr)
        terms.add(newTerm ?: return null)
        return newTerm
    }

    override fun toString(): String = name

    fun sortBy(column: String, order: SortOrder = SortOrder.Ascending): Filter {
        sorter.column = column
        sorter.order = order
        return this
    }

    fun clone(): Filter = Filter(name, terms.toMutableList())

    companion object {
        fun fromStrings(name: String, vararg termStrings: String): Filter? {
            if (name.isEmpty()) return null

            val out = Filter(name)
            termStrings.forEach { out.add(it) ?: return null }

            return out
        }
    }
}

/** Turns a FilterTerm into the database-specific SQL string to add to the query */
fun termToString(term: FilterTerm): String {

    val bareProperty = if (term.property.startsWith('-')) {
        term.property.substring(1)
    } else {
        term.property
    }

    return when (term.comparator) {
        FilterComparator.Contains -> "$bareProperty ilike '%${term.value}%'"
        FilterComparator.NotContains -> "NOT $bareProperty ilike '%${term.value}%'"
    }
}

/** Builds the actual query string to be given to a prepStatement call */
fun buildQueryString(filter: Filter, table: String): String {
    val qb = mutableListOf("SELECT * FROM $table")
    if (filter.terms.isNotEmpty())
        qb.add("WHERE")

    filter.terms.forEachIndexed { i, term ->
        qb.apply {
            if (i > 0) add("AND")
            add(termToString(term))
        }
    }
    if (filter.sorter.column.isNotEmpty()) {
        qb.add("ORDER BY " + filter.sorter.column)
        if (filter.sorter.order == SortOrder.Descending)
            qb.add("DESCENDING")
    }

    return qb.joinToString(" ")
}
