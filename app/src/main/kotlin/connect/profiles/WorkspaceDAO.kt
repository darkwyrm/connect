package connect.profiles

import connect.KeyPurpose
import keznacl.*
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libkeycard.WAddress
import libmensago.ResourceExistsException
import libmensago.ResourceNotFoundException
import libmensago.WorkspaceStatus
import libmensago.db.DatabaseException
import utilities.DBDataException
import utilities.DBProxy
import utilities.getDomain
import utilities.getRandomID

/** The WorkspaceDAO class provides Workspace-related database access */
class WorkspaceDAO {
    fun initWorkspaceTables(): Throwable? {

        val initCommands = listOf(
            """CREATE TABLE IF NOT EXISTS appconfig(
                name VARCHAR(128) NOT NULL UNIQUE,
                itemvalue VARCHAR(256) NOT NULL,
                sync BOOL NOT NULL
            );""",
            """CREATE TABLE IF NOT EXISTS workspaces(
                rowid INT PRIMARY KEY AUTO_INCREMENT,
                wid TEXT NOT NULL UNIQUE,
                userid TEXT,
                domain TEXT,
                password TEXT,
                pwhashtype TEXT,
                type TEXT NOT NULL,
                status TEXT NOT NULL,
                devid TEXT NOT NULL,
                public_key TEXT NOT NULL,
                private_key TEXT NOT NULL
            );""",
        )
        val db = DBProxy.get()
        for (cmd in initCommands)
            db.add(cmd)?.let { return it }
        return db.executeBatch()
    }

    /**
     * Archives a workspace's data. This sets it aside as not usable for future correspondence, i.e.
     * message replies, etc., but it still is available for reference.
     */
    fun archive(waddr: WAddress): Throwable? {
        // This call probably doesn't belong here -- it probably needs to hoisted to the caller.
        KeyDAO().archiveKeys(waddr)?.let { return it }

        // Archive the workspace itself
        DBProxy.get().execute(
            "UPDATE workspaces SET status='archived' " +
                    "WHERE wid=? AND domain=? AND status='active'",
            waddr.id, waddr.domain
        )?.let { return it }

        return null
    }

    /** Utility function that just checks to see if a specific workspace exists in the database */
    fun checkWorkspaceExists(waddr: WAddress): Result<Boolean> {
        val rs = DBProxy.get().query(
            "SELECT wid FROM workspaces WHERE wid=? AND domain=?", waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }

        return rs.next().toSuccess()
    }

    /** Gets the password hash for the workspace */
    fun getCredentials(waddr: WAddress): Result<Password> {
        val rs = DBProxy.get().query(
            "SELECT password FROM workspaces WHERE wid=?1 AND domain=?2",
            waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }

        val hashStr = runCatching {
            rs.next().onFalse { return ResourceNotFoundException().toFailure() }
            rs.getString(1)
        }.getOrElse { return it.toFailure() }

        return getHasherForHash(hashStr).getOrElse { return it.toFailure() }.toSuccess()
    }

    /** Returns the device ID for a given workspace */
    fun getDeviceID(waddr: WAddress): Result<RandomID> {
        val rs = DBProxy.get().query(
            "SELECT devid FROM workspaces WHERE wid=? AND domain=?",
            waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }

        rs.next()
        val devid = rs.getRandomID("devid")
            ?: return DBDataException("Bad device id in getSessionID()").toFailure()
        return devid.toSuccess()
    }

    /** Returns the device key for a server session */
    fun getDeviceKeypair(waddr: WAddress): Result<EncryptionPair> {
        val rs = DBProxy.get().query(
            "SELECT public_key,private_key FROM workspaces WHERE wid=? AND domain=?",
            waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }

        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val pubKeyStr = rs.getString(1)
        val privKeyStr = rs.getString(2)
        val pubKeyCS = CryptoString.fromString(pubKeyStr)
            ?: return BadValueException("Bad public key: $pubKeyStr").toFailure()
        val privKeyCS = CryptoString.fromString(privKeyStr)
            ?: return BadValueException("Bad private key: $privKeyStr").toFailure()
        return EncryptionPair.from(pubKeyCS, privKeyCS)
    }

    /** Gets the status of the specified workspace or null if not found */
    fun getStatus(waddr: WAddress): Result<WorkspaceStatus?> {
        val rs = DBProxy.get().query(
            "SELECT status FROM workspaces WHERE wid=? AND domain=?", waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }
        rs.next().onFalse { return Result.success(null) }
        val out = WorkspaceStatus.fromString(rs.getString("status"))
            ?: return DatabaseException("Bad workspace status ${rs.getString("status")}")
                .toFailure()
        return out.toSuccess()
    }

    /**
     * Loads workspace information from the database. If no workspace ID is specified,
     * the identity workspace for the profile is loaded.
     */
    fun load(workspaceID: RandomID?): Result<Workspace> {
        val db = DBProxy.get()
        val widStr = if (workspaceID != null) workspaceID.toString() else {
            val rs = db.query(
                "SELECT wid FROM workspaces WHERE type = 'identity' AND status='active'"
            ).getOrElse { return it.toFailure() }
            rs.next().onFalse { return MissingDataException().toFailure() }
            rs.getString("wid")
        }

        val rs = db.query("SELECT * FROM workspaces WHERE wid = ?1", widStr)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return MissingDataException().toFailure() }

        val wid = RandomID.fromString(widStr)
            ?: return DBDataException("Bad workspace ID in database").toFailure()
        val domain = rs.getDomain("domain")
            ?: return DBDataException("Bad domain in database").toFailure()
        val devid = rs.getRandomID("devid")
            ?: return DBDataException("Bad device ID in database").toFailure()
        val status = WorkspaceStatus.fromString(rs.getString("status"))
            ?: return DBDataException("Bad status in database").toFailure()
        val devpair = EncryptionPair.fromStrings(
            rs.getString("public_key"),
            rs.getString("private_key")
        ).getOrElse { return it.toFailure() }

        val uidStr = rs.getString("userid")
        val uid = if (uidStr.isNullOrEmpty()) null
        else {
            UserID.fromString(uidStr)
                ?: return DBDataException("Bad user ID in database").toFailure()
        }

        val out = Workspace(wid, domain, uid, devid, devpair, status).apply {
            pwHash = rs.getString("password")
        }

        return out.toSuccess()
    }

    /**
     * Adds the workspace instance to the storage database as the profile's identity workspace. You
     * likely will never use this call unless you're working with the low-level APIs because this
     * method is automatically called by Profile::setIdentity().
     */
    fun save(w: Workspace, pwHash: Password?): Throwable? {
        val db = DBProxy.get()
        val rs = db.query("SELECT wid FROM workspaces WHERE type='identity'")
            .getOrElse { return it }

        if (rs.next()) {
            val oldWid = rs.getRandomID("wid")
                ?: return DBDataException("Bad identity WID in Workspace::addToDB()")

            if (oldWid != LOCAL_WID) {
                // An existing workspace is in use. It needs to be archived before a new one can be
                // put into production.
                return ResourceExistsException()
            }
        }

        // There can be only 1 identity workspace on a profile
        db.execute("DELETE FROM workspaces WHERE type='identity'")?.let { return it }

        if (w.uid != null) {
            db.execute(
                "INSERT INTO workspaces(wid,userid,domain,password,pwhashtype,type, " +
                        "status,devid,public_key,private_key) VALUES(?,?,?,?,?,'identity',?,?,?,?)",
                w.wid, w.uid!!, w.domain, pwHash ?: "", pwHash?.getAlgorithm() ?: "",
                w.status, w.devid, w.devpair.pubKey, w.devpair.privKey
            )?.let { return it }
        } else {
            db.execute(
                "INSERT INTO workspaces(wid,domain,password,pwhashtype,type,status, " +
                        "devid,public_key,private_key) VALUES(?,?,?,?,'identity',?,?,?,?)",
                w.wid, w.domain, pwHash ?: "", pwHash?.getAlgorithm() ?: "", w.status,
                w.devid, w.devpair.pubKey, w.devpair.privKey
            )?.let { return it }
        }

        return null
    }

    /** Saves all keys and assigns them to a specific workspace */
    fun saveKeyData(waddress: WAddress, keyData: WorkspaceKeyData): Throwable? {
        with(KeyDAO()) {
            saveKeypair(waddress, keyData.crePair, KeyPurpose.ConReqEncryption)
                .getOrElse { return it }
            saveKeypair(waddress, keyData.crsPair, KeyPurpose.ConReqSigning)
                .getOrElse { return it }
            saveKeypair(waddress, keyData.ePair, KeyPurpose.Encryption).getOrElse { return it }
            saveKeypair(waddress, keyData.sPair, KeyPurpose.Signing).getOrElse { return it }
            saveSecretKey(waddress, keyData.folderKey, KeyPurpose.Folder).getOrElse { return it }
            saveSecretKey(waddress, keyData.storageKey, KeyPurpose.Storage).getOrElse { return it }
        }

        return null
    }

    /** Updates the status of the specified workspace */
    fun setStatus(waddr: WAddress, status: WorkspaceStatus): Throwable? {
        return DBProxy.get().execute(
            "UPDATE workspaces SET status=? WHERE wid=? AND domain=?",
            status, waddr.id, waddr.domain
        )
    }

    /**
     * Sets the user ID in the database. Requires that the workspace's workspace ID and domain have
     * been set and will return an error if either is null.
     */
    fun setUserID(uid: UserID?, wid: RandomID, domain: Domain): Throwable? {
        DBProxy.get().execute(
            "UPDATE workspaces SET userid=? where wid=? AND domain=?",
            uid ?: "", wid, domain
        )
        return null
    }

    /** Updates a device key used for the identity workspace */
    fun updateDeviceKeypair(oldPair: EncryptionPair, newPair: EncryptionPair): Throwable? {
        return DBProxy.get().execute(
            "UPDATE workspaces SET (public_key,private_key)=(?,?) where public_key=?",
            newPair.pubKey, newPair.privKey, oldPair.pubKey
        )
    }
}
