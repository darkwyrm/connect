package connect.profiles

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleSetProperty
import javafx.beans.property.SimpleStringProperty
import libmensago.WorkspaceStatus
import utilities.toObservable

/** ProfilesModel contains profile-related state at runtime */
class ProfilesModel {
    val profileNamesProperty = SimpleSetProperty(sortedSetOf<String>().toObservable())
    var profileNames
        get() = profileNamesProperty.get()
        set(value) = profileNamesProperty.set(value)

    val activeProfileProperty = SimpleObjectProperty<Profile>()
    var activeProfile: Profile?
        get() = activeProfileProperty.get()
        set(value) = activeProfileProperty.set(value)

    val activeProfilePathProperty = SimpleObjectProperty<String>()
    var activeProfilePath: String?
        get() = activeProfilePathProperty.get()
        set(value) = activeProfilePathProperty.set(value)

    val activeIdentityStatusProperty =
        SimpleObjectProperty(WorkspaceStatus.Local)
    var activeIdentityStatus: WorkspaceStatus
        get() = activeIdentityStatusProperty.get()
        set(value) = activeIdentityStatusProperty.set(value)

    val defaultProfileProperty = SimpleStringProperty("")
    var defaultProfile
        get() = defaultProfileProperty.get()
        set(value) = defaultProfileProperty.set(value)
}
