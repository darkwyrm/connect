package connect.profiles

import keznacl.EncryptionPair
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.*
import libmensago.DeviceInfo
import libmensago.WorkspaceStatus

class Workspace(
    var wid: RandomID, var domain: Domain, var uid: UserID?, var devid: RandomID,
    var devpair: EncryptionPair, var status: WorkspaceStatus,
) {

    /**
     * The password hash for the workspace. This field is usually not populated for security
     * reasons.
     */
    var pwHash = ""

    /** The Mensago (human-friendly) address for the workspace */
    val maddress: MAddress
        get() {
            return if (uid == null)
                MAddress.fromWAddress(waddress)
            else
                MAddress.fromParts(uid!!, domain)
        }

    /** The numeric address for the workspace */
    val waddress: WAddress
        get() {
            return WAddress.fromParts(wid, domain)
        }

    companion object {
        /**
         * Creates a new workspace instance from all the data needed for an individual workspace
         * from the required account data.
         */
        fun generate(userID: UserID?, dom: Domain, wkspcID: RandomID, passwordHash: String):
                Result<Pair<Workspace, WorkspaceKeyData>> {

            val devid = RandomID.generate()
            val devpair = EncryptionPair.generate().getOrElse { return it.toFailure() }
            val out = Workspace(wkspcID, dom, userID, devid, devpair, WorkspaceStatus.Unknown)
            out.pwHash = passwordHash

            val keydata = WorkspaceKeyData.generate().getOrElse { return it.toFailure() }

            return Pair(out, keydata).toSuccess()
        }

        /** Creates a workspace to the database which is pending approval. */
        fun newPending(
            userID: UserID?, dom: Domain, wkspcID: RandomID, passwordHash: String,
            devInfo: DeviceInfo
        ): Result<Workspace> {
            return Workspace(
                wkspcID,
                dom,
                userID,
                devInfo.id,
                devInfo.getKeyPair() ?: return NullPointerException().toFailure(),
                WorkspaceStatus.Pending
            ).apply { pwHash = passwordHash }.toSuccess()
        }
    }
}
