package connect.profiles

import keznacl.*
import kotlinx.serialization.Serializable

@Serializable
data class WorkspaceKeyData(
    val crePair: EncryptionPair, val crsPair: SigningPair,
    val ePair: EncryptionPair, val sPair: SigningPair, val folderKey: SecretKey,
    val storageKey: SecretKey
) {

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("CREncPair: $crePair\n")
        sb.append("CRSSignPair: $crsPair\n")
        sb.append("EncPair: $ePair\n")
        sb.append("SignPair: $sPair\n")
        sb.append("FolderKey: $folderKey\n")
        sb.append("StorageKey: $storageKey\n")

        return sb.toString()
    }

    companion object {

        /**
         * Creates a new workspace instance from all the data needed for an individual workspace
         * from the required account data.
         */
        fun generate(): Result<WorkspaceKeyData> {

            return WorkspaceKeyData(
                EncryptionPair.generate().getOrElse { return it.toFailure() },
                SigningPair.generate().getOrElse { return it.toFailure() },
                EncryptionPair.generate().getOrElse { return it.toFailure() },
                SigningPair.generate().getOrElse { return it.toFailure() },
                SecretKey.generate().getOrElse { return it.toFailure() },
                SecretKey.generate().getOrElse { return it.toFailure() }
            ).toSuccess()
        }
    }
}