package connect.profiles

import connect.ContactKeySet
import connect.KeyInfo
import connect.KeyPurpose
import connect.contacts.ContactDAO
import keznacl.*
import libkeycard.RandomID
import libkeycard.Timestamp
import libkeycard.UserKeySet
import libkeycard.WAddress
import libmensago.ResourceNotFoundException
import libmensago.db.DatabaseException
import utilities.DBDataException
import utilities.DBProxy
import utilities.getCryptoString
import java.sql.ResultSet

/** The KeyDAO class encapsulates all the functionality needed to manage keys in the database */
class KeyDAO {
    fun initKeyTables(): Throwable? {
        return DBProxy.get().execute(
            """CREATE table keys (
                id UUID NOT NULL UNIQUE,
                keyid TEXT NOT NULL,
                address TEXT,
                conid UUID,
                type TEXT NOT NULL,
                purpose TEXT NOT NULL,
                private TEXT,
                public TEXT,
                status TEXT NOT NULL,
                timestamp TEXT NOT NULL,
                FOREIGN KEY (conid) REFERENCES contacts(id) ON DELETE CASCADE
            );"""
        )
    }

    /**
     * Imports a set of relationship keys into the database for a specified contact.
     */
    fun addKeySetForContact(conid: RandomID, keySet: ContactKeySet): Throwable? {
        saveKeypair(conid, keySet.encryptionPair, KeyPurpose.RelMyEncryption)
            .getOrElse { return it }

        saveKeypair(conid, keySet.signingPair, KeyPurpose.RelMySigning).getOrElse { return it }

        return null
    }

    /**
     * Archives a key or keypair, given its hash. Archiving is a one-way action, so do not call this
     * function unless needed. As a general rule, this call is not generally needed, as adding a new key
     * in a purpose automatically archives the old one.
     */
    fun archiveKey(keyHash: Hash): Throwable? {
        return DBProxy.get().execute("UPDATE keys SET status='archived' WHERE keyid=?", keyHash)
    }

    /** Archives all keys for a specific workspace */
    fun archiveKeys(waddr: WAddress): Throwable? {
        return DBProxy.get().execute(
            "UPDATE keys SET status='archived' WHERE address=? AND status='active'", waddr
        )
    }

    /**
     * Archives a keypair. As a general rule, this call is not generally needed, as adding a new keypair
     * in a purpose automatically archives the old one.
     */
    fun archiveKeypair(keyHash: Hash): Throwable? = archiveKey(keyHash)

    /**
     * Deletes a cryptography key from a workspace. This call is used in extremely rare cases, as keys
     * that are no longer in use are archived or have been revoked. Keys are archived in case they are
     * needed at a future time.
     *
     * Note that the hash algorithm is expected to match, i.e. if a key is stored using a BLAKE2B-256
     * hash, passing a BLAKE3-256 hash of the exact same key will result in a ResourceNotFoundException
     * error.
     */
    fun deleteKey(keyHash: Hash): Throwable? {
        return DBProxy.get().execute("DELETE FROM keys WHERE keyid=?1", keyHash)
    }

    /**
     * Deletes a keypair from the profile's key list. Like removeKey(), this is almost certainly not
     * what you really want to do. More likely, you probably want archiveKeypair() or revokeKeypair()
     */
    fun deleteKeypair(keyHash: Hash): Throwable? = deleteKey(keyHash)

    /**
     * Loads the encryption key used for communications with a specific workspace address
     *
     * @exception ResourceNotFoundException If the address is not in the user's address book
     * @exception DatabaseException For a corrupted contact key
     */
    fun loadContactKey(waddr: WAddress): Result<EncryptionKey> {
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT keys.public FROM keys " +
                    "INNER JOIN contacts ON contacts.id=keys.conid " +
                    "WHERE contacts.wid=? AND contacts.domain=? AND keys.purpose=?",
            waddr.id, waddr.domain, KeyPurpose.RelEncryption
        ).getOrElse { return Result.failure(it) }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        val cs = CryptoString.fromString(rs.getString("public"))
            ?: return DatabaseException("Bad encryption key for $waddr").toFailure()
        return cs.toEncryptionKey()
    }

    /** Returns all keys in the profile */
    fun loadKeyList(): Result<List<WorkspaceKey>> {
        val rs = DBProxy.get().query("SELECT * FROM keys WHERE address IS NOT NULL")
            .getOrElse { return it.toFailure() }
        val out = mutableListOf<WorkspaceKey>()
        while (rs.next()) {
            val keyid = Hash.fromString(rs.getString("keyid"))
                ?: return DatabaseException("Bad key hash ${rs.getString("keyid")}")
                    .toFailure()
            val address = WAddress.fromString(rs.getString("address"))
                ?: return DatabaseException("Bad address ${rs.getString("address")}")
                    .toFailure()
            val type = rs.getString("type")
            val purpose = KeyPurpose.fromString(rs.getString("purpose"))
                ?: return DatabaseException("Bad key purpose ${rs.getString("purpose")}")
                    .toFailure()
            val pubkey = CryptoString.fromString(rs.getString("public"))
                ?: return DatabaseException("Bad public key ${rs.getString("public")}")
                    .toFailure()
            val privkey = CryptoString.fromString(rs.getString("private"))
                ?: return DatabaseException("Bad private key ${rs.getString("private")}")
                    .toFailure()
            val status = rs.getString("status")
            val timestamp = Timestamp.fromString(rs.getString("timestamp"))
                ?: return DatabaseException("Bad timestamp ${rs.getString("timestamp")}")
                    .toFailure()

            out.add(
                WorkspaceKey(keyid, address, type, purpose, pubkey, privkey, status, timestamp)
            )
        }
        return out.toSuccess()
    }

    /**
     * Returns a pair of 2 CryptoStrings, where the public key is in element 0 and the private key is in
     * element 1. This is to accommodate retrieval of all key types. If a symmetric key is obtained
     * through this call, the public and private key values will be the same.
     */
    fun loadKeypair(keyHash: Hash): Result<Pair<CryptoString, CryptoString>> {
        val rs = DBProxy.get().query("SELECT public,private FROM keys WHERE keyid=?", keyHash)
            .getOrElse { return it.toFailure() }
        return processKeypairQuery(rs)
    }

    /** Convenience method for quickly getting an asymmetric encryption keypair based on its hash. */
    fun loadEncryptionPair(keyHash: Hash): Result<EncryptionPair> {
        val rs = DBProxy.get().query(
            "SELECT public,private FROM keys WHERE keyid=? AND type='asymmetric'",
            keyHash
        ).getOrElse { return it.toFailure() }
        val pair = processKeypairQuery(rs).getOrElse { return it.toFailure() }
        return EncryptionPair.from(pair.first, pair.second)
    }

    /**
     * Convenience method to get an asymmetric encryption keypair owned by the user based on its
     * purpose. Like the other calls, this only returns active keypairs.
     */
    fun loadRelEncryptionPair(waddr: WAddress, keyPurpose: KeyPurpose): Result<EncryptionPair> {
        val rs = DBProxy.get().query(
            "SELECT public,private FROM keys WHERE purpose=? AND status='active' " +
                    "AND type='asymmetric' AND address=?", keyPurpose, waddr
        ).getOrElse { return it.toFailure() }
        val pair = processKeypairQuery(rs).getOrElse { return it.toFailure() }
        return EncryptionPair.from(pair.first, pair.second)
    }

    /** Loads the user's relationship encryption keypair associated with the contact. */
    fun loadRelEncryptionPair(conid: RandomID): Result<EncryptionPair> {
        val pubcs = loadKeyString(conid, KeyPurpose.RelMyEncryption, true)
            .getOrElse { return it.toFailure() }
        val privcs = loadKeyString(conid, KeyPurpose.RelMyEncryption, false)
            .getOrElse { return it.toFailure() }
        return EncryptionPair.from(pubcs, privcs)
    }

    /**
     * Gets a key given its hash. As with getKeypair(), if the hash given does not use the same
     * algorithm, this function will not find the key. This call is generally used to obtain a secret
     * key, but it will also return the public key of a keypair if given the corresponding hash.
     */
    fun loadKey(keyHash: Hash): Result<PublicKey> {

        val rs = DBProxy.get().query("""SELECT public,type FROM keys WHERE keyid=?""", keyHash)
            .getOrElse { return it.toFailure() }

        if (!rs.next()) return ResourceNotFoundException().toFailure()
        return when (rs.getString("type").lowercase()) {
            "symmetric" -> return SecretKey.fromString(rs.getString("public"))
            "asymmetric" -> return EncryptionKey.fromString(rs.getString("public"))
            "signing" -> return VerificationKey.fromString(rs.getString("public"))
            else -> DBDataException("Bad key type in getKey()").toFailure()
        }
    }

    /** Returns the status of a key or keypair in the database */
    fun loadKeyStatus(keyHash: Hash): Result<String> {

        val rs = DBProxy.get().query("SELECT status FROM keys WHERE keyid=?", keyHash)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        return rs.getString("status").toSuccess()
    }

    /**
     * Returns keys and keypairs based on their status. This can be 'archived', 'revoked', or 'active'.
     */
    fun loadKeysByStatus(status: String): Result<List<KeyInfo>> {
        when (status) {
            "active", "revoked", "archived" -> {}
            else -> return BadValueException("Status must be 'archived', 'active', or 'revoked'")
                .toFailure()
        }

        val rs = DBProxy.get().query(
            "SELECT keyid,address,type,purpose,public,private,timestamp " +
                    "FROM keys WHERE status=?", status
        ).getOrElse { return it.toFailure() }
        val out = mutableListOf<KeyInfo>()
        while (rs.next()) {
            val pubHash = rs.getCryptoString("keyid")
                ?: return DBDataException("bad keyid in getKeysByStatus()").toFailure()
            val address = WAddress.fromString(rs.getString("address"))
                ?: return DBDataException("bad address in getKeysByStatus()").toFailure()
            val keyType = rs.getString("type")
            when (keyType) {
                "asymmetric", "symmetric", "signing" -> {}
                else -> return DBDataException("bad type in getKeysByStatus()").toFailure()
            }
            val purpose = KeyPurpose.fromString(rs.getString("purpose"))
                ?: return DBDataException("bad purpose in getKeysByStatus()").toFailure()
            val pubKey = rs.getCryptoString("public")
                ?: return DBDataException("bad public key in getKeysByStatus()")
                    .toFailure()
            val privKey = rs.getCryptoString("private")
                ?: return DBDataException("bad private key in getKeysByStatus()")
                    .toFailure()
            val timestamp = rs.getString("timestamp")

            out.add(
                KeyInfo(keyType, address, pubKey, pubHash, privKey, purpose, status, timestamp)
            )
        }
        return out.toSuccess()
    }

    /**
     * Returns a key and its hash identifier, given its purpose. If the purpose given uses a pair of
     * keys, the public key is returned. This call only returns active keys and ignores archived ones.
     */
    fun loadKeyByPurpose(purpose: KeyPurpose): Result<Pair<CryptoString, Hash>> {

        val rs = DBProxy.get().query(
            "SELECT public,keyid FROM keys WHERE purpose=? AND status='active'",
            purpose.toString()
        ).getOrElse { return it.toFailure() }

        if (!rs.next()) return ResourceNotFoundException().toFailure()
        val pubCS = rs.getCryptoString("public")
            ?: return DBDataException("Bad key for purpose '$purpose' in database")
                .toFailure()

        val keyID = Hash.fromString(rs.getString("keyid"))
            ?: return DBDataException("Bad keyid for purpose '$purpose' in database")
                .toFailure()

        return Result.success(Pair(pubCS, keyID))
    }

    /**
     * Returns a keypair based on their usage, with the public key is the first of the pair's elements.
     * Note that this method only returns active keypairs and ignores any archived ones.
     */
    fun loadKeypairByPurpose(keyPurpose: KeyPurpose): Result<Pair<CryptoString, CryptoString>> {
        val rs = DBProxy.get().query(
            "SELECT public,private FROM keys WHERE purpose=? AND status='active'",
            keyPurpose
        ).getOrElse { return it.toFailure() }
        return processKeypairQuery(rs)
    }

    /**
     * Loads a CryptoString representing the requested key. This method primarily exists as a
     * backend for loadSigningPair() and the like, although it can be useful in other instances.
     */
    fun loadKeyString(
        conid: RandomID, purpose: KeyPurpose, isPublic: Boolean, status: String = "active"
    ): Result<CryptoString> {

        val query = if (isPublic)
            "SELECT public FROM keys WHERE conid=? AND purpose=? AND status=?"
        else
            "SELECT private FROM keys WHERE conid=? AND purpose=? AND status=?"
        val rs = DBProxy.get().query(query, conid, purpose, status)
            .getOrElse { return it.toFailure() }

        rs.next().onFalse {
            return ResourceNotFoundException("Encryption key for $conid").toFailure()
        }

        val keyStr = rs.getString(1) ?: return ResourceNotFoundException().toFailure()
        val out = CryptoString.fromString(keyStr)
            ?: return DatabaseException("Bad key $keyStr").toFailure()
        return out.toSuccess()
    }

    /** Loads the key used to encrypt messages to the contact */
    fun loadContactEncryptionKey(conid: RandomID): Result<EncryptionKey> {
        return loadKeyString(conid, KeyPurpose.RelEncryption, true)
            .getOrElse { return it.toFailure() }
            .toEncryptionKey()
    }

    /** Loads the user's encryption keypair associated with messages from the contact. */
    fun loadRelEncryptionPair(hash: Hash): Result<EncryptionPair> {
        val db = DBProxy.get()
        val rs = db.query("SELECT conid FROM keys WHERE hash=?", hash)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        val idStr = rs.getString("conid")
        val conid = RandomID.fromString(idStr)
            ?: return DatabaseException("Bad contact ID '$idStr'").toFailure()
        return loadRelEncryptionPair(conid)
    }

    /** Loads the verification key for messages from the contact. */
    fun loadContactVerificationKey(conid: RandomID): Result<VerificationKey> {
        return loadKeyString(conid, KeyPurpose.RelSigning, true)
            .getOrElse { return it.toFailure() }
            .toVerificationKey()
    }

    /** Loads the user's keypair for signing messages to the contact. */
    fun loadRelSigningPair(conid: RandomID): Result<SigningPair> {
        val pubcs = loadKeyString(conid, KeyPurpose.RelMySigning, true)
            .getOrElse { return it.toFailure() }
        val privcs = loadKeyString(conid, KeyPurpose.RelMySigning, false)
            .getOrElse { return it.toFailure() }
        return SigningPair.from(pubcs, privcs)
    }

    /** Convenience method for quickly getting a signing keypair based on its hash. */
    fun loadSigningPair(keyHash: Hash): Result<SigningPair> {
        val rs = DBProxy.get().query(
            "SELECT public,private FROM keys WHERE keyid=? AND type='signing'",
            keyHash
        ).getOrElse { return it.toFailure() }
        val pair = processKeypairQuery(rs).getOrElse { return it.toFailure() }
        return SigningPair.from(pair.first, pair.second)
    }

    /**
     * Convenience method to get a signing keypair based on its purpose. Like the other byCategory
     * calls, this only returns active keypairs.
     */
    fun loadSigningPair(waddr: WAddress, keyPurpose: KeyPurpose): Result<SigningPair> {
        val rs = DBProxy.get().query(
            "SELECT public,private FROM keys WHERE purpose=? AND status='active' " +
                    "AND type='signing' AND address=?", keyPurpose, waddr
        ).getOrElse { return it.toFailure() }
        val pair = processKeypairQuery(rs).getOrElse { return it.toFailure() }
        return SigningPair.from(pair.first, pair.second)
    }

    fun loadKeySet(waddr: WAddress): Result<UserKeySet> {
        return UserKeySet(
            loadRelEncryptionPair(waddr, KeyPurpose.ConReqEncryption)
                .getOrElse { return it.toFailure() },
            loadSigningPair(waddr, KeyPurpose.ConReqSigning).getOrElse { return it.toFailure() },
            loadRelEncryptionPair(waddr, KeyPurpose.Encryption).getOrElse { return it.toFailure() },
            loadSigningPair(waddr, KeyPurpose.Signing).getOrElse { return it.toFailure() },
        ).toSuccess()
    }

    /**
     * Migrates the relationship keys used for a contact request after it has been approved by the
     * other entity. This method must be called *after* importing the contact's information into the
     * database, as it expects the contact's ID to already exist.
     */
    fun migrateCRKeys(address: WAddress): Throwable? {
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT id FROM contacts WHERE wid=? AND domain=?", address.id, address.domain
        ).getOrElse { return it }
        rs.next().onFalse { return ResourceNotFoundException() }

        val idStr = rs.getString("id")
        val id = RandomID.fromString(idStr)
            ?: return DatabaseException(
                "Bad contact id '$idStr' in database during CR key migration "
            )

        val keySet = ContactDAO().loadPendingCRKeys(address).getOrElse { return it }
        addKeySetForContact(id, keySet)?.let { return it }

        return db.execute("DELETE FROM contact_requests WHERE address=?", address)
    }

    /** Internal function called by both getKeypair and getKeypairByCategory() */
    private fun processKeypairQuery(rs: ResultSet): Result<Pair<CryptoString, CryptoString>> {
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val pubKeyStr = rs.getString(1)
        val pubKeyCS = CryptoString.fromString(pubKeyStr)
            ?: return BadValueException("Bad public key: $pubKeyStr").toFailure()

        val privKeyStr = rs.getString(2)
        val privKeyCS = CryptoString.fromString(privKeyStr)
            ?: return BadValueException("Bad private key: $privKeyStr").toFailure()

        return Pair(pubKeyCS, privKeyCS).toSuccess()
    }

    /**
     * Saves a relationship key into the database and retains any old keys in the purpose for the
     * contact. Only one previous relationship key is retained.
     */
    fun replaceKey(
        conid: RandomID, purpose: KeyPurpose, key: CryptoString, isPublic: Boolean
    ): Throwable? {
        val db = DBProxy.get()
        db.execute(
            "DELETE FROM keys WHERE conid=? AND purpose=? AND status='archived'", conid, purpose
        )?.let { return it }

        db.execute(
            "UPDATE keys SET status='archived' WHERE conid=? AND purpose=?", conid, purpose
        )?.let { return it }

        return saveKey(conid, purpose, key, isPublic)
    }

    /**
     * Revokes a key or keypair, given its hash. Like archiving, this is a one-way action, so consider
     * carefully calling this function.
     */
    fun revokeKey(keyHash: Hash): Throwable? =
        DBProxy.get().execute("UPDATE keys SET status='revoked' WHERE keyid=?", keyHash)

    /** Revokes a keypair, which is a one-way action. */
    fun revokeKeypair(keyHash: Hash): Throwable? = revokeKey(keyHash)

    /**
     * Saves a relationship key into the database. Note that this call will overwrite any existing
     * key with the specified purpose. If you have any concern about losing a key here, consider
     * calling replaceKey() instead.
     */
    fun saveKey(
        conid: RandomID, purpose: KeyPurpose, key: CryptoString, isPublic: Boolean
    ): Throwable? {

        val keyType = key.getType()
            ?: return UnsupportedAlgorithmException("Bad key algorithm ${key.prefix}")
        val dbtype = when {
            keyType.isSigning() -> "signing"
            keyType.isAsymmetric() -> "asymmetric"
            keyType.isSymmetric() -> "symmetric"
            else -> return UnsupportedAlgorithmException("Bad key algorithm ${key.prefix}")
        }
        val cmd = if (isPublic)
            "MERGE INTO keys(id,keyid,conid,type,purpose,public,timestamp,status) " +
                    "KEY(id) VALUES(?,?,?,?,?,?,?,'active')"
        else
            "MERGE INTO keys(id,keyid,conid,type,purpose,private,timestamp,status) " +
                    "KEY(id) VALUES(?,?,?,?,?,?,?,'active')"

        val keyHash = key.hash().getOrElse { return it }
        return DBProxy.get().execute(
            cmd, RandomID.generate(), keyHash, conid, dbtype, purpose, key, Timestamp()
        )
    }

    /** Adds the keys from an keycard entry to the specified workspace */
    fun saveEntryKeys(waddress: WAddress, keyData: UserKeySet): Throwable? {
        saveKeypair(waddress, keyData.getCREPair(), KeyPurpose.ConReqEncryption)
            .getOrElse { return it }
        saveKeypair(waddress, keyData.getCRSPair(), KeyPurpose.ConReqSigning)
            .getOrElse { return it }
        saveKeypair(waddress, keyData.getEPair(), KeyPurpose.Encryption).getOrElse { return it }
        saveKeypair(waddress, keyData.getSPair(), KeyPurpose.Signing).getOrElse { return it }

        return null
    }

    /** Adds an asymmetric keypair to a workspace */
    fun saveKeypair(waddr: WAddress, keyPair: KeyPair, keyPurpose: KeyPurpose): Result<Hash> {
        val db = DBProxy.get()
        db.execute(
            "UPDATE keys SET status='archived' " +
                    "WHERE address=? AND purpose=? AND status='active'", waddr, keyPurpose
        )?.let { return it.toFailure() }

        val pubHash = keyPair.getPublicHash().getOrElse { return it.toFailure() }
        val timestamp = Timestamp()

        val typeStr = if (keyPair is Encryptor) "asymmetric" else "signing"

        db.execute(
            "INSERT INTO keys(id,keyid,address,type,purpose,private,public,status,timestamp)" +
                    "VALUES(?1,?2,?3,?4,?5,?6,?7,'active',?8)",
            RandomID.generate(), pubHash, waddr, typeStr, keyPurpose, keyPair.getPrivateKey(),
            keyPair.getPublicKey(), timestamp
        )?.let { return it.toFailure() }

        return pubHash.toSuccess()
    }

    /** Adds an asymmetric keypair to a workspace */
    fun saveKeypair(conid: RandomID, keyPair: KeyPair, keyPurpose: KeyPurpose): Result<Hash> {

        val pubHash = keyPair.getPublicHash().getOrElse { return it.toFailure() }
        val timestamp = Timestamp()

        val typeStr = if (keyPair is Encryptor) "asymmetric" else "signing"

        DBProxy.get().execute(
            "INSERT INTO keys(id,keyid,conid,type,purpose,private,public,status,timestamp)" +
                    "VALUES(?1,?2,?3,?4,?5,?6,?7,'active',?8)",
            RandomID.generate(), pubHash, conid, typeStr, keyPurpose, keyPair.getPrivateKey(),
            keyPair.getPublicKey(), timestamp
        )?.let { return it.toFailure() }

        return pubHash.toSuccess()
    }

    /**
     * Adds a symmetric (secret) key to the database and its associated purpose and returns the hash
     * identifier for the key.
     */
    fun saveSecretKey(waddr: WAddress, key: SecretKey, keyPurpose: KeyPurpose): Result<Hash> {
        val db = DBProxy.get()
        db.execute(
            "UPDATE keys SET status='archived' " +
                    "WHERE address=? AND purpose=? AND status='active'", waddr, keyPurpose
        )?.let { return it.toFailure() }

        val pubHash = key.getHash().getOrElse { return it.toFailure() }
        val timestamp = Timestamp()

        db.execute(
            "INSERT INTO keys(id,keyid,address,type,purpose,private,public,status,timestamp) " +
                    "VALUES(?1,?2,?3,?4,?5,?6,?7,'active',?8)",
            RandomID.generate(), pubHash, waddr, "symmetric", keyPurpose, key, key, timestamp
        )?.let { return it.toFailure() }

        return pubHash.toSuccess()
    }

    /** Convenience function which adds a UserKeySet instance to the database. */
    fun saveUserKeySet(waddr: WAddress, keys: UserKeySet): Throwable? {
        saveKeypair(waddr, keys.getCRSPair(), KeyPurpose.ConReqSigning)
            .getOrElse { return it }
        saveKeypair(waddr, keys.getCREPair(), KeyPurpose.ConReqEncryption)
            .getOrElse { return it }
        saveKeypair(waddr, keys.getSPair(), KeyPurpose.Signing)
            .getOrElse { return it }
        saveKeypair(waddr, keys.getEPair(), KeyPurpose.Encryption)
            .getOrElse { return it }
        return null
    }

    /** Adds a list of WorkspaceKey instances to the profile */
    fun saveWorkspaceKeys(list: List<WorkspaceKey>): Throwable? {
        list.forEach {
            with(it) {
                DBProxy.get().execute(
                    "MERGE INTO keys(id,keyid,address,type,purpose,public,private,status,timestamp) " +
                            "KEY(keyid) VALUES(?,?,?,?,?,?,?,?,?)",
                    RandomID.generate(), keyid, address, type, purpose, pubkey, privkey, status,
                    timestamp
                )?.let { e -> return e }
            }
        }
        return null
    }

    /**
     * Internal function to assign a keypair based on purpose. This call exists because an
     * encryption keypair is needed to register a device before a workspace is created.
     */
    internal fun updateKeypairByPurpose(
        waddr: WAddress, keyPair: KeyPair,
        keyPurpose: KeyPurpose
    ): Throwable? {

        val pubHash = keyPair.getPublicHash().getOrElse { return it }
        val timestamp = Timestamp()

        return DBProxy.get().execute(
            "UPDATE keys SET keyid=?,public=?,private=?,timestamp=? " +
                    "WHERE address=? AND purpose=? AND status='active'",
            pubHash, keyPair.getPublicKey(), keyPair.getPrivateKey(), timestamp, waddr, keyPurpose
        )
    }
}
