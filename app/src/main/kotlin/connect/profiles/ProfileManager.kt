package connect.profiles

import keznacl.EmptyDataException
import keznacl.onFalse
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.ResourceExistsException
import libmensago.ResourceNotFoundException
import utilities.DBProxy
import utilities.MissingProfileException
import utilities.ReservedValueException
import utilities.toObservable
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.exists

val LOCAL_DOMAIN = Domain.fromString("localhost.local")!!
val LOCAL_WID = RandomID.fromString("00000000-0000-0000-0000-000000000001")!!
val LOCAL_WADDRESS = WAddress.fromParts(LOCAL_WID, LOCAL_DOMAIN)

class ResourceInUseException : Exception()

/**
 * The ProfileManager class manages all of a user's profiles. This new version is a reworked
 * version of the original and provides better separation of duties and easier management.
 */
object ProfileManager {
    private val pdao = ProfileDAO()
    private val wdao = WorkspaceDAO()
    val model = ProfilesModel()
    var profilesPath = getDefaultProfilePath()


    init {
        wdao.initWorkspaceTables()
    }

    /**
     * Ensures there is always a profile available for use. This call returns a Boolean indicating
     * if the profile folder was empty prior to this call.
     */
    fun ensureDefaultProfile(): Result<Boolean> {
        runCatching { Files.createDirectories(Paths.get(profilesPath)) }
            .onFailure { return it.toFailure() }

        scanProfiles()?.let { return it.toFailure() }

        if (model.profileNames.isEmpty()) {
            createProfile("primary")?.let { return it.toFailure() }
            setDefaultProfile("primary")?.let { return it.toFailure() }
            return true.toSuccess()
        }

        if (model.defaultProfile.isEmpty())
            setDefaultProfile(model.profileNames.toList()[0])

        return false.toSuccess()
    }

    /**
     * Creates a profile of the given name in the user's profile folder. Care should be used with
     * spaces and special characters, as the name will be used as the profile's directory name in
     * the filesystem. The name 'default' is reserved and may not be used. Note that the profile
     * name is not case-sensitive and as such capitalization will be squashed when passed to this
     * method.
     */
    fun createProfile(name: String): Throwable? {
        val profileName = when {
            name.isEmpty() -> return EmptyDataException()
            name.equals("default", true) -> {
                return ReservedValueException()
            }

            else -> name.lowercase()
        }
        if (profileName in model.profileNames) return ResourceExistsException()

        kotlin.runCatching {
            File(pathForProfile(profileName)).mkdirs()
        }.getOrElse { return it }
        model.profileNamesProperty.add(profileName)

        if (model.profileNames.size == 1) setDefaultProfile(name)?.let { return it }

        return null
    }

    /**
     * Deletes the named profile and all files on disk contained in it. It returns true if
     * successful and false if the named profile was not found. If the profile is active, it will
     * be deactivated, and if the profile is the default one, the first profile in the list will
     * become the new default if the profile list is not empty
     */
    fun deleteProfile(name: String): Throwable? {
        if (name !in model.profileNames) return ResourceNotFoundException()

        if (name == model.activeProfile?.name)
            deactivateProfile(name)

        if (name == model.defaultProfile) {
            if (model.profileNames.isNotEmpty())
                setDefaultProfile(model.profileNames.first())?.let { return it }
            else
                model.defaultProfile = ""
        }
        model.profileNames.remove(name)
        return runCatching {
            File(pathForProfile(name)).deleteRecursively()
        }.exceptionOrNull()
    }

    /** Renames the profile from the old name to the new one */
    fun renameProfile(oldName: String, newName: String): Throwable? {
        if (oldName.isEmpty() || newName.isEmpty()) return EmptyDataException()

        val oldSquashed = oldName.lowercase()
        val newSquashed = newName.lowercase()
        if (oldSquashed !in model.profileNames) return ResourceNotFoundException()
        if (newSquashed in model.profileNames) return ResourceExistsException()

        if (model.activeProfile?.name == oldSquashed)
            return ResourceInUseException()

        runCatching {
            val newDir = File(pathForProfile(newSquashed))
            val oldDir = pathForProfile(oldSquashed)
            File(oldDir).renameTo(newDir).onFalse {
                throw IOException("Can't rename $oldDir to $newDir")
            }

            if (model.activeProfile?.name == oldSquashed)
                model.activeProfile?.name = newSquashed

            model.profileNames.remove(oldSquashed)
            model.profileNames.add(newSquashed)

        }.onFailure { return it }

        return null
    }

    /** Sets the named profile as active and deactivates any active ones. */
    fun activateProfile(name: String): Throwable? {
        if (name.isEmpty()) return EmptyDataException()

        val nameSquashed = name.lowercase()
        if (nameSquashed !in model.profileNames) return ResourceNotFoundException()

        if (model.activeProfile != null) {
            // TODO: return if trying to re-activate the currently active profile.
            //  This requires testing to make sure nothing breaks
            deactivateProfile(model.activeProfile!!.name)
        }

        val path = pathForProfile(nameSquashed)

        if (File(path, "storage.mv.db").exists()) {
            val dbPath = Paths.get(path, "storage")
            DBProxy.get().connect("jdbc:h2:$dbPath").getOrElse { return it }
        } else {
            pdao.resetDB(Paths.get(path, "storage"))?.let { return it }
        }

        model.activeProfile = pdao.load(name).getOrElse { return it }
        model.activeProfilePath = path

        return null
    }

    /** Sets the named profile as active and deactivates any active ones. */
    fun deactivateProfile(name: String) {
        name.ifEmpty { return }
        val nameSquashed = name.lowercase()

        if (model.activeProfile == null) return
        if (model.activeProfile!!.name != nameSquashed) return
        model.activeProfile = null
        model.activeProfilePath = ""
        DBProxy.get().disconnect()
    }

    /**
     * Attempts to scan the profile folder for entries and places them in the ProfileModel. If
     * there is a profile which is designated as the default, it was also set that in the model.
     */
    fun scanProfiles(): Throwable? {
        try {
            val profdir = Paths.get(profilesPath).toFile()
            profdir.exists().onFalse {
                model.profileNames.clear()
                return null
            }

            val entries = profdir.listFiles()
            if (entries == null) {
                model.profileNames.clear()
                return null
            }
            model.profileNames = entries
                .filter { it.isDirectory }
                .map { it.name }
                .toSortedSet()
                .toObservable()

            model.profileNames.find {
                Paths.get(pathForProfile(it), "default.txt").toFile().exists()
            }?.let { model.defaultProfile = it }

        } catch (e: Exception) {
            model.profileNames.clear()
            return e
        }
        return null
    }

    /** Sets the default profile */
    fun setDefaultProfile(name: String): Throwable? {
        if (name.isEmpty()) return EmptyDataException()

        val nameSquashed = name.lowercase()
        if (!model.profileNames.contains(nameSquashed)) return ResourceNotFoundException()

        if (model.defaultProfile == nameSquashed)
            return handleDefaultProfileFile(nameSquashed, true)

        if (model.defaultProfile.isNotEmpty())
            handleDefaultProfileFile(model.defaultProfile, false)?.let { return it }

        handleDefaultProfileFile(nameSquashed, true)?.let { return it }
        model.defaultProfile = nameSquashed
        return null
    }

    /**
     * Sets the location the ProfileManager uses for managing profiles. This should theoretically
     * not need to be called except for testing.
     *
     * @exception ResourceNotFoundException Returned if the location provided does not exist
     */
    fun setLocation(path: Path): Throwable? {
        path.exists().onFalse { return ResourceNotFoundException() }
        profilesPath = path.toString()
        return null
    }

    fun pathForProfile(name: String): String {
        return Paths.get(profilesPath, name).toString()
    }

    /**
     * This private method handles creating or deleting the file that designates a specific profile
     * as the default.
     */
    private fun handleDefaultProfileFile(name: String, value: Boolean): Throwable? {
        runCatching {
            val defaultFolderPath = Paths.get(pathForProfile(name))
            Files.exists(defaultFolderPath).onFalse { Files.createDirectory(defaultFolderPath) }

            val defaultFilePath = Paths.get(defaultFolderPath.toString(), "default.txt")
            if (value)
                Files.exists(defaultFilePath).onFalse { Files.createFile(defaultFilePath) }
            else
                Files.deleteIfExists(defaultFilePath)
        }.onFailure { return it }
        return null
    }

}

/**
 * Returns the location of the default path for user profiles based on the platform.
 */
fun getDefaultProfilePath(): String {
    val osName = System.getProperty("os.name")

    return if (osName.startsWith("windows", true))
        Paths.get(System.getenv("LOCALAPPDATA"), "mensago").toString()
    else
        Paths.get(System.getenv("HOME"), ".mensago").toString()
}

/**
 * Guard function which returns an error if there is no active profile, which is an error state
 * @exception MissingProfileException If there is no active profile.
 */
fun ensureActiveProfile(): Throwable? {
    return if (ProfileManager.model.activeProfile == null)
        MissingProfileException()
    else null
}

/** Convenience function to get the currently-active profile or an Exception if there isn't one */
fun getActiveProfile(): Result<Profile> {
    return ProfileManager.model.activeProfile?.toSuccess()
        ?: MissingProfileException().toFailure()
}
