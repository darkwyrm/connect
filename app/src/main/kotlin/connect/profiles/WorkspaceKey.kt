package connect.profiles

import connect.KeyPurpose
import keznacl.CryptoString
import keznacl.Hash
import kotlinx.serialization.Serializable
import libkeycard.Timestamp
import libkeycard.WAddress

@Serializable
class WorkspaceKey(
    val keyid: Hash,
    val address: WAddress,
    val type: String,
    val purpose: KeyPurpose,
    val pubkey: CryptoString,
    val privkey: CryptoString,
    val status: String,
    val timestamp: Timestamp
)
