package connect

import libkeycard.RandomID
import libkeycard.Timestamp
import utilities.DBProxy

/**
 * Class used for logging to the internal database. This is for covering errors from the incoming
 * and outgoing workers.
 */
class LogDAO {

    /**
     * Sets up all the necessary tables for storing contact information in the database and also
     * adds a mostly-empty record for the user's information.
     */
    fun initLogTable(): Throwable? {
        return DBProxy.get().execute(
            """CREATE TABLE IF NOT EXISTS errlog (
                id	        UUID PRIMARY KEY NOT NULL UNIQUE,
                source      TEXT NOT NULL,
                timestamp   TEXT NOT NULL,
                errtype     TEXT NOT NULL,
                short_error TEXT NOT NULL,
                long_error  TEXT NOT NULL
            );"""
        )
    }

    /** Adds an error to the log */
    fun error(source: String, shortError: String, longError: String): Throwable? {
        with(DBProxy.get()) {
            execute(
                "INSERT INTO errlog (id,timestamp,errtype,source,short_error,long_error) " +
                        "VALUES(?,?,?,?,?,?)",
                RandomID.generate(), Timestamp(), "error", source, shortError, longError
            )?.let { return it }
        }
        return null
    }
}