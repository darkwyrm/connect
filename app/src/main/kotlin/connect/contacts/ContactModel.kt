package connect.contacts

import connect.Filter
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty

class ContactModel {
    /** The list of contacts visible as a result of the current filter */
    val contactListProperty = SimpleListProperty<ContactHeaderItem>()
    var contactList
        get() = contactListProperty.get()
        set(value) = contactListProperty.set(value)

    val selectedFilterProperty = SimpleObjectProperty<Filter?>()

    /** The list of filters generated from existing content */
    val filterListProperty = SimpleListProperty<Filter>()

    /** The currently-selected contact */
    val selectedContactProperty = SimpleObjectProperty<ContactItem>()
}
