package connect.contacts.aboutme

import javafx.application.Platform
import javafx.embed.swing.SwingFXUtils
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.*
import javafx.scene.layout.GridPane.setConstraints
import javafx.stage.FileChooser
import javax.imageio.ImageIO

class AboutMeViewBuilder(
    private val model: AboutMeModel, private val savePhotoHandler: () -> Unit
) {
    private val photoView = buildPhotoView()

    fun build(): Region {
        val bioGroup = VBox().apply {
            val bioLabel = Label("Bio:")
            val bioBox = TextArea().apply {
                promptText = "Bio"
                text = model.bio
                prefHeight = 100.0
                isWrapText = true
                textProperty().bindBidirectional(model.bioProperty)
            }
            children.addAll(bioLabel, bioBox)
            VBox.setVgrow(bioBox, Priority.ALWAYS)
        }
        VBox.setVgrow(bioGroup, Priority.ALWAYS)

        return VBox().apply {
            padding = Insets(10.0)
            spacing = 10.0
            alignment = Pos.TOP_CENTER

            children.addAll(
                photoView,
                buildNameGrid(),
                Separator(),
                buildMAddressGrid(),
                buildMiscGrid(),
                bioGroup,
            )
        }
    }

    private fun buildNameGrid(): Region {

        val nameLabel = Label("Name:")
        val firstNameBox = TextField().apply {
            promptText = "First Name"
            text = model.givenName
            textProperty().bindBidirectional(model.givenNameProperty)
        }
        val lastNameBox = TextField().apply {
            promptText = "Last Name"
            text = model.familyName
            textProperty().bindBidirectional(model.familyNameProperty)
        }

        val nameGroup = HBox().apply {
            alignment = Pos.CENTER
            spacing = 5.0
            children.addAll(nameLabel, firstNameBox, lastNameBox)
        }

        Platform.runLater { firstNameBox.requestFocus() }
        return VBox().apply {
            alignment = Pos.CENTER
            children.add(nameGroup)
        }
    }

    private fun buildMAddressGrid(): Region {

        val addressLabel = Label("Mensago Address:")
        val addressContent = Label().apply {
            if (model.maddress.isNotEmpty())
                textProperty().bind(model.maddressProperty)
            else
                textProperty().bind(model.waddressProperty)
        }

        return GridPane().apply {
            hgap = 5.0
            vgap = 5.0

            setConstraints(addressLabel, 0, 0)
            setConstraints(addressContent, 1, 0)

            children.addAll(addressLabel, addressContent)
        }
    }

    private fun buildMiscGrid(): Region {
        val titleLabel = Label("Title:")
        val titleBox = TextField().apply {
            promptText = "Title"
            text = model.title
            textProperty().bindBidirectional(model.titleProperty)
        }

        val organizationLabel = Label("Organization:")
        val organizationBox = TextField().apply {
            promptText = "Organization"
            text = model.organization
            textProperty().bindBidirectional(model.organizationProperty)
        }

        val personalEmailLabel = Label("Personal Email:")
        val personalEmailBox = TextField().apply {
            promptText = "Personal Email"
            text = model.personalEmail
            textProperty().bindBidirectional(model.personalEmailProperty)
        }

        val workEmailLabel = Label("Work Email:")
        val workEmailBox = TextField().apply {
            promptText = "Work Email"
            text = model.workEmail
            textProperty().bindBidirectional(model.workEmailProperty)
        }

        val websiteLabel = Label("Website:")
        val websiteBox = TextField().apply {
            promptText = "Website"
            text = model.website
            textProperty().bindBidirectional(model.websiteProperty)
        }

        return GridPane().apply {
            hgap = 5.0
            vgap = 5.0

            setConstraints(titleLabel, 0, 0)
            setConstraints(titleBox, 1, 0)

            setConstraints(organizationLabel, 0, 1)
            setConstraints(organizationBox, 1, 1)

            setConstraints(personalEmailLabel, 0, 2)
            setConstraints(personalEmailBox, 1, 2)

            setConstraints(workEmailLabel, 0, 3)
            setConstraints(workEmailBox, 1, 3)

            setConstraints(websiteLabel, 0, 4)
            setConstraints(websiteBox, 1, 4)

            children.addAll(
                titleLabel, titleBox,
                organizationLabel, organizationBox,
                personalEmailLabel, personalEmailBox,
                workEmailLabel, workEmailBox,
                websiteLabel, websiteBox,
            )
        }
    }

    private fun buildPhotoView(): Node {
        val imgview = ImageView().apply {
            fitWidth = 100.0
            isPreserveRatio = true
            image = model.photo ?: model.blankProfile
            onMouseClicked = EventHandler { choosePhoto() }
            model.photoProperty.subscribe { newValue ->
                image = newValue ?: model.blankProfile
            }
        }

        val chooseButton = Button("…").apply {
            onAction = EventHandler { choosePhoto() }
        }
        val clearButton = Button("x").apply {
            onAction = EventHandler {
                model.photo = null
                savePhotoHandler()
            }
        }

        val buttonGroup = VBox().apply {
            children.addAll(chooseButton, clearButton)
        }

        val grid = GridPane().apply {
            setConstraints(buttonGroup, 0, 0)
            setConstraints(imgview, 1, 0)

            children.addAll(buttonGroup, imgview)
        }

        return HBox().apply {
            spacing = 5.0
            alignment = Pos.BOTTOM_CENTER
            children.add(grid)
        }
    }

    private fun choosePhoto() {
        val chooser = FileChooser().apply {
            title = "Choose a Picture"
            extensionFilters.add(
                FileChooser.ExtensionFilter(
                    "Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.bmp"
                )
            )
        }

        val file = chooser.showOpenDialog(null) ?: return
        val result = kotlin.runCatching { ImageIO.read(file) }
        if (result.isFailure) {
            Alert(Alert.AlertType.ERROR).apply {
                this.title = "Error"
                this.contentText = "Connect couldn't open $file. Sorry."
            }.showAndWait()
            println("ImageIO.read error: ${result.exceptionOrNull()}")
            return
        } else
            model.photo = SwingFXUtils.toFXImage(result.getOrNull(), null)
        savePhotoHandler()
    }
}
