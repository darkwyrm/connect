package connect.contacts.aboutme

import connect.UserInfoDAO

class AboutMeInteractor(private val model: AboutMeModel) {

    fun saveUserPhoto() {
        if (model.photo == null)
            UserInfoDAO().savePhoto(null)
        else {
            UserInfoDAO().savePhoto(if (model.photo === model.blankProfile) null else model.photo)
        }
    }
}
