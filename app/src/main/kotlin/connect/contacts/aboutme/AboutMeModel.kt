package connect.contacts.aboutme

import connect.contacts.AddressInfo
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image

/** State class for the About Me window */
class AboutMeModel {
    val blankProfile = Image("blankprofile.png")

    val photoProperty = SimpleObjectProperty<Image>()
    var photo: Image?
        get() = photoProperty.value
        set(value) {
            photoProperty.set(value)
        }

    val givenNameProperty = SimpleStringProperty("")
    var givenName: String
        get() = givenNameProperty.value
        set(value) {
            givenNameProperty.set(value)
        }

    val familyNameProperty = SimpleStringProperty("")
    var familyName: String
        get() = familyNameProperty.value
        set(value) {
            familyNameProperty.set(value)
        }

    val maddressProperty = SimpleStringProperty("")
    var maddress: String
        get() = maddressProperty.value
        set(value) {
            maddressProperty.set(value)
        }

    val waddressProperty = SimpleStringProperty("")
    var waddress: String
        get() = waddressProperty.value
        set(value) {
            waddressProperty.set(value)
        }

    val titleProperty = SimpleStringProperty("")
    var title: String
        get() = titleProperty.value
        set(value) {
            titleProperty.set(value)
        }

    val organizationProperty = SimpleStringProperty("")
    var organization: String
        get() = organizationProperty.value
        set(value) {
            organizationProperty.set(value)
        }

    val bioProperty = SimpleStringProperty("")
    var bio: String
        get() = bioProperty.value
        set(value) {
            bioProperty.set(value)
        }

    // Phone numbers

    val mobileNumberProperty = SimpleStringProperty("")
    var mobileNumber: String
        get() = mobileNumberProperty.value
        set(value) {
            mobileNumberProperty.set(value)
        }

    val homeNumberProperty = SimpleStringProperty("")
    var homeNumber: String
        get() = homeNumberProperty.value
        set(value) {
            homeNumberProperty.set(value)
        }

    val workNumberProperty = SimpleStringProperty("")
    var workNumber: String
        get() = workNumberProperty.value
        set(value) {
            workNumberProperty.set(value)
        }

    // Other contact info

    val personalEmailProperty = SimpleStringProperty("")
    var personalEmail: String
        get() = personalEmailProperty.value
        set(value) {
            personalEmailProperty.set(value)
        }

    val workEmailProperty = SimpleStringProperty("")
    var workEmail: String
        get() = workEmailProperty.value
        set(value) {
            workEmailProperty.set(value)
        }

    var websiteProperty = SimpleStringProperty("")
    var website: String
        get() = websiteProperty.value
        set(value) {
            websiteProperty.set(value)
        }

    val addressListProperty = SimpleListProperty<AddressInfo>()
}
