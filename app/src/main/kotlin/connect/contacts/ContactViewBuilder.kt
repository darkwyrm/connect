package connect.contacts

import connect.ChangeHandler
import connect.Filter
import connect.common.buildLabelList
import connect.common.buildModeMenu
import connect.controls.ContactView
import connect.main.AppMode
import connect.main.MainModel
import connect.main.ModeInfo
import javafx.geometry.Orientation
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.scene.layout.VBox.setVgrow

class ContactViewBuilder(
    private val model: ContactModel, private val mainModel: MainModel,
    private val handlers: ContactViewHandlers
) {
    fun build() {
        val info = ModeInfo(
            AppMode.Contacts, buildMainLayout(),
            KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.CONTROL_DOWN)
        )
        mainModel.modeList.add(info)
    }

    private fun buildMainLayout(): Region {

        val contactView = ContactView(
            model.selectedContactProperty, Orientation.VERTICAL, false
        )
        val list = ListView(model.contactListProperty).apply {
            selectionModel.selectedItemProperty().subscribe { oldItem, newItem ->
                handlers.contactSelection(oldItem, newItem)
            }
        }

        val layout = SplitPane().apply {
            items.addAll(
                buildLabelList(model.filterListProperty, handlers.filterSelection),
                list, contactView
            )
            setDividerPositions(.25, .6)
        }

        return VBox().apply {
            children.addAll(buildMenu(), layout)
            setVgrow(layout, Priority.ALWAYS)
        }
    }

    private fun buildMenu(): Region {
        return MenuBar(buildContactMenu(), buildModeMenu(mainModel))
    }

    private fun buildContactMenu(): Menu {
        val aboutMe = MenuItem("_About Me…").apply {
            setOnAction { handlers.editUserInfo() }
        }
        val newCR = MenuItem("_Send Contact Request…").apply {
            setOnAction { handlers.showNewCR() }
            disableProperty().bind(mainModel.isProfileLocalProperty)
        }

        return Menu(
            "_Contact", null,
            newCR,
            SeparatorMenuItem(),
            aboutMe,
        )
    }
}

class ContactViewHandlers(
    val filterSelection: ChangeHandler<Filter?>,
    val contactSelection: ChangeHandler<ContactHeaderItem?>,
    val editUserInfo: () -> Unit,
    val showNewCR: () -> Unit,
)
