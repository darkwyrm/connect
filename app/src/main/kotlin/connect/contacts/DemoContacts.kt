package connect.contacts

import connect.KeyPurpose
import javafx.scene.image.Image
import libkeycard.MAddress
import utilities.toObservable

fun generateDemoContacts(loadPhoto: Boolean = true): MutableList<ContactItem> {
    return demoContacts.map {
        ContactItem(it.formattedName, it.id).apply {
            givenName = it.givenName
            familyName = it.familyName
            setLabels(it.labels)
            middleName = it.middleName
            prefix = it.prefix
            suffix = it.suffix
            gender = it.gender

            if (it.mAddress.isNotEmpty()) {
                val maddr = MAddress.fromString(it.mAddress)!!
                uid = maddr.userid.toString()
                domain = maddr.domain.toString()
            }

            wid = it.wid
            bio = it.bio
            anniversary = it.anniversary
            birthday = it.birthday
            organization = it.organization
            orgUnits = it.orgunits
            title = it.title
            languages = it.languages
            contents = it.notes
            format = it.format

            addressListProperty.set(it.addressList.toObservable())
            keyListProperty.set(it.keyList.toObservable())
            phoneListProperty.set(it.phoneList.toObservable())
            emailListProperty.set(it.emailList.toObservable())
            socialListProperty.set(it.socialList.toObservable())
            websiteListProperty.set(it.websiteList.toObservable())

            photo = if (loadPhoto and it.photoName.isNotEmpty())
                Image(it.photoName)
            else
                null
        }
    }.toMutableList()
}

internal data class DemoContact(
    var id: String,
    var formattedName: String,
    var givenName: String = "",
    var familyName: String = "",
    var labels: String,
    var middleName: String = "",
    var prefix: String = "",
    var suffix: String = "",
    var gender: String = "",
    var mlabel: String = "",
    var mAddress: String = "",
    var wid: String = "",
    var bio: String = "",
    var anniversary: String = "",
    var birthday: String = "",
    var organization: String = "",
    var orgunits: String = "",
    var title: String = "",
    var languages: String = "",
    var notes: String = "",
    var format: String = "plain",

    var addressList: List<AddressInfo> = listOf(),
    var keyList: List<ContactKeyInfo> = listOf(),
    var phoneList: List<MultiInfo> = listOf(),
    var emailList: List<MultiInfo> = listOf(),
    var socialList: List<MultiInfo> = listOf(),
    var websiteList: List<MultiInfo> = listOf(),
    var photoName: String = "",
)

internal val demoContacts = listOf(
    DemoContact(
        "b80de689-febf-4c43-9ff6-3f7d3251638a",
        "Dr. Richard Brannan, MD",
        "Richard",
        "Brannan",
        "sample",
        "M",
        "Dr.",
        "MD",
        "Male",
        "Work",
        "rbrannan/example.com",
        "714d66b1-8c0f-4763-a772-ef9acd1f1dee",
        "Underwater basket weaving and medieval plumbing are interests of mine",
        "14-07",
        "1975-04-15",
        "Mifflin Medical Center",
        "Intensive Care",
        "Hospitalist",
        "eng,spa",
        "I like cheese",
        "plain",
        listOf(
            AddressInfo(
                "f5e18183-f1b0-4ade-a9c2-c3365062b443",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Work",
                addStreet = "1313 Mockingbird Lane",
                addLocality = "San Jose",
                addRegion = "California",
                addPostalCode = "12345",
                preferred = true
            ),
            AddressInfo(
                "ad103721-6d26-4d38-856c-7b1f95e7e30c",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Home",
                addLocality = "Sunnyvale",
                addRegion = "California",
                addCountry = "United States",
            ),
        ),
        listOf(
            ContactKeyInfo(
                "6714b3b1-1af3-4211-974b-de5f512c852e",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                true,
                KeyPurpose.ConReqEncryption,
                "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
                "2024-06-01T13:25Z",
            ),
            ContactKeyInfo(
                "af9def6a-d7c4-4985-a76e-6a4d403b90cf",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                false,
                KeyPurpose.ConReqEncryption,
                "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>",
                "2024-06-01T13:25Z",
            ),
        ),
        listOf(
            MultiInfo(
                "f20c94b0-931e-401f-995d-debe2cd3a727",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Mobile",
                "phone",
                "555-555-1234",
                true
            ),
            MultiInfo(
                "6f18f4cb-28be-4800-a272-a1c241894d9c",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Work",
                "phone",
                "555-555-1234",
                false
            ),
        ),
        listOf(
            MultiInfo(
                "88383202-7176-43df-ae23-93f12d2e3ff6",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Work",
                "email",
                "rbrannan/contoso.com",
                true
            ),
            MultiInfo(
                "4f3c7c37-91da-4896-a3e8-a1141c1a778d",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Home",
                "email",
                "drrick/example.com",
                false
            ),
        ),
        listOf(
            MultiInfo(
                "b2c5c1c0-d53e-4b51-9fe0-92a105644032",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Facebook",
                "social",
                "https://facebook.com/rick.brannan",
                false
            ),
            MultiInfo(
                "a9a16819-dd99-47a8-9564-3947a608fce5",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "Mastodon",
                "social",
                "@drrick@mastodon.cloud",
                true
            ),
        ),
        listOf(
            MultiInfo(
                "1d629ea7-edbc-49d0-8e28-d2915db221b4",
                "b80de689-febf-4c43-9ff6-3f7d3251638a",
                "DuckDuckGo",
                "website",
                "https://www.duckduckgo.com",
                true
            ),
        ),
        photoName = "businesscat.jpg"
    ),
    DemoContact(
        "39a98db8-5fe7-492a-8675-44017eac73e0",
        formattedName = "Allie Bennett",
        givenName = "Allie",
        familyName = "Bennett",
        labels = "",
        gender = "Female",
        mlabel = "Personal",
        mAddress = "a.bennett/example.com",
        wid = "22280e4b-e297-4848-b274-5257664e2c67",
        birthday = "29-08",
        organization = "Frami, Jacobi and Lesch",
        title = "Paralegal",
        addressList = listOf(
            AddressInfo(
                "5bdd7087-4a46-45ff-b1cf-e5e0f842de2e",
                "39a98db8-5fe7-492a-8675-44017eac73e0",
                "Personal",
                addStreet = "2601 Southwest Joshua Ave.",
                addLocality = "Bentonville",
                addRegion = "AR",
                addPostalCode = "72712",
                addCountry = "United States",
            )
        ),
        keyList = listOf(
            ContactKeyInfo(
                "18b13b7d-e427-42d7-94ed-d0e4c58fb7f9",
                "39a98db8-5fe7-492a-8675-44017eac73e0",
                true,
                KeyPurpose.ConReqEncryption,
                "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
                "2024-06-01T13:25Z",
            ),
            ContactKeyInfo(
                "03eae31b-6f76-4cf4-af06-77efb3d834f9",
                "39a98db8-5fe7-492a-8675-44017eac73e0",
                false,
                KeyPurpose.ConReqEncryption,
                "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>",
                "2024-06-01T13:25Z",
            ),
        ),
        phoneList = listOf(
            MultiInfo(
                "99e5b379-76a2-46c6-b210-db992c8fad18",
                "39a98db8-5fe7-492a-8675-44017eac73e0",
                "Mobile",
                "phone",
                "458-555-1234",
                true
            )
        ),
    ),
    DemoContact(
        "97d5c4fb-4cdf-42dd-8944-09fc45a79456",
        formattedName = "Charlie Cuevas",
        givenName = "Charlie",
        familyName = "Cuevas",
        labels = "",
        wid = "131a3147-e966-47ac-9690-bb49b61003a6",
        mAddress = "131a3147-e966-47ac-9690-bb49b61003a6/example.com",
    ),
    DemoContact(
        "e6e8a309-8794-4f76-947b-ecbb67eb2052",
        formattedName = "Holly Green",
        givenName = "Holly",
        familyName = "Green",
        labels = "",
    ),
    DemoContact(
        "0c8d28ee-c0f8-444f-bd56-048ccb3ed1b5",
        formattedName = "Kevin Villalobos",
        givenName = "Kevin",
        familyName = "Villalobos",
        labels = "",
    ),
    DemoContact(
        "e39ac618-90dc-4592-ae7f-1bacdf7173ac",
        formattedName = "Martha Polanco",
        givenName = "Martha",
        familyName = "Polanco",
        labels = "",
    ),
    DemoContact(
        "e7ead313-e4ea-4744-84a6-7db4baf24815",
        formattedName = "Robyn Nguyen",
        givenName = "Robyn",
        familyName = "Nguyen",
        labels = "",
    ),
    DemoContact(
        "156a9244-2dba-4cbb-b421-f71c8b0afa26",
        formattedName = "Tanner Wang",
        givenName = "Tanner",
        familyName = "Wang",
        labels = "",
    ),
    DemoContact(
        "459365e4-bff1-4d60-b1b1-3bf051a452a2",
        formattedName = "Daisy Buchanan",
        givenName = "Daisy",
        familyName = "Buchanan",
        labels = "Friends",
        mlabel = "Personal",
        mAddress = "daisyb7423/example.com",
        wid = "f6987a89-6aaa-4d8c-a2e5-20a2004b842e"
    ),
    DemoContact(
        "60e075d9-af03-4bba-a487-706e280e2afa",
        formattedName = "Evie O'Neill",
        givenName = "Evie",
        familyName = "O'Neill",
        labels = "Work",
        mlabel = "Work",
        mAddress = "evie.oneill/example.com",
        wid = "9fd4b98d-cc39-4415-907e-9d4bbb8dac0e",
    ),
    DemoContact(
        "ff444939-cfbc-4012-9c03-327a7b9486f7",
        formattedName = "CPL Customer Service",
        labels = "Services",
        mlabel = "Work",
        mAddress = "service/cpl.lib",
        wid = "f0feedaa-e8e7-4672-8507-92e33971a3de",
        organization = "Cleveland Public Library",
    ),
    DemoContact(
        "fe9f8500-8014-443d-a329-a99fe59e2335",
        formattedName = "Fun World Deals",
        labels = "Personal",
        mlabel = "Work",
        mAddress = "deals/funworld.biz",
        wid = "3deb6bd9-7f92-4707-b442-7ba612da9adc",
        organization = "Fun World",
    ),
    DemoContact(
        "c2ca9693-0b7a-4159-9175-6c6f38ab72e2",
        formattedName = "FitFusion",
        labels = "Personal,Services",
        mlabel = "Work",
        mAddress = "fitfusion/example.com",
        wid = "5a388f15-0fa3-4829-8940-ba58c123ac7c",
        organization = "FitFusion",
    ),
    DemoContact(
        "cd3bf70c-2ac9-410f-b442-78266be2a8e5",
        formattedName = "BeanBox Coffee Customers",
        labels = "Personal,Services",
        mlabel = "Work",
        mAddress = "service/beanbox.xyz",
        wid = "536ed195-b488-4926-8d23-e58c3ff5e01f",
        organization = "BeanBox",
    ),
    DemoContact(
        "852f7c55-7867-427d-995a-3dc0fad21d6b",
        formattedName = "MatchMe",
        labels = "Services",
        mlabel = "Work",
        mAddress = "info/match.me",
        wid = "536ed195-b488-4926-8d23-e58c3ff5e01f",
        organization = "MatchMe",
    ),
    DemoContact(
        "aaed41bb-26cc-4bd1-a7d1-33f2d8f9e2bd",
        formattedName = "JACC",
        labels = "Services",
        mlabel = "Work",
        mAddress = "jacc/example.com",
        wid = "9d244e48-e463-4f14-8bad-91c4dd45c6d2",
        organization = "J. Armitage Community Center",
    ),
    DemoContact(
        "43a5d89b-742b-4018-903b-9df0cf946940",
        formattedName = "GroovySounds",
        labels = "",
        mlabel = "Work",
        mAddress = "s0unding_gr00vy/example.com",
        wid = "592d273f-ecb2-44f4-a71b-c25083756e83",
        organization = "GroovySounds Record Store",
    ),
    DemoContact(
        "b540a52b-c304-44e8-bd4e-db4158f5bcf7",
        "Sabin Figaro",
        "Sabin",
        "Figaro",
        "Returners",
    ),
    DemoContact(
        "b540a52b-8637-4828-ba1c-0a3b015ff2b1",
        "Edgar Figaro",
        "Edgar",
        "Figaro",
        "Returners",
    ),
    DemoContact(
        "b540a52b-559a-4236-a32e-019a27020294",
        "Celes Chere",
        "Celes",
        "Chere",
        "Imperial",
    ),
    DemoContact(
        "b540a52b-50e3-4f9f-8b6d-d6b0f0436b14",
        "Locke Cole",
        "Locke",
        "Cole",
        "Returners",
    ),
    DemoContact(
        "b540a52b-d686-49e7-ba3d-371bb81202e2",
        "Kefka Palazzo",
        "Kefka",
        "Palazzo",
        "Imperial,Trash",
    ),
)
