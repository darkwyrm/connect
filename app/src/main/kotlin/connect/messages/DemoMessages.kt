package connect.messages

import connect.notes.loremIpsum
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

fun generateDemoMessages(): MutableList<MessageItem> {
    val pathBase = "/ 815842a6-1294-4d1b-9bb4-963e845683d9 "
    return demoEmails.map {
        MessageItem(it["subject"]!!, it["id"]!!).apply {
            fromName = it["fromName"]!!
            fromAddr = it["from"]!!
            toAddr = it["to"]!!
            date = LocalDateTime.ofInstant(Instant.parse(it["date"]!!), ZoneOffset.UTC)
            subject = it["subject"]!!
            contents = it["contents"]!!
            id = it["id"]!!
            threadID = it["threadid"]!!
            setLabels(it["labels"]!!)
        }.apply {
            val contentSize = contents.length
            val filename = "${Instant.now().epochSecond}.$contentSize." +
                    UUID.randomUUID().toString().lowercase()
            serverPath = pathBase + filename
        }
    }.toMutableList()
}

private const val recipient = "46de39d6-42d6-4937-aa41-d463b6d7a569/example.com"
internal val demoEmails = listOf(
    mapOf(
        "fromName" to "Sender A",
        "from" to "ad317f94-fca8-4eb4-9c80-1256035a38c4/example.com",
        "to" to recipient,
        "date" to "2024-06-01T13:00:00Z",
        "subject" to "Email A",
        "contents" to loremIpsum,
        "id" to "01903BC5-69E3-42A6-07EC-AB19FB6C09F1",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to ""
    ),
    mapOf(
        "fromName" to "Sender B",
        "from" to "90ff4a39-b608-48de-a865-5e3fd1284ba0/example.com",
        "to" to recipient,
        "date" to "2024-06-02T01:01:00Z",
        "subject" to "Email B",
        "contents" to loremIpsum,
        "id" to "01903BC5-69E3-2D8F-964A-D7901677D6EB",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to ""
    ),
    mapOf(
        "fromName" to "Sender C",
        "from" to "ad317f94-fca8-4eb4-9c80-1256035a38c4/example.com",
        "to" to recipient,
        "date" to "2024-06-03T09:37:00Z",
        "subject" to "Email C",
        "contents" to loremIpsum,
        "id" to "01903BC5-69E3-CB77-0579-9B54C8FEB752",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to "foo"
    ),
    mapOf(
        "fromName" to "Sender D",
        "from" to "d8646185-1a16-4449-9ff0-6cdb3e3c3682/example.com",
        "to" to recipient,
        "date" to "2024-06-03T14:00:00Z",
        "subject" to "Email D",
        "contents" to loremIpsum,
        "id" to "01903BC5-69E3-D6DF-B821-32E4253978FD",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to "foo,bar"
    ),
    mapOf(
        "fromName" to "Sender E",
        "from" to "ad317f94-fca8-4eb4-9c80-1256035a38c4/example.com",
        "to" to recipient,
        "date" to "2024-06-04T14:30:00Z",
        "subject" to "Email E",
        "contents" to loremIpsum,
        "id" to "01903BC5-69E3-4C76-DF65-3F77CEC31823",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to "bar,baz"
    ),
    mapOf(
        "fromName" to "Sender F",
        "from" to "d8646185-1a16-4449-9ff0-6cdb3e3c3682/example.com",
        "to" to recipient,
        "date" to "2024-06-05T10:00:00Z",
        "subject" to "Email F",
        "contents" to loremIpsum,
        "id" to "9122499f-4570-436c-a02a-35ab7a4cac66",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to "Trash"
    ),
    mapOf(
        "fromName" to "Daisy Buchanan",
        "from" to "daisyb7423/example.com",
        "to" to recipient,
        "date" to "2024-06-06T07:42:23Z",
        "subject" to "Invitation to Gatsby’s Extravaganza",
        "id" to "01903BC5-69E3-42A6-07EC-AB19FB6C09F1",
        "threadid" to "477fcfdf-8088-47ac-bf8f-f2ec8f51abfc",
        "labels" to "Inbox",
        "contents" to "Dear Corbin,\n\n" +
                "You're cordially invited to Jay Gatsby's legendary party at his mansion on the " +
                "lake this Saturday. Expect glitz, glamour, and plenty of jazz. Don't forget to " +
                "wear your finest attire!\n\n" +
                "Yours,\n" +
                "Daisy",
    ),
    mapOf(
        "fromName" to "Evie O'Neill",
        "from" to "evie.oneill/example.com",
        "to" to recipient,
        "date" to "2024-06-08T16:10:53Z",
        "subject" to "Occult Research Request",
        "id" to "17893a2f-aff4-41c6-9717-d4fbdbe6dbce",
        "threadid" to "61da6e86-da29-4e16-8480-dab76700b8ea",
        "labels" to "Inbox",
        "contents" to "Heya. I've stumbled upon some mysterious symbols related to a case I'm " +
                "researching. Could you see if you can dig up any references to these cryptic " +
                "marks? Meet me at our usual place afterward.\n",
    ),
    mapOf(
        "fromName" to "CPL Customer Service",
        "from" to "service/cpl.lib",
        "to" to recipient,
        "date" to "2024-06-09T08:01:02Z",
        "subject" to "Library Card Activation",
        "id" to "f11b1cae-c3c6-4638-a09d-c3839252ba03",
        "threadid" to "33a84b00-5786-4342-8540-7f3bf1362a27",
        "labels" to "Inbox",
        "contents" to "Dear Corbin,\n\n" +
                "Congratulations! Your library card is now active. Explore our extensive " +
                "collection of books, periodicals, and occult references. Swing by the library to " +
                "pick up your card.\n\n" +
                "Sincerely,\n" +
                "CPL",
    ),
    mapOf(
        "fromName" to "Fun World Deals",
        "from" to "deals/funworld.biz",
        "to" to recipient,
        "date" to "2024-06-09T08:24:17Z",
        "subject" to "Save 20% on Summer Fun with Us!",
        "id" to "b050d887-74dd-42f6-b59c-c5381fab3eb3",
        "threadid" to "7ec90a76-c42f-42c9-a6b3-c6cb4fd70114",
        "labels" to "Inbox",
        "contents" to "Hi Corbin,\n\n" +
                "Looking for a chance to hit the rollercoasters this summer? Use code " +
                "\"ROARING20s\" at checkout to save 20% on tickets in our new mobile app!\n\n" +
                "Fun World\n",
    ),
    mapOf(
        "fromName" to "FitFusion",
        "from" to "fitfusion/example.com",
        "to" to recipient,
        "date" to "2024-06-10T11:50:32Z",
        "subject" to "New Workout Challenge",
        "id" to "9afbbec0-feeb-49b2-969d-93d1b2621fc7",
        "threadid" to "4fd86de0-d887-4e4e-aa06-5de3aeacf1db",
        "labels" to "Inbox",
        "contents" to "Hey Corbin,\n\n" +
                "Ready to crush your fitness goals? Our app now offers a 30-day HIIT challenge. " +
                "Burn calories, build muscle, and unlock exclusive rewards. Get started today!\n\n" +
                "Stay fit,\n" +
                "The FitFusion Team",
    ),
    mapOf(
        "fromName" to "BeanBox Coffee Customers",
        "from" to "Service/BeanBox.xyz",
        "to" to recipient,
        "date" to "2024-06-10T11:57:40Z",
        "subject" to "Your Next Brew is on Us!",
        "id" to "ffe2b3aa-1186-47c4-8d2e-98b4fd438d95",
        "threadid" to "e55a1972-f87b-4199-85a7-8cb8039b1b61",
        "labels" to "Inbox",
        "contents" to "Hi Corbin,\n\n" +
                "Thanks for being a loyal subscriber! Your next bag of artisanal coffee is " +
                "complimentary. Choose your blend and delivery date. ☕\n\n" +
                "Cheers,\n" +
                "BeanBox",
    ),
    mapOf(
        "fromName" to "MatchMe",
        "from" to "Info/Match.Me",
        "to" to recipient,
        "date" to "2024-06-11T07:21:33Z",
        "subject" to "New Match Alert!",
        "id" to "741aa0a6-1c07-43d6-bee7-52e54d22e076",
        "threadid" to "f44ac656-b22c-4705-bb80-c359808eea04",
        "labels" to "Trash",
        "contents" to "You've got a match! Meet Emily, a fellow bookworm who loves indie music. " +
                "Start chatting now and maybe plan a coffee date at the local bookstore? " +
                "Happy swiping!\n\n" +
                "MatchMe",
    ),
    mapOf(
        "fromName" to "J. Armitage Community Center",
        "from" to "JACC/example.com",
        "to" to recipient,
        "date" to "2025-01-12T12:22:13Z",
        "subject" to "Book Club Meeting Reminder",
        "id" to "3a740cae-c255-44cb-b6bd-07776c9db531",
        "threadid" to "37f841b4-bb45-4bfc-b126-6098d6722e5f",
        "labels" to "Inbox",
        "contents" to "Our monthly book club meets this Thursday at 7 PM. We'll discuss " +
                "\"The Great Gatsby.\" Swing by the community center, grab a coffee, and join " +
                "the literary conversation! See you then!\n\n" +
                "Community Reads",
    ),
    mapOf(
        "fromName" to "GroovySounds",
        "from" to "s0unding_gr00vy/example.com",
        "to" to recipient,
        "date" to "2025-02-12T23:10:20Z",
        "subject" to "Vinyl Sale: 20% Off Classics!",
        "id" to "89ead958-9283-4299-a1e8-ead67dca774f",
        "threadid" to "fe3aab79-59b9-402e-9482-b16ead4cb877",
        "labels" to "Inbox",
        "contents" to "Dust off your turntable! Our store is offering a discount on " +
                "vintage vinyl records. Swing by and groove to timeless tunes.\n\n" +
                "Stay Groovy!\n\n" +
                "GroovySounds\n",
    ),
)
