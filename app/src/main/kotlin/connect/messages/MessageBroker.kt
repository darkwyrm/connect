package connect.messages

import connect.Filter
import libmensago.MServerPath

class MessageBroker {
    private val dao = MessageDAO()

    init {
        dao.initMessageTable()?.let { throw it }
    }

    fun emptyTrash() = dao.emptyTrash()

    fun addLabel(item: MsgBaseHeaderItem, label: String) = dao.addLabel(item, label)
    fun removeLabel(item: MsgBaseHeaderItem, label: String) = dao.removeLabel(item, label)
    fun loadFilters(): Result<List<Filter?>> = dao.loadFilters()

    fun loadHeaders(filter: Filter?): Result<List<MsgBaseHeaderItem>> = dao.loadHeaders(filter)
    fun load(id: String): Result<MsgBaseItem?> = dao.load(id)
    fun save(msg: MsgBaseItem): Throwable? = dao.save(msg)
    fun saveSelectedSubject(id: String, subject: String): Throwable? = dao.saveSubject(id, subject)
    fun saveSelectedContents(id: String, contents: String): Throwable? =
        dao.saveContents(id, contents)

    fun delete(note: MsgBaseHeaderItem): Throwable? = dao.delete(note)
    fun deleteFromPath(path: MServerPath): Throwable? = dao.deleteFromPath(path)

    fun moveMessage(oldPath: MServerPath, newPath: MServerPath) = dao.moveMessage(oldPath, newPath)

    fun addDemoDataToDB() = generateDemoMessages().forEach { dao.save(it)?.let { e -> throw e } }
}
