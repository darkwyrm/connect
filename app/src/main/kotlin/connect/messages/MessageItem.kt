package connect.messages

import connect.FilterTerm
import javafx.beans.property.SimpleStringProperty
import libkeycard.RandomID
import java.time.LocalDateTime

open class MessageHeaderItem(subject: String = "", id: String = "", format: String = "plain") :
    MsgBaseHeaderItem(subject, id, format)

open class MessageItem(
    subject: String = "", contents: String = "", id: String = "", format: String = "plain",
) : MsgBaseItem(subject, contents, id, format) {

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "type" -> term.compare(subType)
            "date" -> term.compare(date?.toString() ?: "")
            "subject" -> term.compare(subject)
            "from" -> term.compare(fromName) || term.compare(fromAddr)
            "to" -> term.compare(toAddr)
            "cc" -> term.compare(cc)
            "bcc" -> term.compare(bcc)
            "threadid" -> term.compare(threadID)

            else -> super.match(term)
        }
    }

    fun toFullString(): String {
        val out = mutableListOf(
            "Date: $date",
            "From: $fromName",
            "To: $toAddr",
        )
        if (subType.isNotEmpty())
            out.add("Subtype: $subType")
        if (labels.isNotEmpty())
            out.add("Labels: ${getLabelString()}")
        if (attachments.isNotEmpty()) {
            val attStr = attachments.keys.joinToString(", ") {
                "${attachments[it]!!.name} (${attachments[it]!!.mime})"
            }
            out.add("Attachments: $attStr")
        }
        out.addAll(
            listOf(
                "Subject: $subject",
                "",
                "Contents:",
                contents
            )
        )
        return out.joinToString("\n")
    }

    override fun toHeader(): MsgBaseHeaderItem {
        return MessageHeaderItem(subject, id, format).apply {
            this.date = date
            this.fromName = fromName
            this.fromAddr = fromAddr
        }
    }

    private val ccProperty = SimpleStringProperty("")
    var cc: String
        get() = ccProperty.value
        set(value) = ccProperty.set(value)

    private val bccProperty = SimpleStringProperty("")
    var bcc: String
        get() = bccProperty.value
        set(value) = bccProperty.set(value)

    companion object {
        fun new(
            fromNameStr: String, fromAddrStr: String, toStr: String, subject: String,
            contents: String
        ): MessageItem {
            return MessageItem(subject, RandomID.generate().toString()).also {
                it.id = RandomID.generate().toString()
                it.fromName = fromNameStr
                it.fromAddr = fromAddrStr
                it.toAddr = toStr
                it.date = LocalDateTime.now()
                it.threadID = RandomID.generate().toString()
                it.contents = contents
            }
        }
    }
}
