package connect.messages

import connect.controls.ContactView
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Separator
import javafx.scene.control.Tooltip
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import libmensago.MessageAction

/**
 * ConMsgView is a control for viewing and acting upon Contact Requests.
 */
class ConMsgView(
    val model: MessageModel, private val actionHandler: (MessageAction) -> Unit
) : VBox() {

    val conMsgProperty: SimpleObjectProperty<ConMsgItem> = SimpleObjectProperty(null)
    private var conMsg: ConMsgItem?
        get() = conMsgProperty.value
        set(value) {
            conMsgProperty.set(value)
        }

    private val messageView = Label().apply {
        conMsgProperty.subscribe(Runnable { text = conMsg?.contents ?: "" })
        isWrapText = true
    }
    private val infoView = ContactView(model.selectedConItemProperty, Orientation.HORIZONTAL, true)
    private val approveButton = Button("Approve").apply {
        setOnAction { actionHandler(MessageAction.Approve) }
        tooltip = Tooltip("Approve the contact request, allowing the contact to send you messages.")
    }
    private val rejectButton = Button("Reject").apply {
        setOnAction { actionHandler(MessageAction.Reject) }
        tooltip =
            Tooltip("Reject the contact request, but allow them to send you future requests.")
    }
    private val blockButton = Button("Block").apply {
        setOnAction { actionHandler(MessageAction.Block) }
        tooltip = Tooltip("Reject the contact request and block future requests from the sender.")
    }

    init {
        alignment = Pos.TOP_CENTER
        spacing = 5.0
        padding = Insets(5.0)

        conMsgProperty.subscribe { item ->
            if (item != null) {
                if (item.lastAction.isNotEmpty()) {
                    approveButton.isDisable = true
                    rejectButton.isDisable = true
                    blockButton.isDisable = true

                    val action = MessageAction.fromString(item.lastAction)
                    when (action) {
                        MessageAction.Approve -> approveButton.style = "-fx-border-color: blue"
                        MessageAction.Reject -> rejectButton.style = "-fx-border-color: blue"
                        MessageAction.Block -> blockButton.style = "-fx-border-color: blue"
                        else -> {}
                    }
                } else {
                    approveButton.isDisable = false
                    approveButton.style = ""
                    rejectButton.isDisable = false
                    rejectButton.style = ""
                    blockButton.isDisable = false
                    blockButton.style = ""
                }
            }
        }

        val actionButtonBox = HBox().apply {
            alignment = Pos.CENTER
            spacing = 80.0
            children.addAll(approveButton, rejectButton, blockButton)
        }

        conMsgProperty.subscribe(Runnable {
            actionButtonBox.isVisible = conMsg?.subType == "conreq.1"
        })

        // The empty label here functions beautifully as a blank space separator :)
        children.addAll(infoView, Separator(), messageView, Label(), actionButtonBox)
    }
}