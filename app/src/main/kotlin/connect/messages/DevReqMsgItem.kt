package connect.messages

import javafx.beans.property.SimpleObjectProperty
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libmensago.DeviceInfo

/** Header item which represents a New Device Request */
class DevReqMsgHeaderItem(id: String = "") :
    MessageHeaderItem("New Device Request", id, "plain")

/** Message display class which represents a New Device Request */
class DevReqMsgItem(
    devInfo: DeviceInfo, subject: String = "", contents: String = "", id: String = ""
) : MsgBaseItem(subject, contents, id, "plain") {

    private val devInfoProperty = SimpleObjectProperty(devInfo)
    var info: DeviceInfo
        get() = devInfoProperty.value
        set(value) = devInfoProperty.set(value)

    override fun toHeader(): DevReqMsgHeaderItem {
        return DevReqMsgHeaderItem(id).let {
            it.date = date
            it.fromName = fromName
            it.fromAddr = fromAddr
            it.setLabels(getLabelString())
            it
        }
    }

    override fun getInfo(): Result<String> = runCatching { Json.encodeToString(info) }

    override fun setInfo(s: String): Throwable? {
        info = runCatching { Json.decodeFromString<DeviceInfo>(s) }.getOrElse { return it }
        return null
    }
}
