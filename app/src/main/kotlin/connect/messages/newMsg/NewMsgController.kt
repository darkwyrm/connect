package connect.messages.newMsg

import connect.showFatalError
import javafx.scene.layout.Region

class NewMsgController {
    private val model = NewMsgModel()
    var postSendHandler: Runnable? = null
    private val interactor = NewMsgInteractor(model)
    private val viewBuilder = NewMsgViewBuilder(model, this::sendMessage, this::saveDraft)

    fun getView(): Region {
        return viewBuilder.build()
    }

    /** Saves the current message information as a draft */
    fun saveDraft() {
        interactor.saveDraft()?.let {
            showFatalError("Error saving draft.", it)
        }
    }

    fun sendMessage() {
        interactor.sendMessage()?.let {
            showFatalError("Error sending message.", it)
        }
        postSendHandler?.run()
    }
}
