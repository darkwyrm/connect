package connect.messages.newMsg

import javafx.beans.property.SimpleStringProperty
import libkeycard.RandomID

class NewMsgModel {
    val toProperty = SimpleStringProperty("")
    var to: String
        get() = toProperty.value
        set(value) {
            toProperty.value = value
        }

    val subjectProperty = SimpleStringProperty("")
    var subject: String
        get() = subjectProperty.value
        set(value) {
            subjectProperty.value = value
        }

    val contentsProperty = SimpleStringProperty("")
    var contents: String
        get() = contentsProperty.value
        set(value) {
            contentsProperty.value = value
        }

    private val idProperty = SimpleStringProperty(RandomID.generate().toString())
    var id: String
        get() = idProperty.value
        set(value) {
            idProperty.value = value
        }

    private val threadIDProperty = SimpleStringProperty(RandomID.generate().toString())
    var threadID: String
        get() = threadIDProperty.value
        set(value) {
            threadIDProperty.value = value
        }
}
