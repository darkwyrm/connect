package connect.messages

import connect.ChangeHandler
import connect.Filter
import connect.common.buildLabelList
import connect.common.buildModeMenu
import connect.main.AppMode
import connect.main.MainModel
import connect.main.ModeInfo
import javafx.application.Platform
import javafx.geometry.Orientation
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.layout.VBox.setVgrow
import keznacl.Encryptor
import libkeycard.RandomID
import libmensago.MessageAction
import java.time.LocalDateTime


class MessageViewBuilder(
    private val model: MessageModel, private val mainModel: MainModel,
    private val handlers: MessageViewHandlers
) {

    fun build() {
        val info = ModeInfo(
            AppMode.Messages,
            buildMainLayout(),
            KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.CONTROL_DOWN)
        )
        mainModel.modeList.add(info)
    }

    private fun buildMainLayout(): Region {

        val layout = SplitPane().apply {
            orientation = Orientation.HORIZONTAL
            items.addAll(
                buildLabelList(model.filterListProperty, handlers.labelSelection),
                buildMainPane()
            )
            setDividerPositions(.3)
        }

        return VBox().apply {
            children.addAll(buildMenu(), layout)
            setVgrow(layout, Priority.ALWAYS)
        }
    }

    private fun buildMenu(): Region {
        return MenuBar(buildMessageMenu(), buildModeMenu(mainModel))
    }

    private fun buildMainPane(): Region {
        val fromNameColumn = TableColumn<MsgBaseHeaderItem, String>("From").apply {
            cellValueFactory = PropertyValueFactory("fromName")
        }
        val subjectColumn = TableColumn<MsgBaseHeaderItem, String>("Subject").apply {
            cellValueFactory = PropertyValueFactory("subject")
        }
        val dateColumn = TableColumn<MsgBaseHeaderItem, LocalDateTime>("Date").apply {
            cellValueFactory = PropertyValueFactory("date")
            setCellFactory { FriendlyDateTableCell() }
        }

        val msgView = TextArea().apply {
            isWrapText = true
            isEditable = false
            textProperty().bind(model.selectedMsgContentsProperty)
            visibleProperty().bind(model.selectedItemClassProperty.isEqualTo("MessageItem"))
        }
        val conReqView = ConMsgView(model, handlers.handleConReqAction).apply {
            model.selectedMsgProperty.subscribe { item ->
                if (item != null && model.selectedMsg is ConMsgItem)
                    conMsgProperty.set(model.selectedMsg as ConMsgItem)
            }
            visibleProperty().bind(conMsgProperty.isNotNull)
        }
        val devReqView = DeviceRequestView(model, handlers.handleDevReqAction).apply {
            visibleProperty().bind(model.selectedItemClassProperty.isEqualTo("DevReqMsgItem"))
        }

        val msgList = TableView<MsgBaseHeaderItem>().apply {
            columns.addAll(fromNameColumn, subjectColumn, dateColumn)

            itemsProperty().bindBidirectional(model.msgListProperty)
            visibleProperty().bind(model.msgListProperty.emptyProperty().not())
            selectionModel.selectedItemProperty().subscribe { oldItem, newItem ->
                handlers.msgSelection(oldItem, newItem)
            }
        }

        if (model.msgList.isNotEmpty()) {
            msgList.selectionModel.select(0)
            Platform.runLater { msgList.requestFocus() }
        }

        val viewStack = StackPane().apply {
            children.addAll(msgView, conReqView, devReqView)
        }

        return SplitPane().apply {
            orientation = Orientation.VERTICAL
            items.addAll(msgList, viewStack)
            setDividerPositions(.4)
        }
    }

    private fun buildMessageMenu(): Menu {
        val itemNew = MenuItem("_New").apply {
            setOnAction { handlers.showNewMsg() }
            accelerator = KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN)
            disableProperty().bind(mainModel.isProfileLocalProperty)
        }

        val itemTrash = MenuItem("Move to _Trash").apply {
            setOnAction { handlers.trashMsg() }
            accelerator = KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN)
        }

        val itemRestore = MenuItem("_Restore from Trash").apply {
            setOnAction { handlers.restoreMsg() }
        }

        val itemEmptyTrash = MenuItem("_Empty Trash").apply {
            setOnAction { handlers.emptyTrash() }
        }

        model.selectedFilterProperty.subscribe { newFilter ->
            newFilter?.let {
                itemTrash.text = if (it.name == "Trash") "_Delete" else "Move to _Trash"
                itemRestore.isDisable = it.name != "Trash"
            }
        }

        return Menu(
            "_Message", null,
            itemNew,
            SeparatorMenuItem(),
            itemTrash,
            itemRestore,
            SeparatorMenuItem(),
            itemEmptyTrash,
        )
    }
}

data class MessageViewHandlers(
    val standardFilterSelection: ChangeHandler<Filter?>,
    val labelSelection: ChangeHandler<Filter?>,
    val msgSelection: ChangeHandler<MsgBaseHeaderItem?>,
    val showNewMsg: () -> Unit,
    val restoreMsg: () -> Unit,
    val trashMsg: () -> Unit,
    val emptyTrash: () -> Unit,
    val handleConReqAction: (MessageAction) -> Unit,
    val handleDevReqAction: (RandomID, MessageAction, Encryptor?) -> Unit,
)
