package connect

import connect.main.AppMode
import connect.main.MainController
import connect.profiles.ProfileDAO
import connect.profiles.ProfileManager
import connect.setupwizard.*
import connect.sync.OnlineWorker
import connect.sync.OutgoingHandler
import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import keznacl.SHAPassword
import libmensago.DebugDNSHandler
import libmensago.db.KDB
import libmensago.resolver.KCResolver
import utilities.PasswordFactory
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    StartupOptions.init(args)
    if (StartupOptions.debugMode) {
        val db = KDB(StartupOptions.testDBConfig!!)
        db.connect().getOrThrow()
        KCResolver.dns = DebugDNSHandler(db)
    }

    if (StartupOptions.testMode) {
        if (StartupOptions.useWeakPasswordHashing) {
            PasswordFactory.setDefaultAlgorithm("SHA3-256")
            (PasswordFactory.defaultHasher as SHAPassword).setIterations(10)
        }
    }

    with(ProfileManager) {

        // This code ensures there is a profile to use at startup and determines if this is the
        // first time starting on said profile.

        StartupOptions.firstStartup = ensureDefaultProfile().getOrThrow()

        if (StartupOptions.startupProfile.isNotEmpty()) {
            if (!model.profileNames.contains(StartupOptions.startupProfile)) {
                createProfile(StartupOptions.startupProfile)?.let {
                    println("Couldn't create requested profile '${StartupOptions.startupProfile}'")
                    throw it
                }
                StartupOptions.firstStartup = true
            }
            activateProfile(StartupOptions.startupProfile)?.let { throw it }
        } else {
            if (model.defaultProfile.isNotEmpty())
                activateProfile(model.defaultProfile)?.let { throw it }
            else {
                println("BUG: No default profile to activate")
                return
            }
        }

        // One final check for first startup. This is necessary if the program was opened and
        // the user chose to quit from the first-time startup window.
        if (!StartupOptions.firstStartup) {
            StartupOptions.firstStartup = ProfileDAO().isFirstStartup().getOrElse {
                println("Couldn't check for first-time status in database\n $it")
                return
            }
        }

        OutgoingHandler.loadTaskQueue()
    }

    Application.launch(App::class.java)
}

class App : Application() {
    private val mainController = MainController()
    private val updater = OnlineWorker(mainController, ProfileManager.model)

    override fun start(stage: Stage) {
        mainController.registerModes()
        mainController.setProfileLocality(
            ProfileManager.model.activeProfile!!.isLocal().getOrThrow()
        )

        // DEBUG: TODO: Set update polling period to 10 seconds for update handling testing
        updater.handler.updatePeriodInSeconds = 10

        updater.launch()
        showMainWindow(stage)
    }

    override fun stop() {
        updater.shutdown()
        OutgoingHandler.shutdown()
    }

    private fun showMainWindow(stage: Stage) {
        val scene = Scene(mainController.getView())
        stage.scene = scene
        stage.icons.add(Image("appicon.png"))
        stage.title = "Mensago Connect (${ProfileManager.model.activeProfile?.name})"
        stage.minWidth = 600.0
        stage.minHeight = 500.0
        stage.width = 800.0
        stage.height = 600.0

        Platform.runLater { mainController.setMode(AppMode.Messages) }
        stage.show()

        if (StartupOptions.firstStartup)
            doFirstTimeStartup()
    }

    private fun doFirstTimeStartup() {
        while (true) {
            val firstTimeResult = showFirstTimeWindow()
            if (firstTimeResult.isFailure) continue
            val setupType = firstTimeResult.getOrThrow()

            when (setupType) {
                SetupType.Quit -> exitProcess(0)

                SetupType.Login -> {
                    val addr = showLoginDialog()
                    if (addr != null) {
                        mainController.setProfileLocality(false)
                        ProfileDAO().clearFirstStartup()?.let { throw it }
                        break
                    }
                }

                SetupType.RegCode -> {
                    if (!showRegCodeDialog()) continue
                    mainController.setProfileLocality(false)
                    ProfileDAO().clearFirstStartup()?.let { throw it }
                    addWelcomeContent(mainController)
                    break
                }

                SetupType.NewAccount -> {
                    if (!showSignUpDialog()) continue
                    mainController.setProfileLocality(false)
                    ProfileDAO().clearFirstStartup()?.let { throw it }
                    addWelcomeContent(mainController)
                    break
                }

                else -> {
                    // Assuming local-only. We don't have to do anything for that one.
                    ProfileDAO().clearFirstStartup()?.let { throw it }
                    addWelcomeContent(mainController)
                    break
                }
            }
        }
    }
}
