package connect.sync

import keznacl.onFalse
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.*
import utilities.DBDataException
import utilities.DBProxy
import utilities.getRandomID
import java.nio.file.Paths
import java.sql.ResultSet

/** This class just brokers database access for SyncItem instances */
class SyncDAO {

    fun initUpdateTables(): Throwable? {

        val initCommands = listOf(
            // Updates downloaded from servers that need to be processed
            """CREATE TABLE IF NOT EXISTS downsync (
                rowid INT PRIMARY KEY AUTO_INCREMENT,
                id    TEXT NOT NULL UNIQUE,
                op  TEXT NOT NULL,
                waddress TEXT NOT NULL,
                data  TEXT NOT NULL,
                unixtime  TEXT NOT NULL,
                devid TEXT NOT NULL,
                done BOOL NOT NULL
            );""",

            // Updates queued for upload to the associated server.
            """CREATE TABLE IF NOT EXISTS upsync (
                id UUID PRIMARY KEY NOT NULL UNIQUE,
                op  TEXT NOT NULL,
                localpath TEXT NOT NULL,
                serverpath TEXT,
                replacepath TEXT,
                sender  TEXT,
                receiver  TEXT
            );""",
        )
        val db = DBProxy.get()
        for (cmd in initCommands)
            db.add(cmd)?.let { return it }
        return db.executeBatch()
    }

    /**
     * Add an update item received from the server. This is not necessarily a task which needs done.
     * Instead it is just a record of what events have taken place on the server.
     */
    fun queueIncomingItem(rec: SyncItem): Throwable? {
        return DBProxy.get().execute(
            "INSERT INTO downsync(id,op,waddress,data,unixtime,devid,done) " +
                    "VALUES(?,?,?,?,?,?,false)",
            rec.id, rec.op, rec.waddress, rec.value, rec.unixtime, rec.source
        )
    }

    /** Remove an update item. */
    fun deleteIncomingItem(id: RandomID): Throwable? =
        DBProxy.get().execute("DELETE FROM downsync WHERE id = ?", id)

    /** Returns true if the update item queue has items */
    fun hasIncomingRecords(id: RandomID): Result<Boolean> {
        val rs = DBProxy.get().query("SELECT COUNT(*) FROM downsync WHERE id=?", id)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        return (rs.getInt(1) > 0).toSuccess()
    }

    /**
     * Add a task for outgoing processing, such as sending a message or uploading a file/
     */
    fun queueOutgoingTask(task: OutgoingTask): Throwable? {
        return when (task.type) {
            OutgoingTaskType.Send -> {
                task as OutgoingTask.Send
                DBProxy.get().execute(
                    "INSERT INTO upsync(id,op,localpath,sender,receiver) VALUES(?,?,?,?,?)",
                    RandomID.generate(), task.type, task.localPath, task.sender, task.receiver
                )
            }

            OutgoingTaskType.Upload -> {
                task as OutgoingTask.Upload
                DBProxy.get().execute(
                    "INSERT INTO upsync(id,op,localpath,serverpath) VALUES(?,?,?,?)",
                    RandomID.generate(), task.type, task.localPath, task.serverPath
                )
            }

            OutgoingTaskType.Replace -> {
                task as OutgoingTask.Replace
                DBProxy.get().execute(
                    "INSERT INTO upsync(id,op,localpath,serverpath,replacepath) VALUES(?,?,?,?,?)",
                    RandomID.generate(), task.type, task.localPath, task.serverPath,
                    task.replacePath
                )
            }
        }
    }

    /** Load all tasks awaiting outgoing processing */
    fun loadOutgoingTasks(): Result<List<OutgoingTask>> {
        val recList = mutableListOf<OutgoingTask>()
        val rs = DBProxy.get().query("SELECT * FROM upsync ORDER BY id")
            .getOrElse { return it.toFailure() }

        while (rs.next()) {
            val idStr = rs.getString("id")

            val rid = RandomID.fromString(idStr)
                ?: return DBDataException("Bad incoming update record id $idStr")
                    .toFailure()

            recList.add(
                makeOutgoingTask(rs, rid).getOrElse { return it.toFailure() }
            )
        }
        return recList.toSuccess()
    }

    /** Deletes an outgoing task from the queue */
    fun deleteOutgoingTask(id: RandomID): Throwable? =
        DBProxy.get().execute("DELETE FROM upsync WHERE id = ?", id)

    /** Loads everything in the queue of incoming update items */
    fun loadIncomingItems(): Result<List<SyncItem>> {
        val recList = mutableListOf<SyncItem>()
        val rs = DBProxy.get().query("SELECT * FROM downsync ORDER BY unixtime LIMIT 1000")
            .getOrElse { return it.toFailure() }

        while (rs.next()) {
            val idStr = rs.getString("id")

            val rid = RandomID.fromString(idStr)
                ?: return DBDataException("Bad incoming update record id $idStr")
                    .toFailure()

            recList.add(
                makeIncomingItem(rs, rid).getOrElse { return it.toFailure() }
            )
        }
        return recList.toSuccess()
    }

    private fun makeIncomingItem(rs: ResultSet, rid: RandomID): Result<SyncItem> {
        val op = SyncOp.fromString(rs.getString("op"))
            ?: return DBDataException("Bad update op in downsync record $rid").toFailure()

        val unixtime = rs.getLong("unixtime")

        val value = SyncValue.fromString(op, rs.getString("data"))
            .getOrElse { return it.toFailure() }

        val source = rs.getRandomID("devid")
            ?: return DBDataException("Bad incoming update record id $rid")
                .toFailure()

        val waddr = WAddress.fromString(rs.getString("waddress"))
            ?: return DBDataException("Bad incoming update address for record $rid")
                .toFailure()

        return SyncItem(rid, op, waddr, value, unixtime, source).toSuccess()
    }

    private fun makeOutgoingTask(rs: ResultSet, rid: RandomID): Result<OutgoingTask> {
        val op = OutgoingTaskType.fromString(rs.getString("op"))
            ?: return DBDataException("Bad task op in upsync record $rid").toFailure()

        val localPath = Paths.get(rs.getString("localpath"))
            ?: return DBDataException("Bad task local path in upsync record $rid").toFailure()

        return when (op) {
            OutgoingTaskType.Send -> {
                val msgSize = kotlin.runCatching { rs.getString("size").toInt() }
                    .getOrElse {
                        return DBDataException("Bad message size in upsync record $rid")
                            .toFailure()
                    }
                val sender = DeliveryTarget.fromString(rs.getString("sender"))
                    ?: return DBDataException("Bad sender in upsync record $rid").toFailure()
                val receiver = DeliveryTarget.fromString(rs.getString("receiver"))
                    ?: return DBDataException("Bad receiver in upsync record $rid").toFailure()
                OutgoingTask.Send(localPath, msgSize, sender, receiver, rid).toSuccess()
            }

            OutgoingTaskType.Upload -> {
                val serverPath = MServerPath.fromString(rs.getString("serverpath"))
                    ?: return DBDataException("Bad server path in upsync record $rid").toFailure()
                OutgoingTask.Upload(localPath, serverPath, rid).toSuccess()
            }

            OutgoingTaskType.Replace -> {
                val serverPath = MServerPath.fromString(rs.getString("serverpath"))
                    ?: return DBDataException("Bad server path in upsync record $rid").toFailure()
                val replacePath = MServerPath.fromString(rs.getString("replacepath"))
                    ?: return DBDataException("Bad replace path in upsync record $rid").toFailure()
                OutgoingTask.Replace(localPath, serverPath, replacePath, rid).toSuccess()
            }
        }
    }
}