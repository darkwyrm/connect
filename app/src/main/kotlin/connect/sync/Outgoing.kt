package connect.sync

import connect.Client
import connect.LogDAO
import connect.profiles.KeyDAO
import connect.profiles.ProfileManager
import connect.profiles.getActiveProfile
import connect.showFatalError
import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import libmensago.Envelope
import libmensago.MServerPath
import libmensago.Message
import libmensago.ResourceNotFoundException
import libmensago.commands.send
import libmensago.commands.sendLarge
import org.apache.commons.io.FileUtils
import utilities.LoginRequiredException
import utilities.MissingProfileException
import java.io.File
import java.lang.Thread.currentThread
import java.nio.file.Path
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.io.path.exists

/**
 * The OutgoingHandler object encapsulates all the state needed to track the various threads
 * processing outgoing tasks. Uploads and sending messages are both handled by theis object.
 */
object OutgoingHandler {
    var useThreads = true

    // The task queue for outgoing worker threads to process work from.
    private val outgoingQueue = ConcurrentLinkedQueue<OutgoingTask>()
    private val outgoingConcurrentMap = ConcurrentHashMap<Thread, Boolean>()
    private val outgoingThreads = Collections.newSetFromMap(outgoingConcurrentMap)
    private val quitOutgoingThreads = AtomicBoolean(false)

    /**
     * Loads into memory the queue of outgoing tasks (uploads, etc) and returns the number of tasks
     * in the queue at the time the call was made.
     */
    fun loadTaskQueue(): Result<Int> {
        val dao = SyncDAO()
        val tasks = dao.loadOutgoingTasks().getOrElse { return it.toFailure() }
        tasks.forEach { task -> outgoingQueue.add(task) }
        return tasks.size.toSuccess()
    }

    fun shutdown() {
        if (!useThreads) return

        quitOutgoingThreads.set(true)
        while (outgoingThreads.size > 0)
            outgoingThreads.first().join()
    }

    /**
     * Add an upload task to the outgoing queue. It also spawns a worker thread to handle the task
     * if the thread pool is not running at capacity.
     */
    fun queueUpload(
        localpath: Path, serverpath: MServerPath, replace: MServerPath? = null
    ): Result<Thread> {
        val task = if (replace != null)
            OutgoingTask.Replace(localpath, serverpath, replace)
        else
            OutgoingTask.Upload(localpath, serverpath)

        if (!useThreads) {
            val profile = ProfileManager.model.activeProfile
                ?: return MissingProfileException().toFailure()
            val addr = profile.getMAddress().getOrElse { return it.toFailure() }
            val client = Client()
            client.login(addr).getOrElse { throw it }
            val err = processTask(task, client)
            client.logout()?.let { return it.toFailure() }
            return err?.toFailure() ?: currentThread().toSuccess()
        }

        SyncDAO().queueOutgoingTask(task)?.let { return it.toFailure() }
        outgoingQueue.add(task)
        return Thread.ofVirtual().start { outgoingWorker() }.toSuccess()
    }

    /**
     * Add a message task to the outgoing queue. It also spawns a worker thread to handle the task
     * if the thread pool is not running at capacity.
     */
    fun queueMessage(
        path: Path, sender: DeliveryTarget, receiver: DeliveryTarget
    ): Result<Thread> {
        if (!path.exists()) return ResourceNotFoundException().toFailure()
        val size =
            runCatching { FileUtils.sizeOf(path.toFile()) }.getOrElse { return it.toFailure() }

        if (!useThreads) {
            val profile = ProfileManager.model.activeProfile
                ?: return MissingProfileException().toFailure()
            val addr = profile.getMAddress().getOrElse { return it.toFailure() }
            val client = Client()
            client.login(addr).getOrThrow()
            val err = sendSealedUnsafe(client, path.toFile(), receiver)
            client.disconnect()
            return err?.toFailure() ?: currentThread().toSuccess()
        }

        val task = OutgoingTask.Send(path, size.toInt(), sender, receiver)
        SyncDAO().queueOutgoingTask(task)?.let { return it.toFailure() }
        outgoingQueue.add(task)
        return Thread.ofVirtual().start { outgoingWorker() }.toSuccess()
    }

    /**
     * This function only exists for testing. It merely turns off the flag signalling quitting time for
     * worker threads
     */
    @Suppress("unused")
    fun resetWorkers() = quitOutgoingThreads.set(false)

    /**
     * Pulls a task out of the outgoing task queue. This code is mostly just needed for testing and
     * shouldn't be needed anywhere else.
     */
    fun getOutgoingTask(): OutgoingTask? = outgoingQueue.poll()

    /** Processes an outgoing task item. */
    fun processTask(task: OutgoingTask, client: Client): Throwable? {
        val file = task.localPath.toFile()
        when (task.type) {
            OutgoingTaskType.Send -> {
                task as OutgoingTask.Send
                val err = sendSealedUnsafe(client, file, task.receiver)
                if (err != null) {
                    LogDAO().error(
                        "OutgoingHandler.processTask",
                        "Connect couldn't pass a message to the server for delivery.",
                        err.stackTraceToString()
                    )?.let { throw it }
                    outgoingQueue.add(task)
                    return err
                }
            }

            OutgoingTaskType.Upload -> {
                task as OutgoingTask.Upload
                val err = client.upload(task.localPath.toString(), task.serverPath)
                    .exceptionOrNull()
                if (err != null) {
                    LogDAO().error(
                        "OutgoingHandler.processTask",
                        "Connect couldn't upload a file to the server.",
                        err.stackTraceToString()
                    )?.let { throw it }
                    outgoingQueue.add(task)
                    return err
                }
            }

            OutgoingTaskType.Replace -> {
                task as OutgoingTask.Replace
                val err =
                    client.upload(task.localPath.toString(), task.serverPath, task.replacePath)
                        .exceptionOrNull()
                if (err != null) {
                    LogDAO().error(
                        "OutgoingHandler.processTask",
                        "Connect couldn't upload a file to the server.",
                        err.stackTraceToString()
                    )?.let { throw it }
                    outgoingQueue.add(task)
                    return err
                }
            }
        }
        return null
    }

    fun processAllTasks(client: Client): Throwable? {
        var task = getOutgoingTask()
        while (task != null) {
            processTask(task, client)?.let { e -> return e }
            task = getOutgoingTask()
        }
        return null
    }

    /** Sends a message which has already been sealed in an envelope and saved to a file. */
    fun sendSealedUnsafe(client: Client, file: File, target: DeliveryTarget): Throwable? {
        if (!file.exists()) return ResourceNotFoundException()
        if (!client.isLoggedIn()) return LoginRequiredException()

        if (file.length() > 0x100_000 * 25)
            return sendLarge(client.getConnection(), file.toString(), target.domain)

        val envStr = runCatching { file.readText() }.getOrElse { return it }
        return send(client.getConnection(), target.domain, envStr)
    }

    /**
     * Sends the supplied message immediately. The message will be lost if the program is closed
     * during transit.
     */
    fun sendUnsafe(client: Client, msg: Message): Throwable? {
        if (!client.isLoggedIn()) return LoginRequiredException()

        // Sanity check on the message
        val profile = getActiveProfile().getOrElse { return it }
        val userAddr = profile.getWAddress().getOrElse { return it }

        if (msg.from.id != userAddr.id)
            return BadValueException("Message sender ID must match profile workspace ID")
        if (msg.from.domain != userAddr.domain)
            return BadValueException("Message sender domain must match profile workspace domain")

        val recipientKey = KeyDAO().loadContactKey(msg.to).getOrElse { return it }
        val env = Envelope.seal(
            recipientKey, msg.getAddressPair(), msg.toPayload().getOrElse { return it }
        ).getOrElse { return it }

        return send(
            client.getConnection(), msg.to.domain, env.toStringResult().getOrElse { return it }
        )
    }

    private fun outgoingWorker() {
        val profile = ProfileManager.model.activeProfile ?: return

        val addr = profile.getMAddress().getOrElse {
            showFatalError(
                "Connect couldn't look up your Mensago address when trying to talk to the server.",
                it
            )
            outgoingThreads.remove(currentThread())
            return
        }

        val client = Client()
        client.login(addr).getOrThrow()

        outgoingThreads.add(currentThread())
        val task = outgoingQueue.poll() ?: return

        if (!quitOutgoingThreads.get()) {
            var err = processTask(task, client)
            if (err != null) return

            err = SyncDAO().deleteOutgoingTask(task.id)
            if (err != null)
                LogDAO().error(
                    "Outgoing worker", "Connect encountered a database error.",
                    err.stackTraceToString()
                )
        }
        outgoingThreads.remove(currentThread())
    }
}
