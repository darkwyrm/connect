package connect.sync

import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libkeycard.WAddress
import libmensago.EntrySubject
import libmensago.MServerPath
import java.nio.file.Path

/**
 * A DeliveryTarget is a special data class which can be either a full workspace address or just a
 * domain. It is used in envelope outgoing headers.
 */
class DeliveryTarget(var domain: Domain, var id: RandomID? = null) {

    /** Returns true if the subject represented is a user */
    fun isUser(): Boolean {
        return (id != null)
    }

    /** Converts the DeliveryTarget into an EntrySubject. */
    fun toEntrySubject(): EntrySubject {
        return if (id != null) EntrySubject(domain, UserID.fromWID(id!!))
        else EntrySubject(domain)
    }

    override fun toString(): String {
        return if (id != null) "$id/$domain" else domain.toString()
    }

    companion object {
        fun fromString(s: String): DeliveryTarget? {
            val waddr = WAddress.fromString(s)
            val dom = Domain.fromString(s)
            return DeliveryTarget(waddr?.domain ?: dom ?: return null, waddr?.id)
        }
    }
}

enum class OutgoingTaskType {
    Send,
    Upload,
    Replace;

    override fun toString(): String {
        return when (this) {
            Send -> "Send"
            Upload -> "Upload"
            Replace -> "Replace"
        }
    }

    companion object {
        fun fromString(s: String): OutgoingTaskType? {
            return when (s.lowercase()) {
                "Send" -> Send
                "Upload" -> Upload
                "Replace" -> Replace
                else -> null
            }
        }
    }
}

/**
 * Used for giving the necessary information for outgoing worker threads to do their jobs.
 */
sealed class OutgoingTask private constructor(
    val type: OutgoingTaskType, val localPath: Path,
    val id: RandomID
) {

    class Send(
        localPath: Path, val size: Int, val sender: DeliveryTarget, val receiver: DeliveryTarget,
        id: RandomID = RandomID.generate()
    ) : OutgoingTask(OutgoingTaskType.Send, localPath, id)

    class Upload(
        localPath: Path, val serverPath: MServerPath, id: RandomID = RandomID.generate()
    ) : OutgoingTask(OutgoingTaskType.Upload, localPath, id)

    class Replace(
        localPath: Path, val serverPath: MServerPath, val replacePath: MServerPath,
        id: RandomID = RandomID.generate()
    ) : OutgoingTask(OutgoingTaskType.Upload, localPath, id)
}
