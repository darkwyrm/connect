package connect.setupwizard

import connect.Client
import connect.StartupOptions
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import libkeycard.MAddress
import libmensago.WorkspaceStatus

fun showLoginDialog(): MAddress? {

    val client = Client()
    var address: MAddress?

    val loginInfo: Pair<WorkspaceStatus, String>
    while (true) {
        val login = LoginDialog()
        val result = login.showAndWait()
        if (result.isEmpty || result.get().buttonData == ButtonBar.ButtonData.CANCEL_CLOSE)
            return null

        // We got a Login button press. Attempt to log in with the credentials.
        address = MAddress.fromString(login.addressField.text)
        if (address == null) {
            Alert(Alert.AlertType.ERROR).apply {
                this.title = "Error"
                this.contentText = "Your Mensago address doesn't appear to be valid."
            }.showAndWait()
            continue
        }

        val loginResult = client.login(address, login.passwordField.text)
        if (loginResult.isSuccess) {
            loginInfo = loginResult.getOrNull()!!
            break
        }

        Alert(Alert.AlertType.ERROR).apply {
            this.title = "Error"
            this.contentText = loginResult.toString()
        }.showAndWait()
    }

    return if (showWorkspaceStatusAlert(loginInfo)) address else null
}

/**
 * A Dialog window which logs into a Mensago server and returns the address on success.
 */
class LoginDialog : Dialog<ButtonType>() {
    val addressField = TextField(if (StartupOptions.debugMode) "admin/example.com" else "")
    val passwordField = PasswordField().apply {
        if (StartupOptions.debugMode)
            text = "Linguini2Pegboard*Album"
    }

    init {
        val headerVBox = VBox().run {
            padding = Insets(10.0)
            alignment = Pos.TOP_CENTER
            this
        }
        val title = Label("Please sign in with your Mensago address")
        title.style = "-fx-font-weight: bold"
        headerVBox.children.add(title)
        dialogPane.header = headerVBox

        val bodyVBox = VBox()
        bodyVBox.alignment = Pos.CENTER
        dialogPane.children.add(bodyVBox)

        val usernameLabel = Label("Mensago Address:")
        val passwordLabel = Label("Password:")

        val gridPane = GridPane().apply {
            hgap = 10.0
            vgap = 10.0

            add(usernameLabel, 0, 0)
            add(addressField, 1, 0)

            add(passwordLabel, 0, 1)
            add(passwordField, 1, 1)
        }
        bodyVBox.children.add(gridPane)

        val cancelButton = ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
        dialogPane.buttonTypes.add(cancelButton)
        val loginButton = ButtonType("Login", ButtonBar.ButtonData.OK_DONE)
        dialogPane.buttonTypes.add(loginButton)

        dialogPane.content = bodyVBox

    }
}

private fun showWorkspaceStatusAlert(status: Pair<WorkspaceStatus, String>): Boolean {
    return when (status.first) {
        WorkspaceStatus.Active -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Success"
                this.contentText = "Login successful."
            }.showAndWait()
            true
        }

        WorkspaceStatus.Approved, WorkspaceStatus.Unknown, WorkspaceStatus.Local -> {
            /* We should never be here */
            false
        }

        WorkspaceStatus.Pending -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Pending Approval"
                this.contentText = "The server requires approval for this device. Please use " +
                        "another device that is already logged in to approve this one. Use the " +
                        "following approval code:\n\n${status.second}\n\n" +
                        "If you have no other devices for this workspace, please contact your " +
                        "IT support for a device reset."
            }.showAndWait()
            true
        }

        WorkspaceStatus.Archived -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Workspace Unavailable"
                this.contentText = "This workspace has been permanently archived " +
                        "and cannot be logged into."
            }.showAndWait()
            false
        }

        WorkspaceStatus.BalanceDue -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Balance Due"
                this.contentText = "Online functionality for this workspace is temporarily " +
                        "unavailable because the account associated with it has a balance due. " +
                        "Please contact customer service for assistance."
            }.showAndWait()
            true
        }

        WorkspaceStatus.Blocked -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Device Blocked"
                this.contentText = "This device has been blocked from logging into this " +
                        "workspace. Please contact your IT support for assistance."
            }.showAndWait()
            false
        }

        WorkspaceStatus.Disabled, WorkspaceStatus.Suspended -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Workspace Unavailable"
                this.contentText = "This workspace has been temporarily disabled. " +
                        "Please contact your IT support for assistance."
            }.showAndWait()
            true
        }

        WorkspaceStatus.Preregistered -> {
            Alert(Alert.AlertType.INFORMATION).apply {
                this.title = "Registration Code Required"
                this.contentText = "This workspace has already been set up by an administrator. " +
                        "Please contact your IT support for the registration code to finish " +
                        "setting up your account."
            }.showAndWait()
            false
        }
    }
}
