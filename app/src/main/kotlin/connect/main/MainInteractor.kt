package connect.main

class MainInteractor(private val model: MainModel) {

    fun selectMode(mode: AppMode) {
        model.modeList.forEach { it.view.isVisible = (it.mode == mode) }
        model.selectedMode = mode
    }
}