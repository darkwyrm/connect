package connect.main

import connect.FilterableItem
import libmensago.MServerPath

/**
 * The FilterItemProcessor interface is for classes which process FilterableItem instances in
 * different ways. The main instance of this interface is MainController, but the
 * TestItemProcessor class also implements it for integration testing.
 */
interface FilterItemProcessor {

    /** Pass a new data item to the appropriate mode controller */
    fun processNewItem(item: FilterableItem): Throwable?

    /** Changes an item's server-side path */
    fun processMoveItem(oldPath: MServerPath, newPath: MServerPath)

    /** Replaces the item at the specified path with the new one */
    fun processReplaceItem(oldItem: MServerPath, newItem: FilterableItem)

    /** Update contact information based on the item passed to it */
    fun updateContactInfo(item: FilterableItem): Throwable?
}
