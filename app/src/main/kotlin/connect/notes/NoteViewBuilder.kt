package connect.notes

import connect.ChangeHandler
import connect.Filter
import connect.common.buildLabelList
import connect.common.buildModeMenu
import connect.common.getStringDialog
import connect.main.AppMode
import connect.main.MainModel
import connect.main.ModeInfo
import javafx.application.Platform
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.scene.layout.VBox.setVgrow


class NoteViewBuilder(
    private val model: NoteModel,
    private val mainModel: MainModel,
    private val handlers: NoteViewHandlers
) {

    fun build() {
        val info = ModeInfo(
            AppMode.Notes,
            buildMainLayout(),
            KeyCodeCombination(KeyCode.DIGIT5, KeyCombination.CONTROL_DOWN)
        )
        mainModel.modeList.add(info)
    }

    private fun buildMainLayout(): Region {

        val layout = SplitPane().apply {
            orientation = Orientation.HORIZONTAL
            items.addAll(
                buildLabelList(model.labelListProperty, handlers.labelSelection),
                buildListAndEditor()
            )
            setDividerPositions(.2)
        }

        return VBox().apply {
            children.addAll(buildMenu(), layout)
            setVgrow(layout, Priority.ALWAYS)
        }
    }

    private fun buildMenu(): Region {
        return MenuBar(buildNoteMenu(), buildModeMenu(mainModel))
    }

    private fun buildListAndEditor(): Region {
        val editor = TextArea().apply {
            isWrapText = true
        }
        val list = ListView(model.noteListProperty).apply {
            editor.textProperty().bindBidirectional(model.selectedNoteContentsProperty)
            selectionModel.selectedItemProperty().subscribe { oldItem, newItem ->
                handlers.noteSelection(oldItem, newItem)
            }
        }

        // Whenever the note list changes and there isn't a selection already, select the first item
        model.noteListProperty.subscribe(Runnable {
            if (list.selectionModel.selectedIndex < 0 && model.noteList.isNotEmpty())
                Platform.runLater { list.selectionModel.select(0) }
        })

        if (model.noteList.isNotEmpty()) {
            list.selectionModel.select(0)
            Platform.runLater { list.requestFocus() }
        }

        val splitter = SplitPane().apply {
            orientation = Orientation.VERTICAL
            items.addAll(list, editor)
            setDividerPositions(.3)
        }
        return VBox().apply {
            padding = Insets(0.0)
            children.addAll(splitter)
            setVgrow(splitter, Priority.ALWAYS)
        }
    }

    private fun buildNoteMenu(): Menu {
        val itemNew = MenuItem("_New").apply {
            accelerator = KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN)
            setOnAction { handlers.newItem() }
        }

        val itemRename = MenuItem("_Rename").apply {
            accelerator = KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN)
            setOnAction {
                getStringDialog("Rename Note", "Enter the new name for the note")
                    ?.let { handlers.renameSelection(it) }
            }
        }

        val itemTrash = MenuItem("Move to _Trash").apply {
            accelerator = KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_DOWN)
            setOnAction { handlers.trashSelection() }
        }

        val itemRestore = MenuItem("Restore _from Trash").apply {
            setOnAction { handlers.restoreSelection() }
        }

        val itemEmptyTrash = MenuItem("_Empty Trash").apply {
            setOnAction { handlers.emptyTrash() }
        }

        return Menu(
            "_Note", null,
            itemNew,
            itemRename,
            SeparatorMenuItem(),
            itemTrash,
            itemRestore,
            itemEmptyTrash,
        )
    }
}

data class NoteViewHandlers(
    val noteSelection: ChangeHandler<NoteHeaderItem?>,
    val labelSelection: ChangeHandler<Filter?>,
    val renameSelection: (name: String) -> Unit,
    val trashSelection: () -> Unit,
    val restoreSelection: () -> Unit,
    val emptyTrash: () -> Unit,
    val newItem: () -> Unit,
)
