package connect.notes

import connect.Filter
import connect.FilterTerm
import connect.FilterableHeaderItem
import connect.FilterableItem
import javafx.beans.property.SimpleStringProperty

class NoteHeaderItem(title: String = "", id: String = "", format: String = "plain") :
    FilterableHeaderItem(id, format) {

    override fun toString(): String = title

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "title" -> term.compare(title)

            else -> super.match(term)
        }
    }

    private val titleProperty = SimpleStringProperty(title)
    var title: String
        get() = titleProperty.value
        set(value) = titleProperty.set(value)
}

/**
 * FilterableDataItem which contains a note's contents along with all of its other properties
 */
class NoteItem(
    title: String = "",
    contents: String = "",
    id: String = "",
    format: String = "plain",
) : FilterableItem(contents, id, format) {

    override fun toString(): String {
        return "$title ($id)"
    }

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "title" -> term.compare(title)

            else -> super.match(term)
        }
    }

    fun toHeader(): NoteHeaderItem {
        return NoteHeaderItem(title, format, id)
    }

    private val titleProperty = SimpleStringProperty(title)
    var title: String
        get() = titleProperty.value
        set(value) = titleProperty.set(value)

    private val serverPathProperty = SimpleStringProperty("")
    var serverPath: String
        get() = serverPathProperty.value
        set(value) = serverPathProperty.set(value)
}
