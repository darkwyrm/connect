package connect.notes

import libkeycard.RandomID
import java.time.Instant
import java.util.*

internal val loremIpsum: String =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
            "labore et dolore magna aliqua. Aliquam nulla facilisi cras fermentum odio eu feugiat " +
            "pretium. Diam quis enim lobortis scelerisque fermentum. Aliquam sem et tortor " +
            "consequat id porta nibh. Quis eleifend quam adipiscing vitae. Massa tempor nec " +
            "feugiat nisl. In eu mi bibendum neque egestas congue quisque. Pulvinar pellentesque " +
            "habitant morbi tristique senectus et netus et malesuada. Elit scelerisque mauris " +
            "pellentesque pulvinar pellentesque habitant morbi. Vestibulum rhoncus est " +
            "pellentesque elit ullamcorper dignissim cras tincidunt lobortis. At augue eget arcu " +
            "dictum varius duis at consectetur lorem. Ac auctor augue mauris augue neque. Id " +
            "neque aliquam vestibulum morbi blandit cursus. Ut ornare lectus sit amet est placerat."

/**
 * The generateDemoNotes function creates a list of NoteItem instances containing demonstration
 * data for the purpose of testing note-related classes.
 */
fun generateDemoNotes(): MutableList<NoteItem> {
    val demoNotes = mutableListOf<NoteItem>()

    val pathBase = "/ 815842a6-1294-4d1b-9bb4-963e845683d9 "
    val contentSize = loremIpsum.length
    (65..76).forEach {
        val title = "Note ${it.toChar()}"
        val item = NoteItem(
            title, "The contents of note $title:\n\n$loremIpsum",
            RandomID.generate().toString(), "plain"
        ).apply {
            val filename = "${Instant.now().epochSecond}.$contentSize." +
                    UUID.randomUUID().toString().lowercase()
            serverPath = pathBase + filename
        }
        demoNotes.add(item)
    }

    demoNotes[4].apply {
        addLabel("foo")
        addLabel("Trash")
    }
    demoNotes[5].apply {
        addLabel("foo")
        addLabel("bar")
    }
    demoNotes[6].apply {
        addLabel("bar")
        addLabel("baz")
    }


    return demoNotes
}
