package connect

import javafx.util.StringConverter
import keznacl.BadValueException
import libkeycard.MAddress
import libkeycard.WAddress


/** Converts between strings and Mensago  addresses */
class MAddressStringConverter : StringConverter<MAddress>() {
    override fun toString(addr: MAddress?): String =
        addr?.toString() ?: ""

    override fun fromString(s: String?): MAddress =
        MAddress.fromString(s) ?: throw BadValueException("Bad value $s")
}

class WAddressStringConverter : StringConverter<WAddress>() {
    override fun toString(addr: WAddress?): String =
        addr?.toString() ?: ""

    override fun fromString(s: String?): WAddress =
        WAddress.fromString(s) ?: throw BadValueException("Bad value $s")
}
