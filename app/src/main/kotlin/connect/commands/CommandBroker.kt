package connect.commands

import libmensago.ResourceNotFoundException

/**
 * The CommandBroker class is a registry for commands, providing ways to look up and execute
 * application commands.
 */
class CommandBroker {
    private val commands = mutableMapOf<String, Command>()

    fun get(command: String): Command? = commands[command]

    /** Registers a command with the broker */
    fun register(cmd: Command) {
        commands[cmd.name] = cmd
    }

    /**
     * Unregisters a command with the broker
     *
     * @throws ResourceNotFoundException
     */
    fun unregister(cmdName: String) {
        commands.remove(cmdName) ?: throw ResourceNotFoundException("Command $cmdName not found")
    }
}
