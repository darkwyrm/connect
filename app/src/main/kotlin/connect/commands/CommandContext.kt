package connect.commands

/**
 * The CommandContext class provides contextual information to execute a Command instance. This is
 * the top-level class from which specific kinds of contexts will be needed for specific types
 * of Command instances.
 */
class CommandContext(var args: Map<String, CommandArg> = mapOf())
