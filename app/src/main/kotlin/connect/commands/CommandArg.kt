package connect.commands

/**
 * The CommandArg class represents an argument for a Command instance.
 */
class CommandArg(val name: String, val type: CommandArgType, val required: Boolean = true)

/** Enumeration which represents the data types taken by a CommandArg instance */
enum class CommandArgType {
    Int,
    UInt,
    Bool,
    String,
    Object,
}
