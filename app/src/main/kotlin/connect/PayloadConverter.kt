package connect

import connect.contacts.ContactDAO
import connect.contacts.ContactItem
import connect.messages.ConMsgItem
import connect.messages.DevReqMsgItem
import connect.messages.MessageItem
import connect.messages.MsgBaseItem
import connect.notes.NoteItem
import connect.profiles.KeyDAO
import connect.profiles.getActiveProfile
import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.*
import libmensago.*
import libmensago.resolver.KCResolver
import utilities.toLocalDateTime

/**
 * The PayloadConverter class is used to convert JSON payloads to the corresponding domain objects
 * used internally by Connect, such as Message to MessageItem and Note to NoteItem.
 */
class PayloadConverter {

    /**
     * Converts any kind of message to its corresponding message item type, e.g. DeviceRequest ->
     * DevReqMsgItem.
     */
    fun toMsgItem(msg: MessageInterface): Result<MsgBaseItem> {
        return when (msg.javaClass.simpleName) {
            "Message" -> messageToMessageItem(msg as Message)
            "ContactMessage" -> conMsgToConMsgItem(msg as ContactMessage)
            "DeviceRequest" -> devReqToDevReqMsgItem(msg as DeviceRequest)
            else -> BadValueException("Message type '${msg.javaClass.simpleName}' is not supported!")
                .toFailure()
        }
    }

    /**
     * Converts serializable Contact transport classes to something used internally by Connect. This
     * call is used for Payload.Contact, a payload type when syncing contacts between devices.
     */
    fun conToContactItem(con: Contact): Result<ContactItem> {
        return ContactItem.fromContact(con, true)
    }

    /**
     * Converts serializable DeviceRequest transport classes to something used internally by
     * Connect
     */
    fun devReqToDevReqMsgItem(devreq: DeviceRequest): Result<DevReqMsgItem> {
        getActiveProfile().getOrElse { return it.toFailure() }
        val out = convertDevReq(devreq).getOrElse { return it.toFailure() }

        infoForWAddress(devreq.from).getOrElse { return it.toFailure() }.let {
            out.fromName = it.first
            out.fromAddr = it.second
        }
        out.toAddr = infoForWAddress(devreq.to).getOrElse { return it.toFailure() }.first
        return out.toSuccess()
    }

    /**
     * Converts serializable ContactMessage transport classes to something used internally by
     * Connect
     */
    fun conMsgToConMsgItem(cmsg: ContactMessage): Result<ConMsgItem> {
        getActiveProfile().getOrElse { return it.toFailure() }
        val out = convertConMsg(cmsg).getOrElse { return it.toFailure() }

        with(infoForWAddress(cmsg.from).getOrElse { return it.toFailure() }) {
            out.fromName = first
            out.fromAddr = second
        }
        with(infoForWAddress(cmsg.to).getOrElse { return it.toFailure() }) {
            out.toName = first
            out.toAddr = second
        }

        val subjectFrom = out.fromName.ifEmpty { out.fromAddr }
        out.subject = when (cmsg.subType) {
            "conreq.2" -> "Contact Approved for $subjectFrom"
            "conup" -> "Contact Information Update from $subjectFrom"
            else -> "Contact Request from $subjectFrom"
        }
        if (cmsg.subType == "conreq.2") {
            out.contents = "${cmsg.contactInfo.formattedName} has approved your Contact Request! " +
                    "You can now find their information in your address book."
        }
        return out.toSuccess()
    }

    /** Converts serializable Message transport classes to something used internally by Connect */
    fun messageToMessageItem(msg: Message): Result<MessageItem> {
        getActiveProfile().getOrElse { return it.toFailure() }

        val out = MessageItem(msg.subject, msg.body, msg.id.toString(), msg.format.toString())
            .apply {
                subType = msg.subType ?: ""
                date = msg.date.toLocalDateTime()
                threadID = msg.threadID.toString()
                msg.attachments.forEach { attachments[it.name] = it }
            }

        // lookups are needed to convert the WIDs in the message to friendly names used in the
        // MessageItem object
        if (msg.cc.isNotEmpty())
            out.cc = msg.cc.map {
                infoForWAddress(it).getOrElse { e -> return e.toFailure() }
            }.joinToString(", ") { it.first.ifEmpty { it.second } }

        if (msg.bcc.isNotEmpty())
            out.bcc = msg.bcc.map {
                infoForWAddress(it).getOrElse { e -> return e.toFailure() }
            }.joinToString(", ") { it.first.ifEmpty { it.second } }

        with(infoForWAddress(msg.from).getOrElse { return it.toFailure() }) {
            out.fromName = first
            out.fromAddr = second
        }
        with(infoForWAddress(msg.to).getOrElse { return it.toFailure() }) {
            out.toName = first
            out.toAddr = second
        }

        return out.toSuccess()
    }

    /** Converts serializable Note transport classes to something used internally by Connect */
    fun noteToNoteItem(note: Note): Result<NoteItem> {

        return NoteItem(note.title, note.contents, note.format.toString()).apply {
            id = note.id.toString()
        }.toSuccess()
    }

    private fun convertConMsg(cr: ContactMessage): Result<ConMsgItem> {
        val out = ConMsgItem("", cr.body, cr.id.toString(), "plain")
            .apply {
                date = cr.date.toLocalDateTime()
                subType = cr.subType

                threadID = cr.threadID.toString()

                fromName = cr.contactInfo.formattedName
                if (cr.contactInfo.workspace != null && cr.contactInfo.domain != null)
                    fromAddr =
                        WAddress.fromParts(cr.contactInfo.workspace!!, cr.contactInfo.domain!!)
                            .toString()
                toAddr = cr.to.toString()
                contents = cr.body

                contact = cr.contactInfo
                if (contact.custom == null)
                    contact.custom = mutableMapOf()
                contact.custom!!["Managed"] = "true"
            }
        return out.toSuccess()
    }

    private fun convertDevReq(dr: DeviceRequest): Result<DevReqMsgItem> {
        val epair = KeyDAO().loadKeypairByPurpose(KeyPurpose.Encryption)
            .getOrElse { return it.toFailure() }
            .toEncryptionPair()
            ?: return BadValueException("Failed to load encryption keypair").toFailure()

        val info = DeviceInfo.fromCryptoString(epair, dr.devInfo)
            .getOrElse { return it.toFailure() }

        val sb = mutableListOf<String>()

        info.attributes["Device Name"]?.let { sb.add("Device Name: $it") }
        info.attributes["OS"]?.let { sb.add("Device Software: $it") }
        info.attributes["User Name"]?.let { sb.add("User Name: $it") }

        val bodyAttrs = dr.getBodyAttributes()
        // We're going to tack the server's timestamp on inside the device info just in case we need
        // it at some future time. It shouldn't really matter, TBH
        bodyAttrs["Timestamp"]?.let {
            info.attributes["ServerTimestamp"] = it
        }
        bodyAttrs["IP Address"]?.let {
            sb.add("Internet Address: $it")
            info.attributes["IP Address"] = it
        }

        info.attributes["Timestamp"]?.let {
            val tstamp = convertISO8601(it, DateTimeFormats.medLocalDateTime)
                .getOrElse { e -> return e.toFailure() }
            sb.add("When: $tstamp")
        }

        val body = "A new device is asking to log into your account.\n\n" +
                sb.joinToString("\n")

        val out = DevReqMsgItem(info, dr.subject, body, dr.id.toString()).apply {
            date = dr.date.toLocalDateTime()
            subType = dr.subType
            threadID = dr.threadID.toString()

            fromName = "Server for ${dr.from.domain}"
            fromAddr = dr.from.toString()
            toAddr = dr.to.toString()
        }
        return out.toSuccess()
    }

    companion object {
        /**
         * Returns components for address strings: a name and the address. A Mensago address is
         * preferred for usability and memorability reasons, so when possible, it is returned as the
         * second component of the pair. Workspace addresses are returned only when there is no
         * alternative. The name string returned may be empty.
         */
        fun infoForWAddress(waddress: WAddress): Result<Pair<String, String>> {
            if (waddress.id.toString() == "00000000-0000-0000-0000-000000000000") return Pair(
                "${waddress.domain} Server",
                waddress.toString()
            ).toSuccess()

            val profile = getActiveProfile().getOrElse { return it.toFailure() }
            val userWAddress = profile.getWAddress().getOrElse {
                return it.toFailure()
            }
            if (waddress == userWAddress) {
                val addr = profile.getMAddress().getOrElse { return it.toFailure() }
                return Pair("Me", addr.toString()).toSuccess()
            }

            ContactDAO()
                .conItemForWAddress(waddress)
                .getOrElse { return it.toFailure() }
                ?.let { return Pair(it.formattedName, it.maddress).toSuccess() }

            val keycard =
                KCResolver.getKeycard(EntrySubject(waddress.domain, UserID.fromWID(waddress.id)))
                    .getOrElse {
                        if (it is ProtocolException && it.code == 404)
                            return Pair("", waddress.toString()).toSuccess()

                        return it.toFailure()
                    }

            val entry = keycard.current ?: return Pair("", waddress.toString()).toSuccess()
            val domain = entry.getField("Domain")
                ?: return Pair("", waddress.toString()).toSuccess()

            val name = entry.getFieldString("Name") ?: ""

            entry.getFieldString("User-ID")?.let {
                val addr = MAddress.fromString("$it/$domain")
                    ?: return Pair("", waddress.toString()).toSuccess()
                return Pair(name, addr.toString()).toSuccess()
            }

            return Pair("", waddress.toString()).toSuccess()
        }
    }
}
