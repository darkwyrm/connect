package connect.controls

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.HBox

/** A control which horizontally displays a piece of information and a boldfaced label. */
class LabeledValueView(label: String, valueStr: String = "") : HBox() {
    val labelProperty = SimpleStringProperty(label)
    var label: String
        get() = labelProperty.value
        set(value) {
            labelProperty.value = value
        }

    val valueProperty = SimpleStringProperty(valueStr)
    var value: String?
        get() = valueProperty.value
        set(value) {
            valueProperty.value = value
        }

    private val labelLabel = Label().apply {
        textProperty().bind(labelProperty)
        style = "-fx-font-weight: bold;"
    }

    private val valueLabel = Label().apply {
        textProperty().bind(valueProperty)
    }

    init {
        spacing = 10.0
        padding = Insets(5.0)
        children.addAll(labelLabel, valueLabel)
    }
}