package connect.locale

import java.util.*
import java.util.regex.Pattern

private val languageRE = Pattern.compile("""^[a-zA-Z]{2,3}(,[a-zA-Z]{2,3})*?$""").toRegex()

fun makeDisplayLanguageList(langStr: String): String {
    if (!languageRE.matches(langStr)) return ""

    return langStr.split(",").joinToString(", ") {
        Locale.forLanguageTag(it.trim()).displayLanguage
    }
}