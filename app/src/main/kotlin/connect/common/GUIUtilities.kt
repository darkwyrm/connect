package connect.common

import javafx.scene.control.TextInputDialog
import javafx.stage.Modality
import javafx.stage.StageStyle

fun getStringDialog(title: String, header: String): String? {
    val dialog = TextInputDialog().apply {
        initModality(Modality.APPLICATION_MODAL)
        initStyle(StageStyle.UTILITY)
        headerText = header
        this.title = title
    }

    var out: String? = null
    dialog.onShown = javafx.event.EventHandler { dialog.dialogPane.requestFocus() }
    val result = dialog.showAndWait()
    result.ifPresent { out = result.get() }
    return out
}
