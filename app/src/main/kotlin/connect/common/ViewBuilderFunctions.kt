package connect.common

import connect.ChangeHandler
import connect.Filter
import connect.main.AppMode
import connect.main.MainModel
import connect.main.ModeInfo
import javafx.beans.property.SimpleListProperty
import javafx.collections.ListChangeListener
import javafx.geometry.Insets
import javafx.scene.control.ListView
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.scene.layout.VBox.setVgrow

fun buildLabelList(
    filterListProperty: SimpleListProperty<Filter>,
    handler: ChangeHandler<Filter?>
): Region {
    val labelList = ListView(filterListProperty).apply {
        selectionModel.selectedItemProperty().subscribe { oldItem, newItem ->
            handler(oldItem, newItem)
        }
        selectionModel.select(0)
    }
    return VBox().apply {
        padding = Insets(0.0)
        children.addAll(labelList)
        setVgrow(labelList, Priority.ALWAYS)
    }
}

fun modeInfoToMenuItem(item: ModeInfo, mainModel: MainModel): MenuItem {
    return when (item.mode) {
        AppMode.Messages -> {
            MenuItem("_Messages").apply {
                accelerator = item.accelerator
                setOnAction { mainModel.selectedMode = AppMode.Messages }
            }
        }

        AppMode.Contacts -> {
            MenuItem("_Contacts").apply {
                accelerator = item.accelerator
                setOnAction { mainModel.selectedMode = AppMode.Contacts }
            }
        }

        AppMode.Notes -> {
            MenuItem("_Notes").apply {
                accelerator = item.accelerator
                setOnAction { mainModel.selectedMode = AppMode.Notes }
            }
        }
    }
}

fun buildModeMenu(mainModel: MainModel): Menu {
    val modeMenu = Menu("_Activity", null)
    mainModel.modeList.forEach {
        modeMenu.items.add(modeInfoToMenuItem(it, mainModel))
    }
    mainModel.modeList.addListener { c: ListChangeListener.Change<out ModeInfo?> ->
        c.next()
        when {
            c.wasAdded() -> {
                modeMenu.items.add(modeInfoToMenuItem(c.addedSubList[0]!!, mainModel))
            }

            else -> throw UnsupportedOperationException("BUG: Unsupported mode change operation")
        }
    }

    return modeMenu
}
