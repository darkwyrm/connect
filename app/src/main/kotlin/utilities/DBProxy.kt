package utilities

import keznacl.BadValueException
import keznacl.EmptyDataException
import keznacl.toFailure
import keznacl.toSuccess
import libmensago.NotConnectedException
import java.sql.*
import java.util.*

/**
 * The DBProxy class is a frontend to the storage database for the application. It provides an
 * interface to the storage medium which abstracts away the specific type of database and also
 * enables possible optimizations and needs, such as thread safety.

 */
class DBProxy private constructor() {
    private var db: Connection? = null
    private var batch: Statement? = null

    /**
     * Connects to a database using a standard JDBC URL string
     * @param dburl a JDBC url to connect to the database. Currently only SQLite is supported.
     */
    fun connect(dburl: String, args: Properties? = null): Result<DBProxy> {
        if (dburl.isEmpty()) return EmptyDataException().toFailure()

        return try {
            db = if (args != null) DriverManager.getConnection(dburl, args)
            else DriverManager.getConnection(dburl)
            this.toSuccess()
        } catch (e: Exception) {
            e.toFailure()
        }
    }

    /** Disconnects from the database */
    fun disconnect() {
        if (db != null) {
            // Swallow any exceptions about the database already being closed. We really don't
            // care here.
            runCatching { db!!.close() }
        }
        batch = null
        db = null
    }

    fun isConnected(): Boolean {
        return db != null
    }

    /** Executes a query to the database */
    fun query(q: String, vararg args: Any): Result<ResultSet> {
        if (q.isEmpty()) return EmptyDataException().toFailure()
        if (!isConnected()) return NotConnectedException().toFailure()

        val stmt = prepStatement(q, args).getOrElse { return it.toFailure() }
        return try {
            Result.success(stmt.executeQuery())
        } catch (e: Exception) {
            e.toFailure()
        }
    }

    /** Executes a single command on the database */
    fun execute(s: String, vararg args: Any): Throwable? {
        if (s.isEmpty()) return EmptyDataException()
        if (!isConnected()) return NotConnectedException()

        val stmt = prepStatement(s, args).getOrElse { return it }
        return try {
            stmt.execute()
            null
        } catch (e: Exception) {
            e
        }
    }

    /** Adds a command to the internal list of commands to be executed in batch */
    fun add(s: String): Throwable? {
        if (s.isEmpty()) return EmptyDataException()
        if (!isConnected()) return NotConnectedException()

        if (batch == null)
            batch = try {
                db!!.createStatement()
            } catch (e: Exception) {
                return e
            }

        try {
            batch!!.addBatch(s)
        } catch (e: Exception) {
            return e
        }

        return null
    }

    /** Runs all commands in the internal list created with add() */
    fun executeBatch(): Throwable? {
        if (!isConnected()) return NotConnectedException()
        if (batch == null) return NotInitializedException()
        try {
            batch!!.executeBatch()
        } catch (e: Exception) {
            return e
        }
        batch = null

        return null
    }

    /**
     * Checks to see if at least one row is returned for a query. It expects usage of SELECT because
     * a database may not necessarily support COUNT().
     */
    fun exists(q: String, vararg args: Any): Result<Boolean> {
        if (q.isEmpty()) return EmptyDataException().toFailure()
        if (!isConnected()) return NotConnectedException().toFailure()

        val stmt = prepStatement(q, args).getOrElse { return it.toFailure() }
        return try {
            val rs = stmt.executeQuery()
            return Result.success(rs.next())
        } catch (e: Exception) {
            e.toFailure()
        }
    }

    fun prepStatement(s: String, args: Array<out Any>): Result<PreparedStatement> {

        // Make sure the ? count in s matches the number of args
        val qCount = s.split("?").size - 1
        if (qCount != args.size)
            return BadValueException(
                "Parameter count $qCount does not match number of placeholders"
            ).toFailure()

        // This is an internal call for query() and execute(). The null check is done there.
        return try {
            val out = db!!.prepareStatement(s)

            for (i in 0 until qCount) {
                when (args[i]::class.simpleName) {
                    "Boolean" -> out.setBoolean(i + 1, args[i] as Boolean)
                    "ByteArray" -> out.setBytes(i + 1, args[i] as ByteArray)
                    "Byte" -> out.setByte(i + 1, args[i] as Byte)
                    "Double" -> out.setDouble(i + 1, args[i] as Double)
                    "Float" -> out.setFloat(i + 1, args[i] as Float)
                    "Int" -> out.setInt(i + 1, args[i] as Int)
                    "Long" -> out.setLong(i + 1, args[i] as Long)
                    "Short" -> out.setShort(i + 1, args[i] as Short)

                    "NullBinary" -> out.setNull(i + 1, Types.BINARY)
                    "NullBoolean" -> out.setNull(i + 1, Types.BOOLEAN)
                    "NullByte" -> out.setNull(i + 1, Types.TINYINT)
                    "NullDouble" -> out.setNull(i + 1, Types.DOUBLE)
                    "NullFloat" -> out.setNull(i + 1, Types.FLOAT)
                    "NullInt" -> out.setNull(i + 1, Types.INTEGER)
                    "NullLong" -> out.setNull(i + 1, Types.BIGINT)
                    "NullVarBinary" -> out.setNull(i + 1, Types.VARBINARY)
                    else -> out.setString(i + 1, args[i].toString())
                }
            }

            out.toSuccess()
        } catch (e: Exception) {
            e.toFailure()
        }
    }

    companion object {
        @Volatile
        private var instance: DBProxy? = null

        fun get() = instance ?: synchronized(this) {
            instance ?: DBProxy().also { instance = it }
        }
    }
}
