package utilities

import keznacl.toSuccess
import libkeycard.DateTimeFormats
import java.time.LocalDate
import java.time.LocalDateTime

fun stringToLocalDateTime(s: String?): LocalDateTime? {
    if (s == null) return null
    return runCatching { LocalDateTime.parse(s) }.getOrNull()
}

/**
 * Turns a local date/time into a friendly date format string similar to that used by Gmail. For
 * dates during the current year, it displays only the month and day. For other dates, it returns
 * a short date format appropriate to the current locale.
 */
fun friendlyDateFormat(date: LocalDateTime?, currentDate: LocalDate = LocalDate.now())
        : Result<String> {

    return if (date == null) "".toSuccess() else runCatching {
        if (date.year != currentDate.year)
            date.format(DateTimeFormats.monthDayYear)
        else
            date.format(DateTimeFormats.monthDay)
    }
}


