package utilities

import keznacl.Password
import keznacl.getHasherForAlgorithm
import keznacl.getPreferredPasswordAlgorithm
import keznacl.getSupportedPasswordAlgorithms

/**
 * A singleton object to provide an abstraction around password creation. Its primary use is for
 * boosting performance of tests by enabling choice of default password hashing algorithms at
 * runtime.
 */
object PasswordFactory {
    private var defaultAlgorithm = getPreferredPasswordAlgorithm()

    /**
     * The default hashing algorithm instance. Calls to [getHasher] will create copies of this
     * object, so any configuration settings assigned to it will propagate to new ones. All settings
     * are reset to defaults if [setDefaultAlgorithm] is called.
     */
    var defaultHasher = getHasherForAlgorithm(defaultAlgorithm)!!

    /** Returns the available password hashing algorithms. */
    fun getAlgorithms(): List<String> {
        return getSupportedPasswordAlgorithms()
    }

    /**
     * Sets the algorithm used for new password hashing instances.
     */
    fun setDefaultAlgorithm(algorithm: String): Boolean {
        defaultHasher = getHasherForAlgorithm(algorithm) ?: return false
        return true
    }

    /** Returns a password hashing instance based on the settings in the PasswordFactory object */
    fun getHasher(): Password {
        return defaultHasher.copy()
    }
}
