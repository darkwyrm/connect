package connect.notes

import connect.Filter
import connect.initProfile
import connect.setupProfileBase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import utilities.DBProxy

class NoteDAOTest {

    private fun setupDAOTest(testName: String, addDemoData: Boolean = true): NoteDAO {
        val testPath = setupProfileBase(testName)
        initProfile(testPath)

        val dao = NoteDAO()
        dao.initNoteTable()?.let { throw it }
        if (addDemoData)
            generateDemoNotes().forEach { dao.save(it)?.let { e -> throw e } }
        return dao
    }

    @Test
    fun basicTest() {
        val dao = setupDAOTest("notedao_basic", false)
        assertEquals(0, dao.loadHeaders().getOrThrow().size)

        val id = "fcd6fbdd-a632-49e8-9b2c-f5be9986c115"
        val testItem = NoteItem("noteTitle", "noteContents", id)
        dao.save(testItem)?.let { throw it }

        with(DBProxy.get()) {
            val rs = query("SELECT * FROM notes WHERE id=?", id).getOrThrow()
            assert(rs.next())
            assertEquals("noteTitle", rs.getString("title"))
            assertEquals("noteContents", rs.getString("contents"))
            assertEquals(id, rs.getString("id"))
            assertEquals("plain", rs.getString("format"))
        }

        val headers = dao.loadHeaders().getOrThrow()
        assertEquals(1, headers.size)
        assertEquals("noteTitle", headers[0].title)
        assertEquals(id, headers[0].id)
        assertEquals("plain", headers[0].format)

        val loaded = dao.load(id).getOrThrow()!!
        assertEquals("noteTitle", loaded.title)
        assertEquals("noteContents", loaded.contents)
        assertEquals(id, loaded.id)
        assertEquals("plain", loaded.format)
    }

    @Test
    fun filterTest() {
        val dao = setupDAOTest("notedao_filter")

        val filterB = Filter("Note B Contents").apply {
            sortBy("title")
            add("Note B")
        }
        var headers = dao.loadHeaders(filterB).getOrThrow()
        assertEquals(1, headers.size)

        val filterTitle = Filter("Note C").apply {
            sortBy("title")
            add("title:\"Note C\"")
        }
        headers = dao.loadHeaders(filterTitle).getOrThrow()
        assertEquals(1, headers.size)

        val filterLabels1 = Filter("baz").apply {
            sortBy("title")
            add("labels:baz")
        }
        headers = dao.loadHeaders(filterLabels1).getOrThrow()
        assertEquals(1, headers.size)
        assertEquals("Note G", headers[0].title)

        val filterLabels2 = Filter("bar").apply {
            sortBy("title")
            add("labels:bar")
        }
        headers = dao.loadHeaders(filterLabels2).getOrThrow()
        assertEquals(2, headers.size)
        assertEquals("Note F", headers[0].title)
        assertEquals("Note G", headers[1].title)
    }

    @Test
    fun loadLabelsTest() {
        val dao = setupDAOTest("notedao_loadlabels")

        val lf = dao.loadLabelFilters().getOrThrow()
        assertEquals(5, lf.size)
    }

    @Test
    fun emptyTrashTest() {
        val dao = setupDAOTest("notedao_emptytrash")

        val trashFilter = Filter("Trash").apply {
            sortBy("title")
            add("labels:Trash")
        }
        assertEquals(1, dao.loadHeaders(trashFilter).getOrThrow().size)
        dao.emptyTrash()
        assertEquals(0, dao.loadHeaders(trashFilter).getOrThrow().size)
    }

    @Test
    fun renameTest() {
        val dao = setupDAOTest("notedao_rename")
        val headers = dao.loadHeaders(null).getOrThrow()
        assertEquals("Note A", headers[0].title)
        dao.rename(headers[0], "Renamed Note A")?.let { throw it }
        val renamed = dao.load(headers[0].id).getOrThrow()!!
        assertEquals("Renamed Note A", renamed.title)
    }

    @Test
    fun labelChangeTest() {
        val dao = setupDAOTest("notedao_labelchange")
        val headers = dao.loadHeaders(
            Filter.fromStrings("Baz", "labels:baz")!!.sortBy("title")
        ).getOrThrow()
        assertEquals("Note G", headers[0].title)
        assert(headers[0].getLabels().contains("baz"))
        assertFalse(headers[0].getLabels().contains("spamEggs"))
    }
}
