package connect.notes

import connect.initProfile
import connect.setupProfileBase
import libmensago.MServerPath
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

class NoteInteractorTest {

    @Test
    fun moveNoteTest() {
        val testPath = setupProfileBase("noteint_move")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply { selectLabel(null) }
        nint.addDemoData()
        assert(model.noteListProperty.size > 0)
        nint.selectNote(model.noteList[0])

        val dao = NoteDAO()
        val item = dao.load(model.noteList[0].id).getOrThrow()!!
        val oldPath = MServerPath(item.serverPath)
        val newPath = MServerPath("/ 042152cc-0fba-4ed7-a2d7-0ba58127e62a")
        nint.moveNote(oldPath, newPath)
        val newNotePath = newPath.clone().push(oldPath.basename()).getOrThrow()
        assertEquals(newNotePath.toString(), model.selectedNote?.serverPath.toString())
    }

    @Test
    fun createNoteTest() {
        // This also exercises the addNote() function

        val testPath = setupProfileBase("noteint_create")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply { selectLabel(null) }
        nint.addDemoData()
        val demoSize = model.noteListProperty.size
        assert(demoSize > 0)

        nint.createNote("New")
        assertEquals(demoSize + 1, model.noteListProperty.size)
        assertEquals("New", model.noteListProperty.first().title)
    }

    @Test
    fun selectNoteTest() {
        val testPath = setupProfileBase("noteint_selectnote")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply {
            selectLabel(null)
            addDemoData()
        }
        assert(model.noteListProperty.isNotEmpty())

        val first = model.noteListProperty.first()
        nint.selectNote(first)?.let { throw it }
        assertEquals(first.id, model.selectedNoteID)
        assert(model.selectedNoteContents.isNotEmpty())
        assertNotNull(model.selectedNoteProperty.get())
        assertEquals(first.title, model.selectedNoteProperty.get()!!.title)
    }

    @Test
    fun loadLabelsTest() {
        val testPath = setupProfileBase("noteint_loadlabels")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply { selectLabel(null) }

        // At this point, we should have no data and no labels
        assert(model.noteListProperty.isEmpty())
        nint.loadLabels()
        assertEquals(2, model.labelListProperty.size)
        assertEquals("All Notes", model.labelListProperty[0]!!.name)
        assertEquals("Trash", model.labelListProperty[1]!!.name)

        nint.addDemoData()
        assert(model.noteListProperty.isNotEmpty())
        nint.loadLabels()
        assertEquals(5, model.labelListProperty.size)
        assertEquals("bar", model.labelListProperty[2]!!.name)
        assertEquals("baz", model.labelListProperty[3]!!.name)
        assertEquals("foo", model.labelListProperty[4]!!.name)
    }

    @Test
    fun selectLabelTest() {
        val testPath = setupProfileBase("noteint_selectlabel")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply {
            addDemoData()
            loadLabels()
            selectLabel(null)
        }

        val demoData = generateDemoNotes()
        assertEquals(demoData.size, model.noteListProperty.size)
        listOf("bar", "baz", "foo").forEach { label ->
            val expectedCount = demoData.filter {
                it.getLabels().contains(label) && !it.getLabels().contains("Trash")
            }.size
            val labelFilter = model.labelListProperty.find { it.name == label }!!
            nint.selectLabel(labelFilter)
            val actualCount = model.noteList.size
            if (actualCount != expectedCount) fail(
                "Label count mismatch for label $label. Expected $expectedCount, got $actualCount"
            )
        }
    }

    @Test
    fun emptyTrashTest() {
        val testPath = setupProfileBase("noteint_emptytrash")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply {
            addDemoData()
            loadLabels()
            // Select the Trash label
            selectLabel(model.labelListProperty[1]!!)
        }
        assert(model.noteListProperty.isNotEmpty())
        nint.emptyTrash()
        assert(model.noteListProperty.isEmpty())
    }

    @Test
    fun trashNoteTest() {
        val testPath = setupProfileBase("noteint_trashnote")
        initProfile(testPath)
        val model = NoteModel()
        val nint = NoteInteractor(model).apply {
            addDemoData()
            emptyTrash()
            loadLabels()
            // Select the Trash label
            selectLabel(model.labelListProperty[1]!!)
        }
        assert(model.noteListProperty.isEmpty())

        // Select All Items
        nint.selectLabel(model.labelListProperty[0]!!)

        val count = model.noteList.size
        nint.selectNote(model.noteList[1]!!)
        nint.trashSelectedNote()
        assertEquals(count - 1, model.noteList.size)
    }
}
