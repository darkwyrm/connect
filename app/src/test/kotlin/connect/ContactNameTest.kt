package connect

import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

class ContactNameTest {

    @Test
    fun givenFamilyNameOrder() {

        parseGivenFamilyNameOrder("Richard").apply {
            assertEquals("Richard", formattedName)
            assertEquals("Richard", givenName)
        }

        parseGivenFamilyNameOrder("Richard Brannan").apply {
            assertEquals("Richard Brannan", formattedName)
            assertEquals("Richard", givenName)
            assertEquals("Brannan", familyName)
        }

        parseGivenFamilyNameOrder("Richard Brannan, MD, PhD").apply {
            assertEquals("Richard Brannan, MD, PhD", formattedName)
            assertEquals("Richard", givenName)
            assertEquals("Brannan", familyName)
            assertEquals("MD, PhD", suffix)
        }

        parseGivenFamilyNameOrder("Dr. Richard Brannan, MD, PhD").apply {
            assertEquals("Dr. Richard Brannan, MD, PhD", formattedName)
            assertEquals("Dr.", prefix)
            assertEquals("Richard", givenName)
            assertEquals("Brannan", familyName)
            assertEquals("MD, PhD", suffix)
        }

        parseGivenFamilyNameOrder("Billy Bob Smith").apply {
            assertEquals("Billy Bob Smith", formattedName)
            assertEquals("Billy", givenName)
            assertEquals("Bob Smith", familyName)
        }

        parseGivenFamilyNameOrder("Billy_Bob Smith").apply {
            assertEquals("Billy Bob Smith", formattedName)
            assertEquals("Billy Bob", givenName)
            assertEquals("Smith", familyName)
        }

        parseGivenFamilyNameOrder("Mr. Billy_Bob Smith, CISSP").apply {
            assertEquals("Mr. Billy Bob Smith, CISSP", formattedName)
            assertEquals("Mr.", prefix)
            assertEquals("Billy Bob", givenName)
            assertEquals("Smith", familyName)
            assertEquals("CISSP", suffix)
        }

    }
}
