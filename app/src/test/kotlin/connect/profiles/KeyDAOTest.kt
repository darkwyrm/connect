package connect.profiles

import connect.KeyPurpose
import connect.contacts.ContactDAO
import connect.contacts.generateDemoContacts
import connect.initProfile
import connect.setupProfileBase
import keznacl.*
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.ResourceNotFoundException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import utilities.DBProxy
import java.nio.file.Paths

class KeyDAOTest {
    private val testEncPair1 = EncryptionPair.fromStrings(
        "CURVE25519:Umbw0Y<^cf1DN|>X38HCZO@Je(zSe6crC6X_C_0F",
        "CURVE25519:Bw`F@ITv#sE)2NnngXWm7RQkxg{TYhZQbebcF5b$",
    ).getOrThrow()

    private val testSignPair1 = SigningPair.fromStrings(
        "ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p",
        "ED25519:p;XXU0XF#UO^}vKbC-wS(#5W6=OEIFmR2z`rS1j+",
    ).getOrThrow()

    private val conid = RandomID.fromString("b80de689-febf-4c43-9ff6-3f7d3251638a")!!

    private fun setupDAOTest(testName: String, addDemoData: Boolean = true) {
        val testPath = setupProfileBase(testName)
        initProfile(testPath)

        with(ContactDAO()) {
            initContactTables()

            if (!addDemoData) return
            generateDemoContacts(false).forEach {
                saveConItem(it)?.let { e -> throw e }
            }
        }
    }


    @Test
    fun archiveKeyTest() {
        val testPath = setupProfileBase("keydao_archive")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val secretKey = SecretKey.generate().getOrThrow()
        val secretHash = kdao.saveSecretKey(waddr, secretKey, KeyPurpose.Folder).getOrThrow()
        val encPair = EncryptionPair.generate().getOrThrow()
        val encHash = kdao.saveKeypair(waddr, encPair, KeyPurpose.Encryption).getOrThrow()

        kdao.loadKey(secretHash).getOrThrow()
        kdao.loadKeypair(encHash).getOrThrow()
        assertEquals("active", kdao.loadKeyStatus(encHash).getOrThrow())
        assertEquals("active", kdao.loadKeyStatus(secretHash).getOrThrow())

        assertNull(kdao.archiveKey(secretHash))
        assertNull(kdao.archiveKeypair(encHash))

        kdao.loadKeypair(encHash).getOrThrow()
        kdao.loadKey(secretHash).getOrThrow()
        assert(kdao.loadKeypairByPurpose(KeyPurpose.Encryption).isFailure)
        assert(kdao.loadKeyByPurpose(KeyPurpose.Folder).isFailure)
        assertEquals("archived", kdao.loadKeyStatus(encHash).getOrThrow())
        assertEquals("archived", kdao.loadKeyStatus(secretHash).getOrThrow())
    }

    @Test
    fun basicConKeyTest() {
        setupDAOTest("keydao_basicconkey")

        val kdao = KeyDAO()
        kdao.initKeyTables()
        kdao.saveKeypair(conid, testEncPair1, KeyPurpose.RelMyEncryption).getOrThrow()

        val encCS = kdao.loadKeyString(conid, KeyPurpose.RelMyEncryption, true).getOrThrow()
        assertEquals(testEncPair1.pubKey, encCS)

        val epair = kdao.loadRelEncryptionPair(conid).getOrThrow()
        assertEquals(testEncPair1.pubKey, epair.pubKey)
        assertEquals(testEncPair1.privKey, epair.privKey)

        kdao.saveKeypair(conid, testSignPair1, KeyPurpose.RelMySigning).getOrThrow()

        val spair = kdao.loadRelSigningPair(conid).getOrThrow()
        assertEquals(testSignPair1.pubKey, spair.pubKey)
        assertEquals(testSignPair1.privKey, spair.privKey)
    }

    @Test
    fun keysByStatusTest() {
        val testPath = setupProfileBase("keydao_keybystatus")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val encPair = EncryptionPair.generate().getOrThrow()
        kdao.saveKeypair(waddr, encPair, KeyPurpose.Encryption).getOrThrow()
        val signPair = SigningPair.generate().getOrThrow()
        kdao.saveKeypair(waddr, signPair, KeyPurpose.Signing).getOrThrow()

        val secretKey = SecretKey.generate().getOrThrow()
        kdao.saveSecretKey(waddr, secretKey, KeyPurpose.Folder).getOrThrow()
        kdao.archiveKey(secretKey.getHash().getOrThrow())

        val badval = kdao.loadKeysByStatus("foobar")
        assert(badval.isFailure)
        assert(
            badval.exceptionOrNull() is BadValueException
        )
        val activeKeys = kdao.loadKeysByStatus("active").getOrThrow()
        assertEquals(2, activeKeys.size)

        assertNotNull(activeKeys.find { it.pubKey.toString() == encPair.pubKey.toString() })
        assertNotNull(activeKeys.find { it.pubKey.toString() == signPair.pubKey.toString() })
        assertNull(activeKeys.find { it.pubKey.toString() == secretKey.key.toString() })

        val archivedKeys = kdao.loadKeysByStatus("archived").getOrThrow()
        assertEquals(1, archivedKeys.size)
        assertEquals(secretKey.key.toString(), archivedKeys[0].pubKey.toString())
    }

    @Test
    fun keyPairTest() {
        val testPath = setupProfileBase("keydao_keypairtest")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        val db = DBProxy.get()
        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val encPair = EncryptionPair.generate().getOrThrow()
        val signPair = SigningPair.generate().getOrThrow()

        // Key not found cases

        val result = kdao.loadKeypair(encPair.getPublicHash().getOrThrow())
        assert(result.isFailure)
        assert(result.exceptionOrNull() is ResourceNotFoundException)

        val encResult = kdao.loadEncryptionPair(encPair.getPublicHash().getOrThrow())
        assert(encResult.isFailure)
        assert(encResult.exceptionOrNull() is ResourceNotFoundException)

        val signResult = kdao.loadSigningPair(signPair.getPublicHash().getOrThrow())
        assert(signResult.isFailure)
        assert(signResult.exceptionOrNull() is ResourceNotFoundException)

        // Adding keys

        kdao.saveKeypair(waddr, encPair, KeyPurpose.Encryption).getOrThrow()
        var rs = db.query("""SELECT * FROM keys WHERE purpose='encryption'""").getOrThrow()
        assert(rs.next())
        assertEquals("asymmetric", rs.getString("type"))
        assertEquals("active", rs.getString("status"))
        assertEquals(encPair.pubKey.toString(), rs.getString("public"))
        assertEquals(encPair.privKey.toString(), rs.getString("private"))

        kdao.saveKeypair(waddr, signPair, KeyPurpose.Signing).getOrThrow()

        rs = db.query("""SELECT * FROM keys WHERE purpose='signing'""").getOrThrow()
        assert(rs.next())
        assertEquals("signing", rs.getString("type"))
        assertEquals("active", rs.getString("status"))
        assertEquals(signPair.pubKey.toString(), rs.getString("public"))
        assertEquals(signPair.privKey.toString(), rs.getString("private"))

        // Actual success cases getting keys

        assertEquals(
            encPair.pubKey.toString(),
            kdao.loadKeypair(encPair.getPublicHash().getOrThrow()).getOrThrow().first.toString()
        )

        assertEquals(
            encPair.pubKey.toString(),
            kdao.loadEncryptionPair(encPair.getPublicHash().getOrThrow()).getOrThrow().pubKey
                .toString()
        )

        assertEquals(
            signPair.pubKey.toString(),
            kdao.loadSigningPair(signPair.getPublicHash().getOrThrow()).getOrThrow().pubKey
                .toString()
        )
    }

    @Test
    fun keyPairPurposeTest() {
        val testPath = setupProfileBase("keydao_keypairpurpose")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        // Just basic adding and getting keypairs test cases

        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val encPair = EncryptionPair.generate().getOrThrow()
        val signPair = SigningPair.generate().getOrThrow()

        // Key not found cases

        var result = kdao.loadKeyByPurpose(KeyPurpose.Signing)
        assert(result.isFailure)
        assert(result.exceptionOrNull() is ResourceNotFoundException)

        val encResult = kdao.loadRelEncryptionPair(waddr, KeyPurpose.Encryption)
        assert(encResult.isFailure)
        assert(encResult.exceptionOrNull() is ResourceNotFoundException)

        val signResult = kdao.loadSigningPair(waddr, KeyPurpose.Signing)
        assert(signResult.isFailure)
        assert(signResult.exceptionOrNull() is ResourceNotFoundException)

        kdao.saveKeypair(waddr, encPair, KeyPurpose.Encryption).getOrThrow()
        kdao.saveKeypair(waddr, signPair, KeyPurpose.Signing).getOrThrow()

        // Actual success cases getting/archiving keys

        assertEquals(
            encPair.pubKey.toString(),
            kdao.loadKeypairByPurpose(KeyPurpose.Encryption).getOrThrow().first.toString()
        )
        assertEquals(
            encPair.pubKey.toString(),
            kdao.loadRelEncryptionPair(waddr, KeyPurpose.Encryption).getOrThrow().pubKey.toString()
        )
        assertEquals(
            signPair.pubKey.toString(),
            kdao.loadSigningPair(waddr, KeyPurpose.Signing).getOrThrow().pubKey.toString()
        )

        assertNull(kdao.deleteKeypair(signPair.getPublicHash().getOrThrow()))
        result = kdao.loadKeyByPurpose(KeyPurpose.Signing)
        assert(result.isFailure)
        assert(result.exceptionOrNull() is ResourceNotFoundException)


    }

    @Test
    fun loadCVKeyTest() {
        setupDAOTest("keydao_loadcvkey")
        // This handles the last method not tested in the other tests: loadContactVerificationKey

        val vkey =
            VerificationKey.fromString("ED25519:d0-oQb;{QxwnO{=!|^62+E=UYk2Y3mr2?XKScF4D")
                .getOrThrow()
        val kdao = KeyDAO()
        kdao.saveKey(conid, KeyPurpose.RelSigning, vkey.key, true)
            ?.let { throw it }

        val loaded = kdao.loadContactVerificationKey(conid).getOrThrow()
        assertEquals(vkey.key, loaded.key)
    }

    @Test
    fun loadKeyListTest() {
        setupDAOTest("keydao_loadkeylist")

        val kdao = KeyDAO()
        kdao.initKeyTables()

        val infoPair = Workspace.generate(
            null, Domain.fromString("example.com")!!, RandomID.generate(),
            SHAPassword().updateHash("foobar").getOrThrow()
        ).getOrThrow()

        ProfileBroker(ProfileManager.model)
            .setKeyData(infoPair.first.waddress, infoPair.second)?.let { throw it }

        val keyList = kdao.loadKeyList().getOrThrow()
        assertEquals(6, keyList.size)
    }

    @Test
    fun replaceTest() {
        setupDAOTest("keydao_replace")

        val newKey =
            EncryptionKey.fromString("CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{")
                .getOrThrow()

        val kdao = KeyDAO()
        kdao.saveKey(conid, KeyPurpose.RelEncryption, testEncPair1.pubKey, true)
            ?.let { throw it }

        kdao.replaceKey(conid, KeyPurpose.RelEncryption, newKey.key, true)
            ?.let { throw it }

        val oldCS = kdao.loadKeyString(conid, KeyPurpose.RelEncryption, true, "archived")
            .getOrThrow()
        assertEquals(testEncPair1.pubKey, oldCS)

        val newLoaded = kdao.loadContactEncryptionKey(conid).getOrThrow()
        assertEquals(newKey.key, newLoaded.key)
    }

    @Test
    fun revokeTest() {
        val testPath = setupProfileBase("keydao_revoke")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val encPair = EncryptionPair.generate().getOrThrow()
        val encHash = kdao.saveKeypair(waddr, encPair, KeyPurpose.Encryption).getOrThrow()
        val secretKey = SecretKey.generate().getOrThrow()
        val secretHash = kdao.saveSecretKey(waddr, secretKey, KeyPurpose.Folder).getOrThrow()
        assertEquals("active", kdao.loadKeyStatus(encHash).getOrThrow())
        assertEquals("active", kdao.loadKeyStatus(secretHash).getOrThrow())

        kdao.loadKeypair(encHash).getOrThrow()
        kdao.loadKey(secretHash).getOrThrow()

        assertNull(kdao.revokeKeypair(encHash))
        assertNull(kdao.revokeKey(secretHash))

        kdao.loadKeypair(encHash).getOrThrow()
        kdao.loadKey(secretHash).getOrThrow()
        assert(kdao.loadKeypairByPurpose(KeyPurpose.Encryption).isFailure)
        assert(kdao.loadKeyByPurpose(KeyPurpose.Folder).isFailure)
        assertEquals("revoked", kdao.loadKeyStatus(encHash).getOrThrow())
        assertEquals("revoked", kdao.loadKeyStatus(secretHash).getOrThrow())
    }

    @Test
    fun secretKeyTest() {
        val testPath = setupProfileBase("keydao_secretKeyTest")
        initProfile(testPath)

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            scanProfiles()?.let { throw it }
            activateProfile(model.defaultProfile)?.let { throw it }
            assertNotNull(model.activeProfile)
        }

        val kdao = KeyDAO()
        val waddr = WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!

        val secretKey = SecretKey.generate().getOrThrow()

        var result = kdao.loadKey(secretKey.getHash().getOrThrow())
        assert(result.isFailure)
        assert(result.exceptionOrNull() is ResourceNotFoundException)

        val hash = kdao.saveSecretKey(waddr, secretKey, KeyPurpose.Folder).getOrThrow()

        assertEquals(secretKey.key.toString(), kdao.loadKey(hash).getOrThrow().toString())
        val pair = kdao.loadKeyByPurpose(KeyPurpose.Folder).getOrThrow()
        assertEquals(secretKey.key.toString(), pair.first.toString())
        assertEquals(hash.toString(), pair.second.toString())

        assertNull(kdao.deleteKey(secretKey.getHash().getOrThrow()))
        result = kdao.loadKey(secretKey.getHash().getOrThrow())
        assert(result.isFailure)
        assert(result.exceptionOrNull() is ResourceNotFoundException)
    }
}