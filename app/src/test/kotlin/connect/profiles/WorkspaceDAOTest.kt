package connect.profiles

import connect.initProfile
import connect.setupProfileBase
import keznacl.EncryptionPair
import keznacl.SHAPassword
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libkeycard.WAddress
import libmensago.WorkspaceStatus
import org.junit.jupiter.api.Test
import utilities.DBProxy
import kotlin.test.assertEquals

class WorkspaceDAOTest {
    private val testWID = RandomID.fromString("aea3a74c-876f-4280-8a36-ab81c0bab80a")!!
    private val testDom = Domain.fromString("example.com")!!
    private val testUID = UserID.fromString("csimons")!!
    private val testDevID = RandomID.fromString("7d60828d-a75c-43cd-9276-c9061fcc037a")!!
    private val testDevPair = EncryptionPair.generate().getOrThrow()
    private val testPw = SHAPassword().apply {
        setIterations(1)
        updateHash("MyS3cretPassw*rd").getOrThrow()
    }
    private val testWAddress = WAddress.fromParts(testWID, testDom)

    @Test
    fun archiveTest() {
        val testPath = setupProfileBase("workspacedao_archive")
        initProfile(testPath)

        val db = DBProxy.get()
        val dao = WorkspaceDAO()
        val w =
            Workspace(testWID, testDom, testUID, testDevID, testDevPair, WorkspaceStatus.Active)
        dao.save(w, testPw)?.let { throw it }
        dao.archive(testWAddress)?.let { throw it }

        val rs = db.query("SELECT wid FROM workspaces WHERE status='archived'").getOrThrow()
        assert(rs.next())
        assertEquals(testWID.toString(), rs.getString("wid"))
    }

    @Test
    fun loadTest() {
        val testPath = setupProfileBase("workspacedao_load")
        initProfile(testPath)

        val dao = WorkspaceDAO()
        val w =
            Workspace(testWID, testDom, testUID, testDevID, testDevPair, WorkspaceStatus.Active)
        dao.save(w, testPw)?.let { throw it }

        val loaded = dao.load(testWID).getOrThrow()
        assertEquals(testWID, loaded.wid)
        assertEquals(testUID, loaded.uid)
        assertEquals(testDom, loaded.domain)
        assertEquals("active", loaded.status.toString())
        assertEquals(testDevID, loaded.devid)
        assertEquals(testDevPair.pubKey, loaded.devpair.pubKey)
        assertEquals(testDevPair.privKey, loaded.devpair.privKey)

    }

    @Test
    fun saveTest() {
        val testPath = setupProfileBase("workspacedao_save")
        initProfile(testPath)

        val db = DBProxy.get()
        val dao = WorkspaceDAO()
        val w =
            Workspace(testWID, testDom, testUID, testDevID, testDevPair, WorkspaceStatus.Active)
        dao.save(w, testPw)?.let { throw it }

        val rs = db.query("SELECT * FROM workspaces WHERE wid=? AND domain=?", testWID, testDom)
            .getOrThrow()
        assert(rs.next())
        assertEquals(testWID.toString(), rs.getString("wid"))
        assertEquals(testUID.toString(), rs.getString("userid"))
        assertEquals(testDom.toString(), rs.getString("domain"))
        assertEquals(testPw.toString(), rs.getString("password"))
        assertEquals("SHA3-256", rs.getString("pwhashtype"))
        assertEquals("identity", rs.getString("type"))
        assertEquals("active", rs.getString("status"))
        assertEquals(testDevID.toString(), rs.getString("devid"))
        assertEquals(testDevPair.pubKey.toString(), rs.getString("public_key"))
        assertEquals(testDevPair.privKey.toString(), rs.getString("private_key"))

    }

    @Test
    fun setUserID() {
        val testPath = setupProfileBase("workspacedao_save")
        initProfile(testPath)

        val db = DBProxy.get()
        val dao = WorkspaceDAO()
        val w =
            Workspace(testWID, testDom, testUID, testDevID, testDevPair, WorkspaceStatus.Active)
        dao.save(w, testPw)?.let { throw it }
        dao.setUserID(null, testWID, testDom)?.let { throw it }

        val rs = db.query("SELECT * FROM workspaces WHERE wid=?", testWID).getOrThrow()
        assert(rs.next())
        assertEquals("", rs.getString("userid"))
    }
}