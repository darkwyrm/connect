package connect.profiles

import connect.initProfile
import connect.setupProfileBase
import libmensago.ResourceNotFoundException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import utilities.AppSettings
import utilities.DBProxy
import java.nio.file.Paths
import kotlin.test.assertEquals

class ProfileDAOTest {

    @Test
    fun loadTest() {
        val testPath = setupProfileBase("profiledao_load")
        initProfile(testPath)

        val dao = ProfileDAO()
        dao.resetDB(Paths.get(testPath, "primary"))?.let { throw it }
        val p = dao.load("primary").getOrThrow()

        assertEquals("primary", p.name)
        assertEquals(p.wid, LOCAL_WID)
        assertEquals(p.domain, LOCAL_DOMAIN)
        assertNull(p.uid)
        assertNotNull(p.settings)
    }

    @Test
    fun resetDBTest() {
        val testPath = setupProfileBase("profiledao_reset")
        initProfile(testPath)

        ProfileDAO().resetDB(Paths.get(testPath, "primary"))?.let { throw it }

        val w = WorkspaceDAO().load(LOCAL_WID).getOrThrow()
        assertEquals(w.wid, LOCAL_WID)
        assertEquals(w.domain, LOCAL_DOMAIN)
        assertNull(w.uid)

        // We could assert a huge number of other things, but this covers the basics and should be
        // sufficient.
    }

    @Test
    fun settingsAPITest() {
        val testPath = setupProfileBase("profiledao_settingsapi")
        initProfile(testPath)

        val dao = ProfileDAO()
        dao.resetDB(Paths.get(testPath, "primary"))?.let { throw it }

        val settings = AppSettings("application/x-vnd.mensago-Connect")
        assertNull(settings.setField("test", "localTestValue", false))
        assertNull(settings.setField("theme.windows", "darcula", true))
        assertNull(settings.setField("theme.linux", "obsidian", true))
        assert(settings.isModified())
        assert(settings.hasField("test"))
        dao.saveSettings(settings)?.let { throw it }

        val rs = DBProxy.get().query("SELECT * FROM appconfig WHERE name='theme.windows'")
            .getOrThrow()
        assert(rs.next())
        Assertions.assertEquals("darcula", rs.getString("itemvalue"))
        Assertions.assertEquals(true, rs.getBoolean("sync"))


        val settings2 = dao.loadSettings().getOrThrow()
        Assertions.assertEquals("obsidian", settings2.getField("theme.linux")!!.first)

        settings2.setExistingField("another.setting", "foo", false)?.let { throw it }
        assert(!settings2.hasField("another.setting"))
        assert(
            settings2.setValue("another.setting", "exists")
                    is ResourceNotFoundException
        )

        settings2.setField("another.setting", "foo", false)?.let { throw it }
        assert(settings2.hasField("another.setting"))
        settings2.deleteField("another.setting")
        assert(!settings2.hasField("another.setting"))
    }
}

