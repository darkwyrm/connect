package connect

import connect.profiles.ProfileManager
import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.file.Paths

/** Returns the canonical version of the path specified. */
fun getPathForTest(testName: String): String? {
    return runCatching {
        Paths.get("build", "testfiles", testName).toAbsolutePath().toString()
    }.getOrElse { null }
}

/**
 * Creates a new profile folder hierarchy on the client side in the specified test folder and
 * returns the path to it. This sets up a bare minimum environment upon which any test can build.
 * No initialization of any kind is performed except setting up the folder hierarchy and ensuring
 * that it is empty.
 */
fun setupProfileBase(name: String): String {
    val dir = File(getPathForTest(name)!!)
    if (dir.exists())
        FileUtils.cleanDirectory(dir)
    else
        dir.mkdirs()

    return dir.toString()
}

/**
 * Adds to an empty profile folder the bare minimum of setup needed to operate Connect -- one which
 * is ready to add data to.
 */
fun initProfile(profileFolder: String) {
    with(ProfileManager) {
        setLocation(Paths.get(profileFolder))?.let { throw it }
        ensureDefaultProfile()
        scanProfiles()?.let { throw it }
        if (model.defaultProfile.isNotEmpty())
            activateProfile(model.defaultProfile)
    }
}
