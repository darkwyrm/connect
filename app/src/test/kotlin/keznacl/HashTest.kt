package keznacl

import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Paths
import kotlin.test.assertEquals

class HashTest {

    @Test
    fun testHashSupport() {
        assert(isSupportedAlgorithm("BLAKE2B-256"))
        assert(isSupportedHash("BLAKE2B-256"))
        assert(isSupportedHash("SHA-256"))
        assert(isSupportedHash("SHA3-256"))

        val hashSupport = getSupportedHashAlgorithms()
        assertEquals(3, hashSupport.size)
        assertEquals(CryptoType.BLAKE2B_256, hashSupport[0])
        assertEquals(CryptoType.SHA_256, hashSupport[1])
        assertEquals(CryptoType.SHA3_256, hashSupport[2])

        assertEquals(CryptoType.BLAKE2B_256, getPreferredHashAlgorithm())
    }

    @Test
    fun testBlake2B() {
        val expectedBlake =
            Hash.fromString("BLAKE2B-256:?*e?y<{rF)B`7<5U8?bXQhNic6W4lmGlN}~Mu}la")!!
        assertEquals(expectedBlake.value, blake2Hash("aaaaaaaa".toByteArray()).getOrThrow().value)

        assertEquals(
            expectedBlake.value, hash("aaaaaaaa".toByteArray(), CryptoType.BLAKE2B_256)
                .getOrThrow().value
        )
    }

    @Test
    fun testSHA256() {
        val expectedSHA =
            Hash.fromString("SHA-256:A3Wp)6`}|qqweQl!=L|-R>C51(W!W+B%4_+&b=VC")!!
        assertEquals(expectedSHA.value, sha256Hash("aaaaaaaa".toByteArray()).getOrThrow().value)

        assertEquals(
            expectedSHA.value, hash("aaaaaaaa".toByteArray(), CryptoType.SHA_256)
                .getOrThrow().value
        )
    }

    @Test
    fun testSHA3256() {
        val expectedSHA =
            Hash.fromString("SHA3-256:U-IV^8<B&Xw`!!lOHe+qQ2O>NX4mqh?@`(-Eh0hn")!!
        assertEquals(expectedSHA.value, sha3256Hash("aaaaaaaa".toByteArray()).getOrThrow().value)

        assertEquals(
            expectedSHA.value, hash("aaaaaaaa".toByteArray(), CryptoType.SHA3_256)
                .getOrThrow().value
        )
    }

    @Test
    fun testCheck() {
        val key = SecretKey.fromString("XSALSA20:Z%_Is*V6uc!_+QIG5F`UJ*cLYoO`=58RCuAk-`Bq")
            .getOrThrow()
        // The call to toHash() is for lint removal, not necessity. :)
        val hash = key.getHash().getOrThrow().toHash()!!
        assert(hash.check(key.key.toByteArray()).getOrThrow())
    }

    @Test
    fun testConversion() {
        val hash = blake2Hash("aaaaaaaa".toByteArray()).getOrThrow()
        val expectedB85 = "?*e?y<{rF)B`7<5U8?bXQhNic6W4lmGlN}~Mu}la"
        assertEquals(expectedB85, hash.encodedData)
        val expectedHex = "ef027ccde61ebc8225283b7c5daaf17c527b03aa13d77a7533835d9546896148"
        assertEquals(expectedHex, hash.toHex().getOrThrow())
        val expectedB64 = "7wJ8zeYevIIlKDt8XarxfFJ7A6oT13p1M4NdlUaJYUg="
        assertEquals(expectedB64, hash.toBase64().getOrThrow())
    }

    @Test
    fun testHashFile() {
        val testPath = Paths.get("build", "testfiles", "testHashFile")
            .toAbsolutePath()
            .toString()

        val testDir = File(testPath)
        if (!testDir.exists())
            testDir.mkdirs()

        val testdata = "0".repeat(10_000).encodeToByteArray()

        val testFile = Paths.get(testDir.path, "fileToHash.txt").toFile()
        testFile.createNewFile()
        val ostream = testFile.outputStream()
        ostream.write(testdata)
        ostream.close()

        getSupportedHashAlgorithms().forEach { algorithm ->
            val startHash = hash(testdata, algorithm).getOrThrow()
            val fileHash = hashFile(testFile.path, algorithm).getOrThrow()
            assertEquals(startHash, fileHash)
        }
    }
}
