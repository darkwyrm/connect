package keznacl

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class SHAPasswordTest {

    @Test
    fun basicTests() {
        val hasher = getHasherForAlgorithm("SHA3-256") as SHAPassword
        hasher.setIterations(2)
        hasher.updateHash("MyS3cretPassw*rd").getOrThrow()
        assert(hasher.verify("MyS3cretPassw*rd"))
        assertFalse(hasher.verify("foobar"))
    }

    @Test
    fun passwordSync() {
        val firstHasher = SHAPassword()
        firstHasher.setIterations(1)
        firstHasher.updateHash("MyS3cretPassw*rd")

        val secondHasher = SHAPassword()
        secondHasher.setIterations(1)
        secondHasher.setSalt(firstHasher.getSalt())
        secondHasher.updateHash("MyS3cretPassw*rd")
        assertEquals(firstHasher.getHash(), secondHasher.getHash())
    }

    @Test
    fun setFromHash() {
        val testHash = "\$sha3-256\$t=50000\$G/8i8sbKhDu6+3e6BfPs8U75RsAiis6Te/MoC85+SfM=" +
                "\$p//G+L8e12ZRwUdWoGHWYvWA/03kO0n6gtgKS4D4Q0o="
        val hasher = shaPassFromHash(testHash).getOrThrow()
        assertEquals(testHash, hasher.getHash())
        assertEquals("G/8i8sbKhDu6+3e6BfPs8U75RsAiis6Te/MoC85+SfM=", hasher.getSalt())
        assertEquals("t=50000", hasher.getParameters())
        assertEquals(50_000, hasher.getIterations())
    }

    @Test
    fun setParameters() {
        val hasher = SHAPassword()
        assertEquals(100_000, hasher.getIterations())
        hasher.setIterations(10)
        assertEquals("t=10", hasher.getParameters())

        hasher.setParameters("t=5")?.let { throw it }
        assertEquals(5, hasher.getIterations())
        assertEquals("t=5", hasher.getParameters())
    }

    @Test
    fun setFromInfo() {
        val info = PasswordInfo(
            "SHA3-256",
            "G/8i8sbKhDu6+3e6BfPs8U75RsAiis6Te/MoC85+SfM=",
            "t=5"
        )
        val hasher = passhasherForInfo(info).getOrThrow() as SHAPassword
        assertEquals(5, hasher.getIterations())

        assert(passhasherForInfo(PasswordInfo("BADALGO")).isFailure)
    }

    @Test
    fun setStrength() {
        val hasher = SHAPassword()
        hasher.setIterations(5)
        hasher.setStrength(HashStrength.Basic)
        assertEquals(500_000, hasher.getIterations())
    }

    @Test
    fun compatibility() {
        val hasher = SHAPassword()
        val hash = hasher.updateHash("this is a test").getOrThrow()
        val hasher2 = SHAPassword()
        hasher2.setFromHash(hash)
        assert(hasher2.verify("this is a test"))
    }

    @Test
    fun updateSaltTest() {
        val hasher = SHAPassword()
        var currentSalt = hasher.getSalt()
        val initialSize = hasher.getSaltSize()

        hasher.updateSalt()
        assertNotEquals(currentSalt, hasher.getSalt())
        assertEquals(initialSize, hasher.getSaltSize())

        currentSalt = hasher.getSalt()
        hasher.updateSalt(24)
        assertNotEquals(currentSalt, hasher.getSalt())
        assertEquals(24, hasher.getSaltSize())
    }
}
