package utilities.messaging

import libmensago.SeparatedStringList
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SeparatedStringListTest {

    @Test
    fun basicTest() {
        assertEquals(
            "a::b::c::d",
            SeparatedStringList("a::b::c::d::", "::").join()
        )

        assertEquals(
            "a::b::c::d",
            SeparatedStringList("a::b::c::::::d::", "::").join()
        )

        assertEquals(
            "a::b",
            SeparatedStringList("a:b:c:d:", ":")
                .setSeparator("-")
                .set("a::b")
                .join()
        )

        assertEquals(
            "a-b-c-d-e::f",
            SeparatedStringList("a:b:c:d:", ":")
                .setSeparator("-")
                .push("e::f")
                .join()
        )
    }
}