package libmensago

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.test.Test
import kotlin.test.assertEquals

class DataFrameTest {

    @Test
    fun readFrame() {
        // Tests reading a single frame from a buffer
        val flatData = byteArrayOf(FrameType.SingleFrame.toByte(), 0, 7, 65, 66, 67, 68, 69, 70, 71)
        val istream = ByteArrayInputStream(flatData)
        val frame = DataFrame()
        frame.read(istream)?.let { throw it }

        assert(frame.getMultipartSize().isFailure)
        assertEquals(FrameType.SingleFrame, frame.type)
        assertEquals(7, frame.size)
        assertEquals("ABCDEFG", frame.payload.decodeToString())
    }

    @Test
    fun readMsgSingle() {
        // Tests reading a message from a buffer which consists of a single frame
        val flatData = byteArrayOf(FrameType.SingleFrame.toByte(), 0, 7, 65, 66, 67, 68, 69, 70, 71)
        val istream = ByteArrayInputStream(flatData)
        val data = readMessage(istream).getOrThrow()
        assertEquals("ABCDEFG", data.decodeToString())
    }

    @Test
    fun readMsgMulti() {
        // Tests reading a message from a buffer which consists of multiple frames
        MAX_MSG_SIZE = 3
        val flatData = byteArrayOf(
            FrameType.MultipartFrameStart.toByte(), 0, 1, 55,
            FrameType.MultipartFrame.toByte(), 0, 3, 65, 66, 67,
            FrameType.MultipartFrame.toByte(), 0, 3, 68, 69, 70,
            FrameType.MultipartFrameFinal.toByte(), 0, 1, 71
        )
        val istream = ByteArrayInputStream(flatData)
        val data = readMessage(istream).getOrThrow()
        MAX_MSG_SIZE = 65532
        assertEquals("ABCDEFG", data.decodeToString())
    }

    @Test
    fun writeMsg() {
        // A basic message writing case. Sending the message "ABCDEFG" should result in a bytearray
        // which contains the Single frame type followed by a 16-bit integer in MSB order which
        // contains the value 7, the length of the test string. The entire message should be just
        // 10 bytes.
        val testString = "ABCDEFG"

        val ostream = ByteArrayOutputStream()
        writeMessage(ostream, testString.toByteArray())?.let { throw it }
        val data = ostream.toByteArray()

        assertEquals(FrameType.SingleFrame.toByte(), data[0])
        assertEquals(0, data[1])
        assertEquals(testString.length, data[2].toInt())
        assertEquals(3 + testString.length, data.size)
    }

    @Test
    fun write4PartMsg() {
        // This test checks correctness for multipart messages: send the multipart header message
        // followed by 2 multipart frame messages followed by the message's tail.
        MAX_MSG_SIZE = 3
        val testString = "ABCDEFG"

        val ostream = ByteArrayOutputStream()
        writeMessage(ostream, testString.toByteArray())?.let { throw it }
        val data = ostream.toByteArray()
        MAX_MSG_SIZE = 65532

        // This multipart message should have
        // Frame 0: 3-byte header, 1-digit message payload size, total size: 4 bytes
        // Frame 1: 3-byte header, letters ABC, total size: 6 bytes
        // Frame 2: 3-byte header, letters DEF, total size: 6 bytes
        // Frame 3: 3-byte header, letter G, total size: 4 bytes

        assertEquals(data.size, 20)

        // Check Frame 0
        assertEquals(FrameType.MultipartFrameStart.toByte(), data[0])
        assertEquals(0, data[1])
        assertEquals(1, data[2])
        assertEquals(testString.length, byteArrayOf(data[3]).decodeToString().toInt())

        // Check Frame 1
        assertEquals(FrameType.MultipartFrame.toByte(), data[4])
        assertEquals(0, data[5])
        assertEquals(3, data[6])
        assertEquals("ABC", data.sliceArray(listOf(7, 8, 9)).decodeToString())

        // Check Frame 2
        assertEquals(FrameType.MultipartFrame.toByte(), data[10])
        assertEquals(0, data[11])
        assertEquals(3, data[12])
        assertEquals("DEF", data.sliceArray(listOf(13, 14, 15)).decodeToString())

        // Check Frame 3
        assertEquals(FrameType.MultipartFrameFinal.toByte(), data[16])
        assertEquals(0, data[17])
        assertEquals(1, data[18])
        assertEquals("G", byteArrayOf(data[19]).decodeToString())
    }

    @Test
    fun dataFrameBufferDebug() {
        // This test ensures that the getDataFrameDebugString() function works correctly
        assertEquals("<empty>", getDataFrameDebugString(byteArrayOf()))
        assertEquals("<32>", getDataFrameDebugString(byteArrayOf(50)))
        assertEquals("<32 14>", getDataFrameDebugString(byteArrayOf(50, 20)))
        assertEquals("<32 1415>", getDataFrameDebugString(byteArrayOf(50, 20, 21)))
        assertEquals(
            "<32 1415> 41424344 ABCD",
            getDataFrameDebugString(byteArrayOf(50, 20, 21, 65, 66, 67, 68))
        )
        val longBuffer = byteArrayOf(
            // Header
            50, 20, 21,
            // First sixteen bytes that should be displayed: 'ABCDEFGHIJKLMNOP'
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
            // Six bytes that shouldn't be displayed: 6 *'s
            42, 42, 42, 42, 42, 42,
            // Last sixteen bytes, which should also be displayed: 'abcdefghijklmnop'
            97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112
        )
        assertEquals(
            "<32 1415> 4142434445464748494a4b4c4d4e4f506162636465666768696a6b6c6d6e6f70 " +
                    "ABCDEFGHIJKLMNOPabcdefghijklmnop",
            getDataFrameDebugString(longBuffer)
        )
    }


    @Test
    fun bufferDebug() {
        // This test ensures that the getBufferDebugString() function works correctly
        assertEquals("<empty>", getBufferDebugString(byteArrayOf()))
        assertEquals(
            "414243444546 ABCDEF",
            getBufferDebugString(byteArrayOf(65, 66, 67, 68, 69, 70))
        )
        val longBuffer = byteArrayOf(
            // First sixteen bytes that should be displayed: 'ABCDEFGHIJKLMNOP'
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
            // Six bytes that shouldn't be displayed: 6 *'s
            42, 42, 42, 42, 42, 42,
            // Last sixteen bytes, which should also be displayed: 'abcdefghijklmnop'
            97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112
        )
        assertEquals(
            "4142434445464748494a4b4c4d4e4f506162636465666768696a6b6c6d6e6f70 " +
                    "ABCDEFGHIJKLMNOPabcdefghijklmnop",
            getBufferDebugString(longBuffer)
        )
    }
}
