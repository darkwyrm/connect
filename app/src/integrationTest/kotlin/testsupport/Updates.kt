package testsupport

import connect.Client
import connect.main.FilterItemProcessor
import connect.profiles.ProfileManager
import connect.sync.IncomingHandler
import libkeycard.RandomID
import libmensago.MServerPath
import libmensago.commands.*
import java.nio.file.Paths

fun generateDemoUpdates(
    env: ITestEnvironment,
    profileData: TestProfileData
): List<MServerPath> {

    data class FileInfo(
        var localPath: String, var serverPath: MServerPath, var name: String, var size: Int
    )

    // Generate updates of all types with this call
    val wid = profileData.wid

    // Multiple MKDIR records
    val dirs = listOf(
        MServerPath("/ $wid messages 5314670e-117d-41fc-80b3-b099f33f257c"),
        MServerPath("/ $wid messages 334c10b4-8ea2-4d07-baf7-2d37ce91b883"),
        MServerPath("/ $wid messages ee114b9e-ad85-4c65-a232-2867c14c020f"),
        MServerPath("/ $wid messages 87ba5b60-461f-4c4e-8863-a1566eb9a54b"),
    )

    dirs.forEach {
        mkdir(env.conn, it)?.let { e -> throw e }
    }

    // RMDIR
    rmdir(env.conn, dirs[3])?.let { throw it }

    // At this point, we still have the first three directories created above

    // CREATE
    val fileInfo = (0..5).map {
        val pair = makeTestNoteFile(env.testPath, profileData.encryption, it.toString())
        val localPath = Paths.get(env.testPath, pair.first).toString()
        val serverPath = MServerPath.fromString("/ ${profileData.wid} messages ${pair.first}")!!

        val serverData = upload(env.conn, localPath, serverPath.parent()!!).getOrThrow()
        FileInfo(
            localPath, serverPath.parent()!!.push(serverData.first).getOrThrow(),
            serverData.first, pair.second
        )
    }

    // REPLACE
    val replacePair = makeTestNoteFile(env.testPath, profileData.encryption, "5")
    val localPath = Paths.get(env.testPath, replacePair.first).toString()
    val serverPath = MServerPath.fromString("/ ${profileData.wid} messages ${replacePair.first}")!!

    val replaceData = upload(env.conn, localPath, serverPath.parent()!!, fileInfo[4].serverPath)
        .getOrThrow()
    fileInfo[4].localPath = localPath
    fileInfo[4].serverPath = serverPath.parent()!!.push(replaceData.first).getOrThrow()
    fileInfo[4].name = replaceData.first
    fileInfo[4].size = replacePair.second

    // MOVE
    val moveDest = MServerPath("/ ${profileData.wid} messages ee114b9e-ad85-4c65-a232-2867c14c020f")
    move(env.conn, fileInfo[5].serverPath.toString(), moveDest.toString())?.let { throw it }

    // DELETE
    select(env.conn, moveDest)?.let { throw it }
    delete(env.conn, listOf(fileInfo[5].name))?.let { throw it }

    // Now that we've generated the updates the easy way, we need to manually change the device ID
    // identified as the source of the updates and reset the local profile folder so that the local
    // side is like a fresh new device.

    val newDeviceID = RandomID.fromString("19d720b6-9f67-4a32-8ab6-313045489348")
    env.db!!.execute(
        "UPDATE updates SET devid=? WHERE wid=?", newDeviceID.toString(), profileData.wid
    )

    return dirs
}

/**
 * Downloads and processes updates for the current profile.
 */
fun runUpdates(profileData: TestProfileData, itemproc: FilterItemProcessor) {
    val client = Client()
    val handler = IncomingHandler(client, itemproc, ProfileManager.model)
    client.login(profileData.address).getOrThrow()
    handler.downloadUpdates()?.let { throw it }
    handler.executeTasks()?.let { throw it }
    client.logout()?.let { throw it }
    client.disconnect()?.let { throw it }
}
