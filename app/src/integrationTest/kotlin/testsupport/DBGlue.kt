package testsupport

import keznacl.CryptoString
import keznacl.EmptyDataException
import libkeycard.*
import libmensago.DeviceStatus
import libmensago.NotConnectedException
import libmensago.ResourceExistsException
import libmensago.WorkspaceStatus
import utilities.getDomain

/**
 * Adds a device to a workspace. The device's initial last login is set to when this method is
 * called because a new device is added only at certain times, such as at registration or when a
 * user logs into a workspace on a new device.
 *
 * @throws NotConnectedException Returned if not connected to the database
 * @throws java.sql.SQLException Returned for database problems, most likely either with your query
 * or with the connection
 */
fun addDevice(
    db: PGConn, wid: RandomID, devid: RandomID, devkey: CryptoString,
    devInfo: CryptoString, status: DeviceStatus
) {
    val now = Timestamp()
    db.execute(
        """INSERT INTO iwkspc_devices(wid, devid, devkey, lastlogin, devinfo, status)
        VALUES(?,?,?,?,?,?)""", wid, devid, devkey, now, devInfo, status
    )
}

/**
 * Adds an entry to the database. The caller is responsible for validation of the entry passed to
 * this command.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 */
fun addEntry(db: PGConn, entry: Entry) {
    val owner = if (entry.getFieldString("Type") == "Organization") "organization"
    else entry.getFieldString("Workspace-ID")!!

    db.execute(
        """INSERT INTO keycards(owner, creationtime, index, entry, fingerprint)
        VALUES(?,?,?,?,?)""",
        owner, entry.getFieldString("Timestamp")!!,
        entry.getFieldInteger("Index")!!, entry.getFullText(null).getOrThrow(),
        entry.getAuthString("Hash")!!
    )
}

/**
 * addWorkspace is used for adding a workspace to the database. Other tasks related to provisioning
 * a workspace are not handled here.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 * @throws ResourceExistsException if the workspace ID already exists in the database
 */
fun addWorkspace(
    db: PGConn, wid: RandomID, uid: UserID?, domain: Domain, passhash: String,
    algorithm: String, salt: String, passParams: String, status: WorkspaceStatus,
    wtype: WorkspaceType
) {

    val wStatus = checkWorkspace(db, wid)

    if (wStatus != null && wStatus != WorkspaceStatus.Preregistered)
        throw ResourceExistsException("$wid exists")
    db.execute(
        """INSERT INTO workspaces(wid, uid, domain, password, passtype, salt,
        passparams, status, wtype) VALUES(?,?,?,?,?,?,?,?,?)""",
        wid, uid ?: "", domain, passhash, algorithm, salt, passParams, status, wtype
    )
}

/**
 * checkWorkspace checks to see if a workspace exists. If it does exist, its status is returned. If
 * not, null is returned.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 */
fun checkWorkspace(db: PGConn, wid: RandomID): WorkspaceStatus? {
    var rs = db.query("""SELECT status FROM workspaces WHERE wid=? AND wtype!='alias'""", wid)
    if (rs.next()) {
        val stat = rs.getString("status")
        val out = WorkspaceStatus.fromString(stat)
            ?: throw DatabaseCorruptionException("Bad workspace status '$stat' for workspace $wid")

        return out
    }
    rs = db.query("""SELECT wid FROM prereg WHERE wid=?""", wid)
    if (rs.next())
        return WorkspaceStatus.Preregistered

    return null
}

/**
 * deletePrereg removes preregistration data from the database.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 */
fun deletePrereg(db: PGConn, addr: WAddress) {
    db.execute("DELETE FROM prereg WHERE wid=? AND domain=?", addr.id, addr.domain)
}

/**
 * preregWorkspace preregisters a workspace in the database.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 * @throws ResourceExistsException if the user ID or workspace ID given already exist.
 * @throws EmptyDataException if the registration code hash string is empty
 */
fun preregWorkspace(db: PGConn, wid: RandomID, userID: UserID?, domain: Domain, reghash: String) {

    if (reghash.isEmpty())
        throw EmptyDataException("registration code hash may not be empty")

    if (userID != null) {
        val resolvedWID = resolveUserID(db, userID)
        if (resolvedWID != null)
            throw ResourceExistsException("User-ID $userID already exists")
        val resolvedWAddr = resolveWID(db, wid)
        if (resolvedWAddr != null)
            throw ResourceExistsException("Workspcae-ID $wid already exists")

        db.execute(
            """INSERT INTO prereg(wid, uid, domain, regcode) VALUES(?,?,?,?)""",
            wid, userID, domain, reghash
        )
        return
    }

    if (resolveWID(db, wid) != null)
        throw ResourceExistsException("Workspace-ID $wid already exists")
    db.execute(
        """INSERT INTO prereg(wid, domain, regcode) VALUES(?,?,?)""", wid, domain,
        reghash
    )
}

/**
 * resolveWID returns a full workspace address given the workspace ID component or null if not
 * found.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 */
fun resolveWID(db: PGConn, wid: RandomID): WAddress? {
    val rs = db.query("""SELECT domain FROM workspaces WHERE wid=?""", wid)
    if (!rs.next()) return null

    val dom = rs.getDomain("domain")
        ?: throw DatabaseCorruptionException(
            "Bad domain '${rs.getString("domain")}' for workspace ID $wid"
        )
    return WAddress.fromParts(wid, dom)
}

/**
 * resolveUserID attempts to return the RandomID corresponding to the specified UserID. If it does
 * not exist, null is returned.
 *
 * @throws NotConnectedException if not connected to the database
 * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
 */
fun resolveUserID(db: PGConn, uid: UserID?): RandomID? {
    if (uid == null) return null

    for (table in listOf("workspaces", "prereg")) {
        val rs = db.query("""SELECT wid FROM $table WHERE uid=?""", uid)
        if (rs.next()) {
            val widStr = rs.getString("wid")
            val out = RandomID.fromString(widStr)
                ?: throw DatabaseCorruptionException("Bad workspace ID '$widStr' for userID $uid")

            return out
        }
    }
    return null
}

enum class WorkspaceType {
    Individual,
    Shared;

    override fun toString(): String {
        return when (this) {
            Individual -> "individual"
            Shared -> "shared"
        }
    }

    companion object {

        fun fromString(s: String): WorkspaceType? {
            return when (s.lowercase()) {
                "individual" -> Individual
                "shared" -> Shared
                else -> null
            }
        }
    }
}
