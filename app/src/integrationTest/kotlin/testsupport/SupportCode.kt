package testsupport

import connect.Client
import connect.profiles.ProfileManager
import keznacl.EncryptionPair
import keznacl.Encryptor
import keznacl.MissingDataException
import keznacl.SigningPair
import libkeycard.Keycard
import libkeycard.OrgEntry
import libkeycard.OrgKeySet
import libkeycard.RandomID
import libmensago.ContentFormat
import libmensago.Envelope
import libmensago.Note
import libmensago.makeEnvelopeFilename
import org.apache.commons.io.FileUtils
import utilities.ServerConfig
import java.io.File
import java.nio.file.Paths
import java.security.SecureRandom
import java.sql.Connection
import java.sql.DriverManager
import java.time.Instant
import java.util.*

//! This file contains setup functions needed by the integration tests

// THESE KEYS ARE STORED ON GITLAB! DO NOT USE THESE FOR ANYTHING EXCEPT TESTS!!

// Test Organization Information

// Name: Example.com
// Contact-Admin: ae406c5e-2673-4d3e-af20-91325d9623ca/acme.com
// Support and Abuse accounts are forwarded to Admin
// Language: en

// Initial Organization Primary Signing Key: {UNQmjYhz<(-ikOBYoEQpXPt<irxUF*nq25PoW=_
// Initial Organization Primary Verification Key: r#r*RiXIN-0n)BzP3bv`LA&t4LFEQNF0Q@$N~RF*
// Initial Organization Primary Verification Key Hash:
// BLAKE2B-256:ag29av@TUvh-V5KaB2l}H=m?|w`}dvkS1S1&{cMo

// Initial Organization Encryption Key: SNhj2K`hgBd8>G>lW$!pXiM7S-B!Fbd9jT2&{{Az
// Initial Organization Encryption Key Hash: BLAKE2B-256:-Zz4O7J;m#-rB)2llQ*xTHjtblwm&kruUVa_v(&W
// Initial Organization Decryption Key: WSHgOhi+bg=<bO^4UoJGF-z9`+TBN{ds?7RZ;w3o

// THESE KEYS ARE STORED ON GITLAB! DO NOT USE THESE FOR ANYTHING EXCEPT TESTS!!

/** Returns the canonical version of the path specified. */
fun getPathForTest(testName: String): String? {
    return runCatching {
        Paths.get("build", "testfiles", testName).toAbsolutePath().toString()
    }.getOrElse { null }
}

/** Empties and resets the server's database to start from a clean slate */
fun setupTest(config: ServerConfig): Connection {
    // jdbc:postgresql://host:port/database?param1=value1&param2=value2&…
    val sb = StringBuilder("jdbc:postgresql://")
    sb.append(
        config.getString("database.host") + ":" +
                config.getInteger("database.port").toString()
    )
    sb.append("/" + config.getString("database.name"))

    val args = Properties()
    args["user"] = config.getString("database.user")

    if (config.getString("database.password")!!.isEmpty())
        throw MissingDataException("Database password must not be empty")
    args["password"] = config.getString("database.password")

    DBConn.initialize(config)?.let { throw it }

    val db = DriverManager.getConnection(sb.toString(), args)
    val stmt = db.createStatement()

    // Drop all tables in the database
    stmt.addBatch(
        """
	DO ${'$'}${'$'} DECLARE
		r RECORD;
	BEGIN
		FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
			EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
		END LOOP;
	END ${'$'}${'$'};
    """
    )

    // Create new ones

    // For logging different types of failures, such as failed usernanme entry or a server's failure
    // to authenticate for delivery. Information stored here is to ensure that all parties which the
    // server interacts with behave themselves.
    stmt.addBatch(
        """CREATE TABLE failure_log(rowid SERIAL PRIMARY KEY, type VARCHAR(16) NOT NULL,
        id VARCHAR(36), source VARCHAR(36) NOT NULL, count INTEGER,
        last_failure CHAR(20) NOT NULL, lockout_until CHAR(20));"""
    )

    // Devices registered to each individual's workspace
    stmt.addBatch(
        """CREATE TABLE iwkspc_devices(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        devid CHAR(36) NOT NULL, devkey VARCHAR(1000) NOT NULL,
        devinfo VARCHAR(8192) NOT NULL,
        lastlogin VARCHAR(32) NOT NULL, status VARCHAR(16) NOT NULL);"""
    )

    // Information about individual workspaces
    stmt.addBatch(
        """CREATE TABLE iwkspc_folders(rowid SERIAL PRIMARY KEY, wid char(36) NOT NULL,
        serverpath VARCHAR(512) NOT NULL, clientpath VARCHAR(768) NOT NULL);"""
    )

    // Stores all entries in the keycard tree
    stmt.addBatch(
        """CREATE TABLE keycards(rowid SERIAL PRIMARY KEY, owner VARCHAR(292) NOT NULL,
        creationtime CHAR(20) NOT NULL, index INTEGER NOT NULL,
        entry VARCHAR(8192) NOT NULL, fingerprint VARCHAR(96) NOT NULL);"""
    )

    // Locations for key information packages needed for key exchange to new devices
    stmt.addBatch(
        """CREATE TABLE keyinfo(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        devid CHAR(36) NOT NULL, path VARCHAR(128));"""
    )

    // Keycard information for the organization
    stmt.addBatch(
        """CREATE TABLE orgkeys(rowid SERIAL PRIMARY KEY, creationtime CHAR(20) NOT NULL, 
        pubkey VARCHAR(7000), privkey VARCHAR(7000) NOT NULL, 
        purpose VARCHAR(8) NOT NULL, fingerprint VARCHAR(96) NOT NULL);"""
    )

    // Preregistration information. Entries are removed upon successful account registration.
    stmt.addBatch(
        """CREATE TABLE prereg(rowid SERIAL PRIMARY KEY, wid VARCHAR(36) NOT NULL UNIQUE,
        uid VARCHAR(128), domain VARCHAR(255) NOT NULL, regcode VARCHAR(128));"""
    )

    // Disk quota tracking
    stmt.addBatch(
        """CREATE TABLE quotas(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        usage BIGINT, quota BIGINT);"""
    )

    // resetcodes table is used for password resets
    stmt.addBatch(
        """CREATE TABLE resetcodes(rowid SERIAL PRIMARY KEY, wid VARCHAR(36) NOT NULL UNIQUE,
        resethash VARCHAR(128) NOT NULL, expires CHAR(20) NOT NULL);"""
    )

    /*
        For logging updates made to a workspace. This table is critical to device synchronization.
        The update_data field is specific to the update type.

        Update Types
        1: CREATE. An item has been created. update_data contains the path of the item created. Note
           that this applies both to files and directories
        2: DELETE. An item has een deleted. update_data contains the path of the item created. Note
           that this applies both to files and directories. If a directory has been deleted, all of
           its contents have also been deleted, which improves performance when large directories
           go away.
        3: MOVE. An item has been moved. update_data contains two paths, the source and the
           destination. The source path contains the directory path, and in the case of a file, the
           file name. The destination contains only a folder path.
        4: ROTATE. Keys have been rotated. update_data contains the path to the encrypted key
           storage package.
    */
    stmt.addBatch(
        "CREATE TABLE updates(rowid SERIAL PRIMARY KEY, rid CHAR(36) NOT NULL, " +
                "wid CHAR(36) NOT NULL, domain VARCHAR(255) NOT NULL, update_type VARCHAR(16), " +
                "update_data VARCHAR(2048), unixtime BIGINT, devid CHAR(36) NOT NULL);"
    )

    // Lookup table for all workspaces. When any workspace is created, its wid is added here.
    // userid is optional. wtype can be 'individual', 'sharing', 'group', or 'alias'
    stmt.addBatch(
        """CREATE TABLE workspaces(rowid BIGSERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
            uid VARCHAR(64), domain VARCHAR(255) NOT NULL, wtype VARCHAR(32) NOT NULL,
            status VARCHAR(16) NOT NULL, password VARCHAR(256), passtype VARCHAR(32),
            salt VARCHAR(128), passparams VARCHAR(128));"""
    )

    stmt.executeBatch()

    return db
}

/**
 * Adds basic data to the database as if setup had been run. It also rotates the org keycard so that
 * there are two entries. Returns data needed for tests, such as the keys
 */
fun initServer(db: Connection): Map<String, String> {
    val initialOSPair = SigningPair.fromStrings(
        "ED25519:r#r*RiXIN-0n)BzP3bv`LA&t4LFEQNF0Q@\$N~RF*",
        "ED25519:{UNQmjYhz<(-ikOBYoEQpXPt<irxUF*nq25PoW=_"
    ).getOrThrow()
    val initialOEPair = EncryptionPair.fromStrings(
        "CURVE25519:SNhj2K`hgBd8>G>lW\$!pXiM7S-B!Fbd9jT2&{{Az",
        "CURVE25519:WSHgOhi+bg=<bO^4UoJGF-z9`+TBN{ds?7RZ;w3o"
    ).getOrThrow()

    val rootEntry = OrgEntry()
    rootEntry.setFieldInteger("Index", 1)
    rootEntry.setField("Name", "Example, Inc.")
    rootEntry.setField(
        "Contact-Admin",
        "c590b44c-798d-4055-8d72-725a7942f3f6/example.com"
    )
    rootEntry.setField("Language", "eng")
    rootEntry.setField("Domain", "example.com")
    rootEntry.setField("Primary-Verification-Key", initialOSPair.pubKey.toString())
    rootEntry.setField("Encryption-Key", initialOEPair.pubKey.toString())

    rootEntry.isDataCompliant()?.let { throw it }
    rootEntry.hash()?.let { throw it }
    rootEntry.sign("Organization-Signature", initialOSPair)?.let { throw it }
    rootEntry.verifySignature("Organization-Signature", initialOSPair).getOrThrow()
    rootEntry.isCompliant()?.let { throw it }

    val orgCard = Keycard.new("Organization")!!
    orgCard.entries.add(rootEntry)

    var stmt = db.prepareStatement(
        """INSERT INTO keycards(owner,creationtime,index,entry,fingerprint)
        VALUES('organization',?,?,?,?);"""
    )
    stmt.setString(1, rootEntry.getFieldString("Timestamp")!!)
    stmt.setInt(2, rootEntry.getFieldInteger("Index")!!)
    stmt.setString(3, rootEntry.getFullText(null).getOrThrow())
    stmt.setString(4, rootEntry.getAuthString("Hash")!!.toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint)
        VALUES(?,?,?,'encrypt',?);"""
    )
    stmt.setString(1, rootEntry.getFieldString("Timestamp")!!)
    stmt.setString(2, initialOEPair.pubKey.toString())
    stmt.setString(3, initialOEPair.privKey.toString())
    stmt.setString(4, initialOEPair.pubKey.hash().getOrThrow().toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint)
        VALUES(?,?,?,'sign',?);"""
    )
    stmt.setString(1, rootEntry.getFieldString("Timestamp")!!)
    stmt.setString(2, initialOSPair.pubKey.toString())
    stmt.setString(3, initialOSPair.privKey.toString())
    stmt.setString(4, initialOSPair.pubKey.hash().getOrThrow().toString())
    stmt.execute()

    // Now that we've added the organization's root keycard entry and keys, chain a new entry and
    // add it to the database.
    val keys = orgCard.chain(initialOSPair, 365).getOrThrow()
    keys as OrgKeySet

    val newEntry = orgCard.entries[1]

    stmt = db.prepareStatement(
        """INSERT INTO keycards(owner,creationtime,index,entry,fingerprint)
        VALUES('organization',?,?,?,?);"""
    )
    stmt.setString(1, newEntry.getFieldString("Timestamp")!!)
    stmt.setInt(2, newEntry.getFieldInteger("Index")!!)
    stmt.setString(3, newEntry.getFullText(null).getOrThrow())
    stmt.setString(4, newEntry.getAuthString("Hash")!!.toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """UPDATE orgkeys SET creationtime=?,pubkey=?,privkey=?,fingerprint=?
        WHERE purpose='encrypt';"""
    )
    stmt.setString(1, newEntry.getFieldString("Timestamp")!!)
    stmt.setString(2, keys.getEPair().pubKey.toString())
    stmt.setString(3, keys.getEPair().privKey.toString())
    stmt.setString(4, keys.getEPair().pubKey.hash().getOrThrow().toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """UPDATE orgkeys SET creationtime=?,pubkey=?,privkey=?,fingerprint=?
        WHERE purpose='sign';"""
    )
    stmt.setString(1, newEntry.getFieldString("Timestamp")!!)
    stmt.setString(2, keys.getSPair().pubKey.toString())
    stmt.setString(3, keys.getSPair().privKey.toString())
    stmt.setString(4, keys.getSPair().pubKey.hash().getOrThrow().toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint)
        VALUES(?,?,?,'altsign',?);"""
    )
    stmt.setString(1, rootEntry.getFieldString("Timestamp")!!)
    stmt.setString(2, initialOSPair.pubKey.toString())
    stmt.setString(3, initialOSPair.privKey.toString())
    stmt.setString(4, initialOSPair.pubKey.hash().getOrThrow().toString())
    stmt.execute()

    // Preregister the admin account

    val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"]!!)!!
    stmt = db.prepareStatement(
        """INSERT INTO prereg(wid,uid,domain,regcode) 
        VALUES(?,'admin','example.com',?);"""
    )
    stmt.setString(1, adminWID.toString())
    stmt.setString(2, ADMIN_PROFILE_DATA["reghash"]!!)
    stmt.execute()

    // Set up abuse/support forwarding to admin

    stmt = db.prepareStatement(
        """INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) 
        VALUES(?,'abuse','example.com','-', '','active','alias');"""
    )
    stmt.setString(1, adminWID.toString())
    stmt.execute()

    stmt = db.prepareStatement(
        """INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) 
        VALUES(?,'support','example.com','-', '','active','alias');"""
    )
    stmt.setString(1, adminWID.toString())
    stmt.execute()

    return mutableMapOf(
        "ovkey" to keys.getSPair().pubKey.toString(),
        "oskey" to keys.getSPair().privKey.toString(),
        "oekey" to keys.getEPair().pubKey.toString(),
        "odkey" to keys.getEPair().privKey.toString(),
        "roothash" to orgCard.current!!.getAuthString("Hash")!!.toString(),
        "admin_wid" to adminWID.toString(),
        "admin_regcode" to ADMIN_PROFILE_DATA["regcode"]!!,
    )
}

/**
 * Creates a new profile folder hierarchy on the client side in the specified test folder and
 * returns the path to it. This sets up a bare minimum environment upon which any test can build.
 * No initialization of any kind is performed except setting up the folder hierarchy and ensuring
 * that it is empty.
 */
fun setupProfileBase(name: String): String {
    val dir = File(getPathForTest(name)!!)
    if (dir.exists())
        FileUtils.cleanDirectory(dir)
    else
        dir.mkdirs()

    return dir.toString()
}

/**
 * Adds to an empty profile folder the bare minimum of setup needed to operate Connect -- one which
 * is ready to add data to.
 */
fun initProfile(profileFolder: String) {
    with(ProfileManager) {
        setLocation(Paths.get(profileFolder))?.let { throw it }
        File(profileFolder).mkdirs()
        scanProfiles()?.let { throw it }
    }
}

/**
 * This function takes the place of fullTestSetup, but utilizes the Client class to do the work.
 * This is needed to perform higher-level tests without code duplication or receiving SQLite lock
 * errors because there are 2 ProfileManager instances in existence for the same test database.
 *
 * The Client instance returned from this function will have an active server connection, but will
 * not be logged in as any user.
 */
fun clientBasedTestSetup(testName: String): Client {
    ITestEnvironment(testName).provision(SETUP_TEST_ADMIN_PROFILE)
    return Client().apply { connect(gAdminProfileData.domain)?.let { throw it } }
}

// Commands for filesystem-related integration tests

/** Resets the system workspace storage directory to an empty skeleton */
fun resetWorkspaceDir(config: ServerConfig) {

    val topDir = File(config.getString("global.top_dir")!!)
    if (topDir.exists())
    // The only reason we'll get an exception here is if we can't delete the folder under
    // Windows, and that's not a problem.... at least for now anyway.
        runCatching { FileUtils.cleanDirectory(topDir) }
}

/**
 * Generate a test file containing nothing but zeroes. If the filename is not specified, a random
 * name will be generated. If the file size is not specified, the file will be between 1 and 10KB
 */
fun makeTestFile(
    fileDir: String, fileName: String? = null,
    fileSize: Int? = null
): Pair<String, Int> {

    val rng = SecureRandom()
    val realSize = fileSize ?: ((rng.nextInt(10) + 1) * 1024)

    val realName = if (fileName.isNullOrEmpty()) {
        "${Instant.now().epochSecond}.$realSize.${UUID.randomUUID().toString().lowercase()}"
    } else {
        fileName
    }

    val fHandle = File(Paths.get(fileDir, realName).toString())
    fHandle.writeBytes("0".repeat(realSize).toByteArray())

    return Pair(realName, realSize)
}

/**
 * Generate a fully-encrypted test envelope file containing a note.
 *
 * @param fileDir A string pointing to the parent directory in which the file will be created
 * @param payloadKey An encryption key used to encrypt the note's ephemeral key
 * @param identifier An identifier for the note itself, such as a letter or number, e.g. 'A' ->
 * "Note A".
 * @return A pair containing the note's filename and its file size
 */
fun makeTestNoteFile(
    fileDir: String, payloadKey: Encryptor, identifier: String = ""
): Pair<String, Int> {
    val note = Note(
        "Integration Test Note $identifier", "This is sample test content $identifier",
        ContentFormat.Text
    )
    val sealed = Envelope.seal(payloadKey, null, note.toPayload().getOrThrow()).getOrThrow()

    // We don't call makeEnvelopeFilename here because we actually need the name and not the full
    // path to the file.
    val fileSize = sealed.toString().length
    val destName = makeEnvelopeFilename(fileSize)

    sealed.saveWithName(Paths.get(fileDir, destName).toString())?.let { throw it }

    return Pair(destName, fileSize)
}

/**
 * Convenience function which ensures the existence of the server-side storage location of a
 * workspace's data. An optional list of subdirectory paths may also be specified. These subpaths
 * can be a single directory name ("foo") or a series of nested directories ("foo/bar/baz"). The
 * function will ensure all specified paths are created.
 */
fun ensureWorkspacePaths(
    config: ServerConfig, widStr: String,
    subDirs: List<String>? = null
): String {
    val dirPath = Paths.get(config.getString("global.workspace_dir")!!, widStr).toString()
    val wDir = File(dirPath)
    if (!wDir.exists()) {
        wDir.mkdirs()
    }

    subDirs?.forEach {
        File(Paths.get(dirPath, it).toString()).also { subDir ->
            if (!subDir.exists()) subDir.mkdirs()
        }
    }
    return dirPath
}
