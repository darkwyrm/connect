package testsupport

import keznacl.HashStrength
import keznacl.Password
import keznacl.toSuccess

/**
 * This Password subclass exists ONLY for integration testing. It implements a cleartext password
 * protocol so that integration tests are faster. Using Argon2ID for client-side integration testing
 * is much, much slower (and _should_ be). This does not affect security of the regular code. Actual
 * client code uses Argon2ID for password hashing.
 *
 * DO NOT USE THIS CLASS FOR ANYTHING BESIDES TESTING. YOU HAVE BEEN WARNED.
 */
class ClearPassword : Password() {

    init {
        info.algorithm = "PLAIN"
    }

    override fun copy(): Password {
        val out = ClearPassword()
        out.info = info.copy()
        out.hashValue = hashValue
        return out
    }

    override fun setFromHash(hashStr: String): Throwable? {
        hashValue = hashStr
        return null
    }

    /**
     * Sets parameters for the algorithm based on the strength requested. The specific parameter
     * values assigned to a strength level is specific to the algorithm.
     */
    override fun setStrength(strength: HashStrength): Password {
        // No-op
        return this
    }

    /** Creates a new hash given a cleartext password. */
    override fun updateHash(pw: String): Result<String> {
        hashValue = pw
        return hashValue.toSuccess()
    }

    /** Checks the cleartext password against the object's hash and returns true if they match */
    override fun verify(pw: String): Boolean {
        return pw == hashValue
    }

    companion object {
        fun fromString(s: String): ClearPassword? {
            val pwd = ClearPassword()
            pwd.setFromHash(s)?.let { return null }
            return pwd
        }
    }
}
