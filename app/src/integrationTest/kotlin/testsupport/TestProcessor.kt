package testsupport

import connect.FilterableItem
import connect.contacts.ContactItem
import connect.main.FilterItemProcessor
import connect.messages.MsgBaseItem
import connect.notes.NoteItem
import keznacl.BadValueException
import libmensago.MServerPath
import libmensago.ResourceNotFoundException

class UnhandledItemTypeException(msg: String = "") : Exception(msg)

/** Test class for mocking out the interface to MainController for item processing */
class TestProcessor : FilterItemProcessor {
    val msgItems = mutableListOf<MsgBaseItem>()
    val contactItems = mutableListOf<ContactItem>()
    val noteItems = mutableListOf<NoteItem>()

    override fun processNewItem(item: FilterableItem): Throwable? {
        when (item) {
            is MsgBaseItem -> msgItems.add(item)
            is ContactItem -> contactItems.add(item)
            is NoteItem -> noteItems.add(item)
            else -> return UnhandledItemTypeException(item.toString())
        }
        return null
    }

    override fun processMoveItem(oldPath: MServerPath, newPath: MServerPath) {}

    override fun processReplaceItem(oldItem: MServerPath, newItem: FilterableItem) {}

    override fun updateContactInfo(item: FilterableItem): Throwable? {
        if (item !is ContactItem)
            return BadValueException("Can't update contact from item $item")

        val conItem = contactItems.find { it.id == item.id }
            ?: return ResourceNotFoundException("Contact for ID not found")

        conItem.updateFrom(item)
        return null
    }
}
