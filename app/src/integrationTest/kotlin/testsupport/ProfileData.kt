@file:Suppress("unused")

package testsupport

import connect.gUserConID
import javafx.scene.image.Image
import keznacl.Base85
import keznacl.EncryptionPair
import keznacl.SecretKey
import keznacl.SigningPair
import libkeycard.*
import libmensago.Contact
import utilities.imageToBytes

// Test profile data for the administrator account used in integration tests
private const val adminKeycard = "Type:User\r\n" +
        "Index:1\r\n" +
        "Name:Administrator\r\n" +
        "User-ID:admin\r\n" +
        "Workspace-ID:ae406c5e-2673-4d3e-af20-91325d9623ca\r\n" +
        "Domain:example.com\r\n" +
        "Contact-Request-Verification-Key:ED25519:E?_z~5@+tkQz!iXK?oV<Zx(ec;=27C8Pjm((kRc|\r\n" +
        "Contact-Request-Encryption-Key:CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{\r\n" +
        "Encryption-Key:CURVE25519:Umbw0Y<^cf1DN|>X38HCZO@Je(zSe6crC6X_C_0F\r\n" +
        "Verification-Key:ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p\r\n" +
        "Time-To-Live:14\r\n" +
        "Expires:2024-02-20\r\n" +
        "Timestamp:2024-01-21T20:02:41Z\r\n"

val ADMIN_PROFILE_DATA = mutableMapOf(
    "name" to "Mensago Administrator",
    "uid" to "admin",
    "wid" to "ae406c5e-2673-4d3e-af20-91325d9623ca",
    "domain" to "example.com",
    "address" to "admin/example.com",
    "waddress" to "ae406c5e-2673-4d3e-af20-91325d9623ca/example.com",
    "password" to "Linguini2Pegboard*Album",
    "passhash" to "\$sha3-256\$t=2\$O+SqHAqQZWb7ZXp+hOjBfF4FDvR/1akVfZRiJ+tTdrg=" +
            "\$jJSX1dU71x3bf8k6b/CenILU/DO2/UcljOt8t+0CrXc=",
    "client_passhash" to "\$sha3-256\$t=2\$wE313+fzp8wMSWd0QYcFjP4DFyaXni9oE/CwW6oJ82E=" +
            "\$Q/aD1jFYIUuWGI+X5szIiA/SadsvOkz9KqKZh3tSxkg=",
    "crencryption.public" to "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
    "crencryption.private" to "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>",
    "crsigning.public" to "ED25519:E?_z~5@+tkQz!iXK?oV<Zx(ec;=27C8Pjm((kRc|",
    "crsigning.private" to "ED25519:u4#h6LEwM6Aa+f<++?lma4Iy63^}V\$JOP~ejYkB;",
    "encryption.public" to "CURVE25519:Umbw0Y<^cf1DN|>X38HCZO@Je(zSe6crC6X_C_0F",
    "encryption.private" to "CURVE25519:Bw`F@ITv#sE)2NnngXWm7RQkxg{TYhZQbebcF5b$",
    "signing.public" to "ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p",
    "signing.private" to "ED25519:p;XXU0XF#UO^}vKbC-wS(#5W6=OEIFmR2z`rS1j+",
    "storage" to "XSALSA20:M^z-E(u3QFiM<QikL|7|vC|aUdrWI6VhN+jt>GH}",
    "folder" to "XSALSA20:H)3FOR}+C8(4Jm#\$d+fcOXzK=Z7W+ZVX11jI7qh*",
    "device.public" to "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
    "device.private" to "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>",
    "devid" to "3abaa743-40d9-4897-ac77-6a7783083f30",
    "regcode" to "Undamaged Shining Amaretto Improve Scuttle Uptake",
    "reghash" to "\$sha3-256\$t=2\$A80fwv/3mo4cX4DMViiK2yT8r8Hbeac1xCnMqAtjA+0=\$JuOcXGZk2B/vK1akaibAHcuHchRfhtI0e9TaJ/4CzGw=",
    "keycard" to adminKeycard
)

internal const val userKeycard = "Type:User\r\n" +
        "Index:1\r\n" +
        "Name:Corbin Simons\r\n" +
        "User-ID:csimons\r\n" +
        "Workspace-ID:4418bf6c-000b-4bb3-8111-316e72030468\r\n" +
        "Domain:example.com\r\n" +
        "Contact-Request-Verification-Key:ED25519:d0-oQb;{QxwnO{=!|^62+E=UYk2Y3mr2?XKScF4D\r\n" +
        "Contact-Request-Encryption-Key:CURVE25519:j(IBzX*F%OZF;g77O8jrVjM1a`Y<6-ehe{S;{gph\r\n" +
        "Encryption-Key:CURVE25519:nSRso=K(WF{P+4x5S*5?Da-rseY-^>S8VN#v+)IN\r\n" +
        "Verification-Key:ED25519:k^GNIJbl3p@N=j8diO-wkNLuLcNF6#JF=@|a}wFE\r\n" +
        "Time-To-Live:14\r\n" +
        "Expires:2024-02-20\r\n" +
        "Timestamp:2024-01-21T20:02:41Z\r\n"

val USER_PROFILE_DATA = mutableMapOf(
    "name" to "Corbin Simons",
    "uid" to "csimons",
    "wid" to "4418bf6c-000b-4bb3-8111-316e72030468",
    "domain" to "example.com",
    "address" to "csimons/example.com",
    "waddress" to "4418bf6c-000b-4bb3-8111-316e72030468/example.com",
    "password" to "MyS3cretPassw*rd",
    "passhash" to "\$sha3-256\$t=2\$O+SqHAqQZWb7ZXp+hOjBfF4FDvR/1akVfZRiJ+tTdrg=" +
            "\$DDJZQQbOwIY+NQo+JKVmypJygW0V7JjthALz8FJPbxM=",
    "client_passhash" to "\$sha3-256\$t=2\$wE313+fzp8wMSWd0QYcFjP4DFyaXni9oE/CwW6oJ82E=" +
            "\$cKH77CX1S+wGyLSSzEveNyD71Wbl1MOPWlVaOCsCwmE=",
    "crencryption.public" to "CURVE25519:j(IBzX*F%OZF;g77O8jrVjM1a`Y<6-ehe{S;{gph",
    "crencryption.private" to "CURVE25519:55t6A0y%S?{7c47p(R@C*X#at9Y`q5(Rc#YBS;r}",
    "crsigning.public" to "ED25519:d0-oQb;{QxwnO{=!|^62+E=UYk2Y3mr2?XKScF4D",
    "crsigning.private" to "ED25519:ip52{ps^jH)t\$k-9bc_RzkegpIW?}FFe~BX&<V}9",
    "encryption.public" to "CURVE25519:nSRso=K(WF{P+4x5S*5?Da-rseY-^>S8VN#v+)IN",
    "encryption.private" to "CURVE25519:4A!nTPZSVD#tm78d=-?1OIQ43{ipSpE;@il{lYkg",
    "signing.public" to "ED25519:k^GNIJbl3p@N=j8diO-wkNLuLcNF6#JF=@|a}wFE",
    "signing.private" to "ED25519:;NEoR>t9n3v%RbLJC#*%n4g%oxqzs)&~k+fH4uqi",
    "storage" to "XSALSA20:(bk%y@WBo3&}(UeXeHeHQ|1B}!rqYF20DiDG+9^Q",
    "folder" to "XSALSA20:-DfH*_9^tVtb(z9j3Lu@_(=ow7q~8pq^<;;f%2_B",
    "device.public" to "CURVE25519:94|@e{Kpsu_Qe{L@_U;QnOHz!eJ5zz?V@>+K)6F}",
    "device.private" to "CURVE25519:!x2~_pSSCx1M\$n7{QBQ5e*%~ytBzKL_C(bCviqYh",
    "devid" to "fd21b07b-6112-4a89-b998-a1c55755d9d7",
    "keycard" to userKeycard,
)


class TestProfileData(val data: MutableMap<String, Any>) {
    val formattedName: String
        get() {
            return data["name"]!! as String
        }
    val givenName: String
        get() = data["name.given"]!! as String
    val familyName: String
        get() = data["name.family"]!! as String

    val uid: UserID
        get() {
            return data["uid"]!! as UserID
        }
    val wid: RandomID
        get() {
            return data["wid"]!! as RandomID
        }
    val domain: Domain
        get() {
            return data["domain"]!! as Domain
        }
    val address: MAddress
        get() {
            return data["address"]!! as MAddress
        }
    val waddress: WAddress
        get() {
            return data["waddress"]!! as WAddress
        }
    val password: String
        get() {
            return data["password"]!! as String
        }
    val passhash: String
        get() {
            return data["passhash"]!! as String
        }
    val clientPasshash: String
        get() {
            return data["client_passhash"]!! as String
        }
    val crsigning: SigningPair
        get() {
            return data["crsigning"]!! as SigningPair
        }
    val crencryption: EncryptionPair
        get() {
            return data["crencryption"]!! as EncryptionPair
        }
    val signing: SigningPair
        get() {
            return data["signing"]!! as SigningPair
        }
    val encryption: EncryptionPair
        get() {
            return data["encryption"]!! as EncryptionPair
        }
    val devid: RandomID
        get() {
            return data["devid"]!! as RandomID
        }
    val devpair: EncryptionPair
        get() {
            return data["devpair"]!! as EncryptionPair
        }
    val storage: SecretKey
        get() {
            return data["storage"]!! as SecretKey
        }
    val regcode: String
        get() {
            return (data["regcode"]
                ?: throw TestFailureException("User profile regcode referenced")) as String
        }
    val reghash: String
        get() {
            return (data["reghash"]
                ?: throw TestFailureException("User profile reghash referenced")) as String
        }
    val keycard: Keycard
        get() {
            return data["keycard"]!! as Keycard
        }
    val profileName: String
        get() {
            return data["profile_name"]!! as String
        }

    val contactInfo: Contact
        get() {
            return data["contact"]!! as Contact
        }
}

val gAdminProfileData = createAdminProfileData()

val gUserProfileData = createUserProfileData()

fun createAdminProfileData(): TestProfileData {
    return TestProfileData(
        mutableMapOf(
            "name" to "Mensago Administrator",
            "name.given" to "Mensago",
            "name.family" to "Administrator",
            "uid" to UserID.fromString("admin")!!,
            "wid" to RandomID.fromString("ae406c5e-2673-4d3e-af20-91325d9623ca")!!,
            "domain" to Domain.fromString("example.com")!!,
            "address" to MAddress.fromString("admin/example.com")!!,
            "waddress" to WAddress.fromString("ae406c5e-2673-4d3e-af20-91325d9623ca/example.com")!!,
            "password" to "Linguini2Pegboard*Album",
            "passhash" to "\$sha3-256\$t=2\$O+SqHAqQZWb7ZXp+hOjBfF4FDvR/1akVfZRiJ+tTdrg=" +
                    "\$jJSX1dU71x3bf8k6b/CenILU/DO2/UcljOt8t+0CrXc=",
            "client_passhash" to "\$sha3-256\$t=2\$wE313+fzp8wMSWd0QYcFjP4DFyaXni9oE/CwW6oJ82E=" +
                    "\$Q/aD1jFYIUuWGI+X5szIiA/SadsvOkz9KqKZh3tSxkg=",

            "crsigning" to SigningPair.fromStrings(
                "ED25519:E?_z~5@+tkQz!iXK?oV<Zx(ec;=27C8Pjm((kRc|",
                "ED25519:u4#h6LEwM6Aa+f<++?lma4Iy63^}V\$JOP~ejYkB;",
            ).getOrThrow(),
            "crencryption" to EncryptionPair.fromStrings(
                "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
                "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>"
            ).getOrThrow(),
            "signing" to SigningPair.fromStrings(
                "ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p",
                "ED25519:p;XXU0XF#UO^}vKbC-wS(#5W6=OEIFmR2z`rS1j+"
            ).getOrThrow(),
            "encryption" to EncryptionPair.fromStrings(
                "CURVE25519:Umbw0Y<^cf1DN|>X38HCZO@Je(zSe6crC6X_C_0F",
                "CURVE25519:Bw`F@ITv#sE)2NnngXWm7RQkxg{TYhZQbebcF5b$"
            ).getOrThrow(),

            "devid" to RandomID.fromString("3abaa743-40d9-4897-ac77-6a7783083f30")!!,
            "devpair" to EncryptionPair.fromStrings(
                "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^\$iiN{5R->#jxO@cy6{",
                "CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>",
            ).getOrThrow(),
            "storage" to SecretKey.fromString(
                "XSALSA20:M^z-E(u3QFiM<QikL|7|vC|aUdrWI6VhN+jt>GH}"
            ).getOrThrow(),
            "regcode" to "Undamaged Shining Amaretto Improve Scuttle Uptake",
            "reghash" to "\$sha3-256\$t=2\$A80fwv/3mo4cX4DMViiK2yT8r8Hbeac1xCnMqAtjA+0=" +
                    "\$JuOcXGZk2B/vK1akaibAHcuHchRfhtI0e9TaJ/4CzGw=",
            "keycard" to run {
                val card = Keycard.new("User")!!
                card.entries.add(UserEntry.fromString(adminKeycard).getOrThrow())
                card
            },
            "profile_name" to "primary",

            "contact" to Contact(
                Contact.EntityType.Individual,
                formattedName = "Mensago Administrator",
                givenName = "Mensago",
                familyName = "Administrator",
                gender = "Male",
                websites = mutableListOf(
                    Contact.LabeledField("personal", "https://www.example.com", true)
                ),
                workspace = RandomID.fromString("ae406c5e-2673-4d3e-af20-91325d9623ca")!!,
                userID = UserID.fromString("admin")!!,
                domain = Domain.fromString("example.com")!!,
                phone = mutableListOf(
                    Contact.LabeledField("mobile", "555-234-5678", true)
                ),
                birthday = "19700101",
                anniversary = "1209",
                email = mutableListOf(
                    Contact.LabeledField("work", "admin@mensago.org", true)
                ),
                id = gUserConID
            ).apply {
                val info = imageToBytes(Image("nope.jpg")).getOrThrow()!!
                photo = Contact.Photo(info.first, Base85.encode(info.second))
            }
        )
    )
}

fun createUserProfileData(): TestProfileData {
    return TestProfileData(
        mutableMapOf(
            "name" to "Corbin Simons",
            "name.given" to "Corbin",
            "name.family" to "Simons",
            "uid" to UserID.fromString("csimons")!!,
            "wid" to RandomID.fromString("4418bf6c-000b-4bb3-8111-316e72030468")!!,
            "domain" to Domain.fromString("example.com")!!,
            "address" to MAddress.fromString("csimons/example.com")!!,
            "waddress" to WAddress.fromString("4418bf6c-000b-4bb3-8111-316e72030468/example.com")!!,
            "password" to "MyS3cretPassw*rd",
            "passhash" to "\$sha3-256\$t=2\$O+SqHAqQZWb7ZXp+hOjBfF4FDvR/1akVfZRiJ+tTdrg=" +
                    "\$DDJZQQbOwIY+NQo+JKVmypJygW0V7JjthALz8FJPbxM=",
            "client_passhash" to "\$sha3-256\$t=2\$wE313+fzp8wMSWd0QYcFjP4DFyaXni9oE/CwW6oJ82E=" +
                    "\$cKH77CX1S+wGyLSSzEveNyD71Wbl1MOPWlVaOCsCwmE=",

            "crsigning" to SigningPair.fromStrings(
                "ED25519:d0-oQb;{QxwnO{=!|^62+E=UYk2Y3mr2?XKScF4D",
                "ED25519:ip52{ps^jH)t\$k-9bc_RzkegpIW?}FFe~BX&<V}9",
            ).getOrThrow(),
            "crencryption" to EncryptionPair.fromStrings(
                "CURVE25519:j(IBzX*F%OZF;g77O8jrVjM1a`Y<6-ehe{S;{gph",
                "CURVE25519:55t6A0y%S?{7c47p(R@C*X#at9Y`q5(Rc#YBS;r}"
            ).getOrThrow(),
            "signing" to SigningPair.fromStrings(
                "ED25519:k^GNIJbl3p@N=j8diO-wkNLuLcNF6#JF=@|a}wFE",
                "ED25519:;NEoR>t9n3v%RbLJC#*%n4g%oxqzs)&~k+fH4uqi"
            ).getOrThrow(),
            "encryption" to EncryptionPair.fromStrings(
                "CURVE25519:nSRso=K(WF{P+4x5S*5?Da-rseY-^>S8VN#v+)IN",
                "CURVE25519:4A!nTPZSVD#tm78d=-?1OIQ43{ipSpE;@il{lYkg"
            ).getOrThrow(),

            "devid" to RandomID.fromString("fd21b07b-6112-4a89-b998-a1c55755d9d7")!!,
            "devpair" to EncryptionPair.fromStrings(
                "CURVE25519:94|@e{Kpsu_Qe{L@_U;QnOHz!eJ5zz?V@>+K)6F}",
                "CURVE25519:!x2~_pSSCx1M\$n7{QBQ5e*%~ytBzKL_C(bCviqYh"
            ).getOrThrow(),
            "storage" to SecretKey.fromString(
                "XSALSA20:(bk%y@WBo3&}(UeXeHeHQ|1B}!rqYF20DiDG+9^Q"
            ).getOrThrow(),
            "keycard" to run {
                val card = Keycard.new("User")!!
                card.entries.add(UserEntry.fromString(userKeycard).getOrThrow())
                card
            },
            "profile_name" to "user",

            "contact" to Contact(
                Contact.EntityType.Individual,
                formattedName = "Corbin Simons",
                givenName = "Corbin",
                familyName = "Simons",
                gender = "Male",
                websites = mutableListOf(
                    Contact.LabeledField("personal", "https://www.example.com", true)
                ),
                workspace = RandomID.fromString("4418bf6c-000b-4bb3-8111-316e72030468")!!,
                userID = UserID.fromString("csimons")!!,
                domain = Domain.fromString("example.com")!!,
                phone = mutableListOf(
                    Contact.LabeledField("mobile", "555-555-1234", true)
                ),
                birthday = "19750415",
                anniversary = "0714",
                email = mutableListOf(
                    Contact.LabeledField("work", "corbin.simons@example.com", true)
                ),
                id = gUserConID
            ),
        )
    )
}
