package end2end

import connect.Client
import connect.KeyPurpose
import connect.messages.DevReqMsgItem
import connect.messages.MsgBaseItem
import connect.profiles.KeyDAO
import connect.profiles.ProfileManager
import connect.profiles.WorkspaceDAO
import connect.sync.IncomingHandler
import connect.sync.SyncDAO
import keznacl.toEncryptionKey
import libmensago.MessageAction
import libmensago.ResourceExistsException
import libmensago.WorkspaceStatus
import libmensago.commands.idle
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_ADMIN_KEYCARD
import testsupport.TestProcessor
import testsupport.gAdminProfileData

@ExtendWith(ApplicationExtension::class)
class E2ENewDevice {
    // This high-level test covers new device handling on the client and server side.
    @Test
    fun runTest() {
        ITestEnvironment("e2e_newdevice").provision(SETUP_TEST_ADMIN_KEYCARD)
        ProfileManager.activateProfile("primary")?.let { throw it }

        // This just confirms that the primary profile can successfully log in
        val client = Client()
        client.login(gAdminProfileData.address).getOrThrow()
        client.logout()
        client.disconnect()

        val profile1EPair = KeyDAO().loadKeypairByPurpose(KeyPurpose.Encryption).getOrThrow()
        val profile1DevID = WorkspaceDAO().getDeviceID(gAdminProfileData.waddress).getOrThrow()

        // Attempt to perform a new device login on a profile which is not local-only. This is
        // supposed to fail because new device logins are only for profiles which want to be
        // associated with an existing identity workspace. This means they can't already have an
        // identity associated with them and, thus, are local-only.
        assert(
            client.login(gAdminProfileData.address, gAdminProfileData.password)
                .exceptionOrNull() is ResourceExistsException
        )

        // Make the second profile and attempt to log in. This should fail, as the new profile is
        // local-only.
        ProfileManager.createProfile("admin2")?.let { throw it }
        ProfileManager.activateProfile("admin2")?.let { throw it }
        assert(ProfileManager.model.activeProfile!!.isLocal().getOrThrow())

        // This call returns non-null if the server responds with 105 APPROVAL REQUIRED to the login
        // process
        val status = client.login(gAdminProfileData.address, gAdminProfileData.password)
            .getOrThrow()
        assertEquals(WorkspaceStatus.Pending, status.first)
        assertFalse(
            client.checkDeviceApproved(
                gAdminProfileData.wid,
                ProfileManager.model.activeProfile!!.devid
            ).getOrThrow()
        )
        client.logout()

        ProfileManager.activateProfile("primary")?.let { throw it }
        client.login(gAdminProfileData.address).getOrThrow()

        // At this point there should be 1 update for us -- a new message needing processed.
        val updateCount = idle(client.getConnection(), 0L).getOrThrow()
        assertEquals(1, updateCount)

        val tproc = TestProcessor()
        val handler = IncomingHandler(client, tproc, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }

        val udao = SyncDAO()
        assertEquals(1, udao.loadIncomingItems().getOrThrow().size)

        handler.executeTasks()?.let { throw it }
        client.logout()

        assertEquals(1, tproc.msgItems.size)
        val devreq: MsgBaseItem = tproc.msgItems[0]
        devreq as DevReqMsgItem
        assertEquals("devrequest", devreq.subType)

        val attmap = devreq.info.attributes

        assert("Device Name" in attmap)
        assert("User Name" in attmap)
        assert("OS" in attmap)
        assert("Timestamp" in attmap)
        assert("IP Address" in attmap)

        val devid = devreq.info.id
        val devkey = devreq.info.pubkey.toEncryptionKey().getOrThrow()

        client.login(gAdminProfileData.address).getOrThrow()
        client.handleNewDevice(devid, MessageAction.Approve, devkey)?.let { throw it }
        client.logout()

        ProfileManager.activateProfile("admin2")?.let { throw it }
        assert(client.checkDeviceApproved(gAdminProfileData.wid, devid).getOrThrow())
        client.login(gAdminProfileData.address).getOrThrow()

        // Ensure that the database loaded is the one we think it is
        val wdao = WorkspaceDAO()
        val profile2DevID = wdao.getDeviceID(gAdminProfileData.waddress).getOrThrow()
        assert(profile1DevID != profile2DevID)
        assertEquals(devid, profile2DevID)

        val kdao2 = KeyDAO()
        val profile2EPair = kdao2.loadKeypairByPurpose(KeyPurpose.Encryption).getOrThrow()
        assertEquals(profile1EPair.first, profile2EPair.first)

        assertEquals(
            WorkspaceStatus.Active,
            wdao.getStatus(gAdminProfileData.waddress).getOrThrow()
        )

        client.disconnect()
    }
}