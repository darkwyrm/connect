package end2end

import connect.Client
import connect.ContactName
import connect.UserInfoDAO
import connect.contacts.ContactDAO
import connect.main.MainController
import connect.messages.ConMsgItem
import connect.messages.MessageDAO
import connect.profiles.ProfileManager
import connect.sync.IncomingHandler
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_RELATIONSHIP
import testsupport.gAdminProfileData
import testsupport.gUserProfileData

@ExtendWith(ApplicationExtension::class)
class E2EConInfoUpdate {

    @Test
    fun runTest() {
        // This test handles sending contact information updates and processing.
        // The test environment handles the CR exchange. Afterward, the admin sends an info update
        // to the user and the user's client automatically processes and applies the changes.

        ITestEnvironment("e2e_coninfoupdate").provision(SETUP_TEST_RELATIONSHIP)

        // Admin sends an info update

        assert(gAdminProfileData.formattedName == "Mensago Administrator")
        // Middle name doesn't even exist in the data structure, so not checking for null here

        val updatedName = ContactName("Mensago D. Administrator", middleName = "D.")
        val updatedInfo = UserInfoDAO().let {
            it.saveName(updatedName)?.let { e -> throw e }
            it.loadPublicContact().getOrThrow()
        }

        val client = Client()
        client.sendContactInfoUpdate(gUserProfileData.waddress, updatedInfo)?.let { throw it }
        client.disconnect()

        // User receives and processes info update

        ProfileManager.activateProfile("user")?.let { throw it }
        val userMain = MainController()

        // Load contact information for admin and confirm value is the original

        // get updates
        client.login(gUserProfileData.address).getOrThrow()

        val handler = IncomingHandler(client, userMain, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }
        handler.executeTasks()?.let { throw it }

        val userMsgCon = userMain.messaging()
        val userMsgList = userMsgCon.getMessages()
        val lastItem = MessageDAO().load(userMsgList.last().id).getOrThrow()!!

        assert(lastItem !is ConMsgItem)

        client.disconnect()

        val loaded = ContactDAO().loadConItemByName("Mensago D. Administrator")
            .getOrThrow()!!
        assert(loaded.middleName == "D.")
    }
}