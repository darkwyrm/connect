package end2end

import connect.Client
import connect.main.MainController
import connect.messages.DevReqMsgItem
import connect.messages.MsgBaseItem
import connect.profiles.ProfileManager
import connect.sync.IncomingHandler
import connect.sync.SyncDAO
import keznacl.toEncryptionKey
import libmensago.MessageAction
import libmensago.WorkspaceStatus
import libmensago.commands.idle
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_ADMIN_KEYCARD
import testsupport.TestProcessor
import testsupport.gAdminProfileData

@ExtendWith(ApplicationExtension::class)
class E2EItemSyncNote {

    @Test
    fun runTest() {
        // This test uses the high-level APIs to create a note and ensure it synchronizes to
        // another device.
        ITestEnvironment("e2e_syncnote").provision(SETUP_TEST_ADMIN_KEYCARD)
        setupDevices()

        val mainCon1 = MainController()
        val noteCon1 = mainCon1.noteController
        val newNote = noteCon1.createNewNote("e2e_syncnote").getOrThrow()
        println(newNote)
    }

    // This private method handles setting up the second device so as to not clutter up the actual
    // test logic.
    private fun setupDevices() {

        val client = Client()
        ProfileManager.createProfile("admin2")?.let { throw it }
        ProfileManager.activateProfile("admin2")?.let { throw it }

        // This call returns non-null if the server responds with 105 APPROVAL REQUIRED to the login
        // process
        val status = client.login(gAdminProfileData.address, gAdminProfileData.password)
            .getOrThrow()
        assertEquals(WorkspaceStatus.Pending, status.first)
        client.logout()

        ProfileManager.activateProfile("primary")?.let { throw it }
        client.login(gAdminProfileData.address).getOrThrow()

        // At this point there should be 1 update for us -- a new message needing processed.
        val updateCount = idle(client.getConnection(), 0L).getOrThrow()
        assertEquals(1, updateCount)

        val tproc = TestProcessor()
        val handler = IncomingHandler(client, tproc, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }

        val sdao = SyncDAO()
        assertEquals(1, sdao.loadIncomingItems().getOrThrow().size)

        handler.executeTasks()?.let { throw it }
        client.logout()

        assertEquals(1, tproc.msgItems.size)
        val devreq: MsgBaseItem = tproc.msgItems[0]
        devreq as DevReqMsgItem
        assertEquals("devrequest", devreq.subType)

        val devid = devreq.info.id
        val devkey = devreq.info.pubkey.toEncryptionKey().getOrThrow()

        client.login(gAdminProfileData.address).getOrThrow()
        client.handleNewDevice(devid, MessageAction.Approve, devkey)?.let { throw it }

        client.disconnect()

        ProfileManager.activateProfile("primary")?.let { throw it }
    }
}