package connect.messages

import connect.Client
import connect.ContactKeySet
import connect.UserInfoDAO
import connect.contacts.ContactDAO
import connect.main.MainController
import connect.profiles.ProfileManager
import connect.sync.OutgoingHandler
import libkeycard.RandomID
import libmensago.MessageAction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*

@ExtendWith(ApplicationExtension::class)
class MessageInteractorITest {

    /** This test is just for ensuring that client-level CR processing works correctly. */
    @Test
    fun crTest() {
        ITestEnvironment("msgint_cr_process").provision(SETUP_TEST_CONTACT_INFO)

        ProfileManager.activateProfile("user")?.let { throw it }

        val userInfo = UserInfoDAO().loadPublicContact().getOrThrow()
        var client = Client()
        client.login(gUserProfileData.address).getOrThrow()
        client.sendContactRequest(
            gAdminProfileData.address, userInfo, "Test CR message: msgint_cr_process", false
        ).getOrThrow()
        OutgoingHandler.processAllTasks(client)?.let { throw it }
        client.logout()?.let { throw it }

        ProfileManager.activateProfile("primary")?.let { throw it }
        val adminMainCon = MainController()
        runUpdates(gAdminProfileData, adminMainCon)

        // Make sure that we received the CR and that both the display item and the actual CR
        // are correct.
        val adminMsgs = adminMainCon.messaging().getMessages()
        assert(adminMsgs.isNotEmpty())
        val adminHeaderItem = adminMsgs.last() as ConMsgHeaderItem
        assert(adminHeaderItem.subject.contains(gUserProfileData.formattedName))
        assert(adminHeaderItem.fromAddr.isNotEmpty())
        assert(adminHeaderItem.fromName.isNotEmpty())
        assert(adminHeaderItem.getLabels().isNotEmpty())

        val adminItem = MessageDAO().loadConMsg(adminHeaderItem.id).getOrThrow()!!
        assert(adminItem.subject.contains(gUserProfileData.formattedName))
        assert(adminItem.fromAddr.contains(gUserProfileData.domain.value))
        assertNotNull(RandomID.fromString(adminItem.id))
        assertNotNull(RandomID.fromString(adminItem.threadID))
        assertEquals("Test CR message: msgint_cr_process", adminItem.contents)
        assertEquals(gUserProfileData.formattedName, adminItem.fromName)
        assertEquals("conreq.1", adminItem.subType)
        assert(adminItem.getLabels().isNotEmpty())
        assertEquals(gUserProfileData.formattedName, adminItem.contact.formattedName)

        val adminInfo = UserInfoDAO().loadPublicContact().getOrThrow()
        client = Client()
        client.approveContactRequest(gUserProfileData.waddress, adminInfo, userInfo, false)
            ?.let { throw it }
        client.login(gAdminProfileData.address).getOrThrow()
        OutgoingHandler.processAllTasks(client)?.let { throw it }
        client.logout()?.let { throw it }

        // Now that we've approved the contact request, make sure the response is received and
        // correct.

        ProfileManager.activateProfile("user")?.let { throw it }

        val userMainCon = MainController()
        runUpdates(gUserProfileData, userMainCon)

        val userMsgs = userMainCon.messaging().getMessages()
        assert(userMsgs.isNotEmpty())
        val userHeaderItem = userMsgs.last() as ConMsgHeaderItem

        // We don't bother checking for the admin's formatted name because name in the test keycard
        // is out-of-sync with the actual test profile info and I don't feel like updating all the
        // test hashes after changing it. :/
        assert(userHeaderItem.subject.contains("Administrator"))
        assert(userHeaderItem.fromAddr.contains(gAdminProfileData.address.toString()))
        assert(userHeaderItem.fromName.contains("Administrator"))
        assert(userHeaderItem.getLabels().isNotEmpty())

        val userItem = MessageDAO().loadConMsg(userHeaderItem.id).getOrThrow()!!
        assertEquals("conreq.2", userItem.subType)
        assertEquals(gAdminProfileData.formattedName, userItem.contact.formattedName)
    }

    /** This test actually checks that MessageInteractor's approval code works correctly. */
    @Test
    fun approveCRTest() {
        ITestEnvironment("msgint_approvecr").provision(SETUP_TEST_CONTACT_INFO)

        ProfileManager.activateProfile("user")?.let { throw it }

        val userPublicInfo = UserInfoDAO().loadPublicContact().getOrThrow()
        val client = Client()
        client.login(gUserProfileData.address).getOrThrow()
        val keySet = ContactKeySet.generate().getOrThrow()
        userPublicInfo.keys = keySet.toPublicMap()
        client.sendContactRequest(
            gAdminProfileData.address, userPublicInfo, "Test CR message: msgint_cr_process",
            false
        ).getOrThrow()
        client.disconnect()?.let { throw it }

        ProfileManager.activateProfile("primary")?.let { throw it }
        val adminMainCon = MainController()
        runUpdates(gAdminProfileData, adminMainCon)

        // Make sure that we received the CR and that both the display item and the actual CR
        // are correct.
        val adminMsgCon = adminMainCon.messaging()
        val adminMsgs = adminMsgCon.getMessages()
        assert(adminMsgs.isNotEmpty())
        assert(adminMsgs.last() is ConMsgHeaderItem)
        adminMsgCon.handleMsgSelection(null, adminMsgs.last())
        adminMsgCon.handleConReqAction(MessageAction.Approve)
        assert(adminMsgs.last() !is ConMsgHeaderItem)

        val conItem = ContactDAO().conItemForWAddress(gUserProfileData.waddress)
        assertNotNull(conItem)

        ProfileManager.activateProfile("user")?.let { throw it }

        val userMainCon = MainController()
        runUpdates(gUserProfileData, userMainCon)

        val usrMsgCon = userMainCon.messaging()
        val usrMsgs = usrMsgCon.getMessages()
        assert(usrMsgs.isNotEmpty())
        val approval = usrMsgs.last() as ConMsgHeaderItem
        assertEquals("Contact Approved for Administrator", approval.subject)
    }
}
