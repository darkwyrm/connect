package connect

import connect.contacts.ContactDAO
import javafx.scene.image.Image
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_ADMIN_PROFILE
import testsupport.SETUP_TEST_CONTACT_INFO
import testsupport.gAdminProfileData
import utilities.DBProxy

@ExtendWith(ApplicationExtension::class)
class UserInfoDAOTest {

    @Test
    fun loadPhoto() {
        ITestEnvironment("userinfodao_loadphoto").provision(SETUP_TEST_ADMIN_PROFILE)
        ContactDAO().initContactTables()

        val blankPhoto = Image("blankprofile.png")

        val db = DBProxy.get()
        db.execute(
            "UPDATE contacts SET phototype=?,photo=? WHERE id=?",
            "image/png", blankPhoto, gUserConID
        )?.let { throw it }

        val dao = UserInfoDAO()
        dao.loadPhoto().getOrThrow()
    }

    @Test
    fun savePhoto() {
        ITestEnvironment("userinfodao_savephoto").provision(SETUP_TEST_ADMIN_PROFILE)

        ContactDAO().initContactTables()

        val blankPhoto = Image("blankprofile.png")
        val dao = UserInfoDAO()
        dao.savePhoto(blankPhoto)?.let { throw it }
        dao.loadPhoto().getOrThrow()
    }

    @Test
    fun basicTest() {
        ITestEnvironment("userinfodao_basic").provision(SETUP_TEST_CONTACT_INFO)

        ContactDAO().initContactTables()
        val dao = UserInfoDAO()

        assertEquals(gAdminProfileData.formattedName, dao.loadFormattedName().getOrThrow())
        assertEquals(gAdminProfileData.givenName, dao.loadGivenName().getOrThrow())
        assertEquals(gAdminProfileData.familyName, dao.loadFamilyName().getOrThrow())
        assertEquals(gAdminProfileData.waddress.toString(), dao.loadWAddress().getOrThrow())
        assertEquals(gAdminProfileData.address.toString(), dao.loadMAddress().getOrThrow())

        dao.saveFormattedName("Joe Admin")?.let { throw it }
        assertEquals("Joe Admin", dao.loadFormattedName().getOrThrow())

        dao.saveGivenName("Joe")?.let { throw it }
        assertEquals("Joe", dao.loadGivenName().getOrThrow())

        dao.saveFamilyName("Admin")?.let { throw it }
        assertEquals("Admin", dao.loadFamilyName().getOrThrow())

        dao.saveIdentity("joe_admin", "11111111-1111-1111-111111111111", "contoso.com")
        assertEquals(
            "11111111-1111-1111-111111111111/contoso.com", dao.loadWAddress().getOrThrow()
        )
        assertEquals("joe_admin/contoso.com", dao.loadMAddress().getOrThrow())

        assertEquals("19700101", dao.loadMisc("birthday").getOrThrow())
        dao.saveMisc("birthday", "20001215")?.let { throw it }
        assertEquals("20001215", dao.loadMisc("birthday").getOrThrow())
    }
}
