package connect.sync

import connect.Client
import connect.profiles.ProfileManager
import libmensago.WorkspaceStatus
import libmensago.commands.idle
import libmensago.commands.setStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*

@ExtendWith(ApplicationExtension::class)
class OnlineWorkerTest {

    @Test
    fun basicTest() {
        val env = ITestEnvironment("onlineworker_basic")
            .provision(SETUP_TEST_ADMIN_KEYCARD)
        env.login(gAdminProfileData)
        generateDemoUpdates(env, gAdminProfileData)
        assertEquals(14, idle(env.conn, 0).getOrThrow())
        env.disconnect()

        val tproc = TestProcessor()
        val handler = OnlineWorker(tproc, ProfileManager.model).apply { testMode = true }
        handler.incomingWorker()
    }

    @Test
    fun suspendedTest() {
        val env = ITestEnvironment("onlineworker_suspended")
            .provision(SETUP_TEST_ADMIN_KEYCARD)
        env.login(gAdminProfileData)
        generateDemoUpdates(env, gAdminProfileData)
        assertEquals(14, idle(env.conn, 0).getOrThrow())
        env.disconnect()

        val client = Client()
        client.login(gAdminProfileData.address).getOrThrow()
        setStatus(client.getConnection(), gUserProfileData.wid, "suspended")
        client.logout()?.let { throw it }

        ProfileManager.activateProfile("user")?.let { throw it }
        val info = client.login(gUserProfileData.address).getOrThrow()
        assertEquals(WorkspaceStatus.Suspended, info.first)
        client.disconnect()?.let { throw it }

        val tproc = TestProcessor()
        val handler = OnlineWorker(tproc, ProfileManager.model).apply { testMode = true }
        handler.incomingWorker()
        assertEquals(WorkspaceStatus.Suspended, ProfileManager.model.activeIdentityStatus)
    }
}
