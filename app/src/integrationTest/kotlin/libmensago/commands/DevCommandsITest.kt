package libmensago.commands

import keznacl.BadValueException
import keznacl.CryptoString
import libkeycard.RandomID
import libmensago.DeviceStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_ADMIN_PROFILE
import testsupport.gAdminProfileData
import java.time.Instant

class DevCommandsITest {

    @Test
    fun getDeviceStatusTest() {
        val env = ITestEnvironment("copy").provision(SETUP_TEST_ADMIN_PROFILE)
        env.connectToServer()

        val adminWID = gAdminProfileData.wid
        val devpair = gAdminProfileData.devpair
        val fakeInfo = CryptoString.fromString("XSALSA20:myfakedevInfoYAY")!!

        // Loop-based test cases. Includes all status except DeviceStatus.Unregistered
        val devList = listOf(
            // Approved device
            Pair(
                RandomID.fromString("6c1a461f-e459-49eb-a7e4-46fba0d84722")!!,
                DeviceStatus.Approved
            ),

            // Blocked device
            Pair(
                RandomID.fromString("3955b8ee-01e2-4a99-bbec-8ec70bd1de05")!!,
                DeviceStatus.Pending
            ),

            // Pending device
            Pair(
                RandomID.fromString("adc5bc01-d515-43de-9e95-53464c3953ef")!!,
                DeviceStatus.Pending
            ),

            // Registered device
            Pair(gAdminProfileData.devid, DeviceStatus.Registered),
        )

        val db = env.getDB()
        devList.forEach {
            db.execute(
                "INSERT INTO iwkspc_devices(wid,devid,devkey,devinfo,lastlogin,status) " +
                        "VALUES(?,?,?,?,?,?)",
                adminWID,
                it.first,
                devpair.pubKey,
                fakeInfo,
                Instant.now(),  // The lastlogin value doesn't matter in this case
                it.second
            )
        }

        // Test each of the loop-based cases
        devList.forEach {
            val status = getDeviceStatus(env.conn, gAdminProfileData.wid, it.first).getOrThrow()
            if (status != it.second)
                throw BadValueException("Expected ${it.second}, got $status")
        }

        val unregStatus = getDeviceStatus(
            env.conn, gAdminProfileData.wid,
            RandomID.fromString("dbe356b2-ef16-4499-a749-8b6647690fb8")!!
        ).getOrThrow()
        assertEquals(DeviceStatus.NotRegistered, unregStatus)
    }
}
