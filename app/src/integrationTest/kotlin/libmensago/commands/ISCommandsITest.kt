package libmensago.commands

import connect.profiles.ProfileManager
import keznacl.*
import libkeycard.*
import libmensago.AdminRequiredException
import libmensago.DeviceInfo
import libmensago.ProtocolException
import libmensago.ServerConnection
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*
import java.net.InetAddress

@ExtendWith(ApplicationExtension::class)
class ISCommandsITest {

    // addEntry() and device() are tested by the regcodeUser()

    @Test
    fun archiveAdminTest() {
        val env = ITestEnvironment("unregister_admin").provision(SETUP_TEST_USER)
        env.login(gAdminProfileData)

        val result =
            archive(env.conn, gUserProfileData.wid).getOrThrow()

        when (val serverMode = env.serverconfig.getString("global.registration")) {
            "private", "moderated" -> result.assertReturnCode(101)
            "network", "public" -> result.assertReturnCode(202)
            else -> throw ProgramException("Invalid server registration mode '$serverMode'")
        }
        env.logout()

        val err =
            login(env.conn, gUserProfileData.wid, env.serverEncryptionPair()).exceptionOrNull()
        assert(err is ProtocolException)
        assertEquals(202, (err as ProtocolException).code)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun archiveUserTest() {
        val env = ITestEnvironment("archive_user").provision(SETUP_TEST_BOTH_PROFILES)

        ProfileManager.activateProfile("user")?.let { throw it }

        env.login(gUserProfileData)

        val result = archive(env.conn, null).getOrThrow()

        when (val serverMode = env.serverconfig.getString("global.registration")) {
            "private", "moderated" -> result.assertReturnCode(101)
            "network", "public" -> result.assertReturnCode(202)
            else -> throw ProgramException("Invalid server registration mode '$serverMode'")
        }

        val err =
            login(env.conn, gUserProfileData.wid, env.serverEncryptionPair()).exceptionOrNull()
        assert(err is ProtocolException)
        assertEquals(202, (err as ProtocolException).code)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun cancelTest() {
        val env = ITestEnvironment("cancel").provision(SETUP_TEST_BOTH_PROFILES)

        val conn = ServerConnection()
        val port = env.serverconfig.getInteger("network.port")!!
        val ip = InetAddress.getByName(env.serverconfig.getString("network.listen_ip"))
        conn.connect(ip, port)

        val waddr = WAddress.fromString(ADMIN_PROFILE_DATA["waddress"])!!
        val oekey = env.serverEncryptionPair().pubKey.toEncryptionKey().getOrThrow()
        conn.disconnect()
        conn.connect(ip, port)
        login(conn, waddr.id, oekey).getOrThrow()
        cancel(conn)?.let { throw it }
    }

    @Test
    fun devKeyTest() {
        val env = ITestEnvironment("devkey").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)

        val oldPair = EncryptionPair.fromStrings(
            ADMIN_PROFILE_DATA["device.public"]!!,
            ADMIN_PROFILE_DATA["device.private"]!!
        ).getOrThrow()

        val newPair = EncryptionPair.generate().getOrThrow()
        devKey(env.conn, gAdminProfileData.devid, oldPair, newPair)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun getWIDTest() {
        val env = ITestEnvironment("getwid").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)

        val wid = getWID(env.conn, gAdminProfileData.uid, gAdminProfileData.domain)
            .getOrThrow()

        assertEquals(gAdminProfileData.wid, wid)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun getKeycardTest() {
        val env = ITestEnvironment("getkeycard").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)

        val card = getCard(env.conn, null, 1).getOrThrow()
        assertEquals(2, card.entries.size)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun orgCardTest() {
        val env = ITestEnvironment("orgcard").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)

        val card = getCard(env.conn, null, 1).getOrThrow()
        assertEquals(2, card.entries.size)

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun preRegRegCodeTest() {
        val env = ITestEnvironment("prereg_regcode")
            .provision(SETUP_TEST_ADMIN)
        env.login(gAdminProfileData)

        val userWID = RandomID.fromString(USER_PROFILE_DATA["wid"]!!)!!
        val userUID = UserID.fromString(USER_PROFILE_DATA["uid"]!!)!!
        val userDomain = Domain.fromString(USER_PROFILE_DATA["domain"]!!)!!
        val userRegData = preregister(env.conn, userWID, userUID, userDomain).getOrThrow()

        val devID = RandomID.fromString(USER_PROFILE_DATA["devid"]!!)!!
        val devPair = EncryptionPair.fromStrings(
            USER_PROFILE_DATA["device.public"]!!,
            USER_PROFILE_DATA["device.private"]!!
        ).getOrThrow()

        val devInfo = DeviceInfo(devID, devPair.pubKey, devPair.privKey)
            .collectAttributes().getOrThrow()
        devInfo.encryptAttributes(gUserProfileData.encryption).getOrThrow()
        val pwHash = getHasherForHash(USER_PROFILE_DATA["passhash"]!!).getOrThrow()
        regCode(
            env.conn, MAddress.fromString(USER_PROFILE_DATA["address"]!!)!!,
            userRegData.regcode, pwHash, devInfo
        ).getOrThrow()

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun registerTest() {
        val env = ITestEnvironment("register").provision(SETUP_TEST_DATABASE)
        env.connectToServer()

        val devID = RandomID.generate()
        val devPair = EncryptionPair.generate().getOrThrow()

        val devInfo = DeviceInfo(devID, devPair.pubKey, devPair.privKey)
            .collectAttributes().getOrThrow()
        devInfo.encryptAttributes(gUserProfileData.encryption).getOrThrow()

        val pwHash = getHasherForHash(gUserProfileData.passhash).getOrThrow()
        register(
            env.conn, gUserProfileData.uid, pwHash, devID, devPair,
            devInfo.encryptedInfo!!
        ).getOrThrow()

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun resetPasswordTest() {
        val env = ITestEnvironment("reset_password").provision(SETUP_TEST_BOTH_PROFILES)
        env.login(gAdminProfileData)

        // Admin resets password
        val resetData = resetPassword(
            env.conn, gUserProfileData.wid,
            null, null
        ).getOrThrow()
        env.logout()

        env.login(gUserProfileData)
        // User successfully sets a new password
        val newPassword = SHAPassword().apply {
            setIterations(2)
            updateHash("NewSecretPassword!2")
        }

        passCode(
            env.conn, gUserProfileData.wid, resetData.first,
            newPassword
        )?.let { throw it }

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun setPasswordTest() {
        val env = ITestEnvironment("set_password").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)

        val oldPassword = getHasherForHash(gAdminProfileData.clientPasshash).getOrThrow()
        val badPassword = getHasherForHash(gUserProfileData.passhash).getOrThrow()
        val newPassword = SHAPassword().apply {
            setIterations(2)
            updateHash("SomeNewPassword!1")
        }

        setPassword(env.conn, badPassword, newPassword).let {
            if (it == null)
                throw ProgramException("setpassword allowed a bad password")
        }

        setPassword(env.conn, oldPassword, newPassword)?.let { throw it }

        env.conn.disconnect()?.let { throw it }
    }

    @Test
    fun setStatusTest() {
        val env = ITestEnvironment("setstatus").provision(SETUP_TEST_BOTH_PROFILES)
        env.connectToServer()

        ProfileManager.activateProfile("user")?.let { throw it }
        val err = setStatus(env.conn, gUserProfileData.wid, "disabled")!!
        err as ProtocolException
        assertEquals(401, err.code)

        ProfileManager.activateProfile("user")?.let { throw it }
        env.login(gUserProfileData)

        val err2 = setStatus(env.conn, gUserProfileData.wid, "disabled")
        err2 as AdminRequiredException
        env.logout()

        // Now the actual test. First, have admin set user to 'disabled' and see if user can't
        // log in.
        val workStates = mapOf("disabled" to 407, "suspended" to 407, "balancedue" to 406)
        workStates.forEach { workState ->
            ProfileManager.activateProfile("primary")?.let { throw it }
            env.login(gAdminProfileData)
            setStatus(env.conn, gUserProfileData.wid, workState.key)?.let { throw it }
            env.logout()

            ProfileManager.activateProfile("user")?.let { throw it }
            val status = login(env.conn, gUserProfileData.wid, env.serverEncryptionPair())
                .exceptionOrNull()
            assert(status is ProtocolException)
            env.logout()

            // Now that we've confirmed that the user really can't log in, make sure that reverting
            // to active works, too.

            ProfileManager.activateProfile("primary")?.let { throw it }
            env.login(gAdminProfileData)
            setStatus(env.conn, gUserProfileData.wid, "active")?.let { throw it }
            env.logout()

            ProfileManager.activateProfile("user")?.let { throw it }
            env.login(gUserProfileData)
            env.logout()
        }

        env.conn.disconnect()
    }

    @Test
    fun userCardTest() {
        val env = ITestEnvironment("").provision(SETUP_TEST_BOTH_KEYCARDS)
        env.login(gAdminProfileData)
        val card = getCard(env.conn, gUserProfileData.waddress.toString(), 1).getOrThrow()

        assertEquals(2, card.entries.size)

        env.conn.disconnect()?.let { throw it }
    }
}
