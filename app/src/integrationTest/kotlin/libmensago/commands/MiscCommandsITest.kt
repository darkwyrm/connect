package libmensago.commands

import keznacl.EncryptionKey
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.Domain
import libmensago.*
import libmensago.resolver.KCResolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.*

class MiscCommandsITest {

    private fun generateUpdates(env: ITestEnvironment, pdata: TestProfileData): List<MServerPath> {

        val wid = pdata.wid
        val dirs = listOf(
            MServerPath("/ $wid messages b635f8c9-779a-4100-9e78-02ac9be23aab"),
            MServerPath("/ $wid messages 4eb340a9-dd34-49b1-bf7f-aa00ae6c6ccf"),
            MServerPath("/ $wid messages 3f2efbcd-a6d4-430f-b0ab-10969289d335"),
        )
        dirs.forEach { mkdir(env.conn, it) }

        return dirs
    }

    @Test
    fun getStatusTest() {
        val env = ITestEnvironment("cmd_getstatus").provision(SETUP_TEST_BOTH_PROFILES)
        env.login(gAdminProfileData)
        assertEquals(WorkspaceStatus.Active, getStatus(env.conn, gUserProfileData.wid).getOrThrow())

        listOf(WorkspaceStatus.Suspended, WorkspaceStatus.BalanceDue, WorkspaceStatus.Disabled)
            .forEach {
                setStatus(env.conn, gUserProfileData.wid, it.toString())?.let { e -> throw e }
                assertEquals(it, getStatus(env.conn, gUserProfileData.wid).getOrThrow())
            }
    }

    @Test
    fun getUpdatesTest() {
        val env = ITestEnvironment("cmd_getupdates").provision(SETUP_TEST_ADMIN_KEYCARD)
        env.login(gAdminProfileData)
        assert(getUpdates(env.conn, null).getOrThrow().isEmpty())

        val dirs = generateUpdates(env, gAdminProfileData)

        assertEquals(3, idle(env.conn, 0).getOrThrow())
        val updates = getUpdates(env.conn, null).getOrThrow()
        assertEquals(3, updates.size)

        assertEquals(SyncOp.Mkdir, updates[0].op)

        for (update in updates) {
            assertEquals(SyncOp.Mkdir, update.op)
            assertEquals(gAdminProfileData.devid.toString(), update.source.toString())
        }

        dirs.forEachIndexed { i, _ ->
            assertEquals(dirs[i], (updates[i].value as SyncValue.Mkdir).serverpath)
        }
    }

    @Test
    fun sendTest() {
        val env = ITestEnvironment("cmd.send").provision(SETUP_TEST_BOTH_KEYCARDS)

        // Because we have done a full, legitimate user setup here, the setup code here should
        // look a lot like production messaging code. Here we kind of pretend we're sending a
        // contact request.

        // First, get the recipient's CR encryption key
        val recipientCard =
            KCResolver.getKeycard(EntrySubject.fromWAddress(gUserProfileData.waddress)).getOrThrow()
        val recipientKey = EncryptionKey.fromString(
            recipientCard.current!!.getFieldString("Contact-Request-Encryption-Key")!!
        ).getOrThrow()

        // Cards and keys in place. Create the message and

        val senderAddress = gAdminProfileData.waddress
        val recipientAddress = gUserProfileData.waddress
        val testMsg = Message(senderAddress, recipientAddress, ContentFormat.Text)
            .withSubType("contactreq.1")
            .withSubject("Test Message")
            .withBody("This is a test message")

        val envelope = Envelope.seal(
            recipientKey,
            AddressPair(testMsg.from, testMsg.to),
            Payload(PayloadType.Message, Json.encodeToString(testMsg))
        ).getOrThrow()

        env.login(gAdminProfileData)
        send(env.conn, Domain.fromString("example.com")!!, envelope.toString())
            ?.let { throw it }

        env.conn.disconnect()
    }

    @Test
    fun sendLargeTest() {
        // Feature: SendLarge / LargeMessageSupport
        // FeatureTODO: Implement test for sendLarge()
    }
}
