package libmensago

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_DATABASE

class DNSHandlerTest {

    @Test
    fun lookupTests() {
        val dns = DNSHandler()
        var results = dns.lookupA("example.com").getOrThrow()
        assertEquals(6, results.size)
        results.find { it.toString() == "/23.192.228.80" }!!

        results = dns.lookupAAAA("example.com").getOrThrow()
        assertEquals(6, results.size)
        results.find { it.toString() == "/2600:1406:3a00:21:0:0:173e:2e66" }!!

        val strResults = dns.lookupTXT("mensago.mensago.net").getOrThrow()
        assertEquals(2, strResults.size)
        assert(strResults[0].startsWith("pvk="))
        assert(strResults[1].startsWith("tls="))

        val srvResults = dns.lookupSRV("_mensago._tcp.mensago.net").getOrThrow()
        assertEquals(3, srvResults.size)
        assertEquals("mensago2.mensago.net", srvResults[1].server.toString())
    }

    @Test
    fun fakeLookupTests() {
        val env = ITestEnvironment("dns_fakelookup").provision(SETUP_TEST_DATABASE)

        val dns = FakeDNSHandler(env.serverconfig.connectToDB().getOrThrow())
        var results = dns.lookupA("example.com").getOrThrow()
        assertEquals(1, results.size)
        assertEquals("/127.0.0.1", results[0].toString())

        results = dns.lookupAAAA("example.com").getOrThrow()
        assertEquals(1, results.size)
        assertEquals("/0:0:0:0:0:0:0:1", results[0].toString())

        val strResults = dns.lookupTXT("mensago.example.com").getOrThrow()
        assertEquals(3, strResults.size)
        assert(strResults[0].startsWith("ek="))
        assert(strResults[1].startsWith("pvk="))

        val srvResults = dns.lookupSRV("_mensago._tcp.example.com").getOrThrow()
        assertEquals(2, srvResults.size)
        assertEquals("mensago2.example.com", srvResults[1].server.toString())

        env.done()
    }
}
